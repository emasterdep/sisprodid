<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- evita el cachado, solo en modo de desarrollo BORRAR EN PRODUCCION -->
    <link rel="shortcut icon" href="" type="image/x-icon">
    <meta name="description" content=""/>
    <link rel="shortcut icon" href="<?=assets('img/favicon.png')?>">
    <meta name="keywords" content="sistema para gestion medica hospitalaria"/>
    <!-- estilos css -->
    <!-- App css -->
    <link href="<?=assets('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=assets('css/icons.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=assets('css/app.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=assets('css/misestilos.css')?>" rel="stylesheet" type="text/css" />
    <title>SISPRODI</title>
    <!-- javascript -->
    
</head>
<body>
    <?=$this->section('content') ?>
</body>
</html>