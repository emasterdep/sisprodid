<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="" type="image/x-icon">
    <meta name="description" content=""/>
    <link rel="shortcut icon" href="<?=assets('img/favicon.png')?>">
    <meta name="keywords" content="sistema para gestion medica hospitalaria"/>
    <!-- estilos css -->

    <link href="<?=assets('libs/datatables/dataTables.bootstrap4.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=assets('libs/datatables/responsive.bootstrap4.css')?>" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="<?=assets('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=assets('css/icons.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=assets('css/app.min.css')?>" rel="stylesheet" type="text/css" />
    
    <link href="<?=assets('css/sweetalert2.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=assets('css/misestilos.css')?>" rel="stylesheet" type="text/css" />
    <title>SISPRODI</title>
    <!-- javascript -->
    <script src="<?=assets('js/axios.min.js')?>"></script>
    <script src="<?=assets('js/sweetalert2.all.min.js')?>"></script>
</head>
<body>
    <!-- Begin page -->
    <div id="wrapper">
        <?=$this->insert('sections/header');?>
        <?=$this->insert('sections/menulateral');?>
        
        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                <div class="container-fluid">   
                <?=$this->section('content') ?>

                <?=$this->insert('sections/footer');?>
                </div>
            </div>
        </div>
    </div>

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- Vendor js -->
    <script src="<?=assets('js/vendor.min.js')?>"></script>

    <!-- Third Party js-->
    <script src="<?=assets('js/jquery.peity.min.js')?>"></script>
    <script src="<?=assets('libs/jquery-knob/jquery.knob.min.js')?>"></script>
    <script src="<?=assets('libs/peity/jquery.peity.min.js')?>"></script>
    <script src="<?=assets('libs/apexcharts/apexcharts.min.js')?>"></script>
    <script src="<?=assets('libs/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?=assets('libs/datatables/dataTables.bootstrap4.js')?>"></script>
    <script src="<?=assets('libs/datatables/dataTables.responsive.min.js')?>"></script>
    <script src="<?=assets('libs/datatables/responsive.bootstrap4.min.js')?>"></script>

    <!-- Dashboard init -->
    <script src="<?=assets('js/pages/dashboard-2.init.js')?>"></script>

    <!-- App js -->
    <script src="<?=assets('js/app.min.js')?>"></script>

    <!-- Axios js -->
    <script src="<?=assets('js/axios.min.js')?>"></script>

</body>
</html>

