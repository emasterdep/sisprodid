<?php $this->layout('layouts/fullwidth') ?>

<div class="container m-auto">
<div class="row mt-5">
    <div class="col-12">
        <h3 class="text-center">DETALLES DEL PACIENTE</h3>
        <div class="card">
            <div class="card-body">
                <div class="col-12 d-flex wrap">
                    <h4 class="col-12">Datos del paciente</h4>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <input type="hidden" name="paciente" id="codigoPaciente" value="<?=$this->e($paciente['codigo_paciente'])?>">
                            <p class=" mb-0 font-13"><strong>Nombre</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Cédula</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['cedula'])?></span></p>        
                        </div>   
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Fecha de nacimiento</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['fecha_nacimiento'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Edad</strong></p>
                            <p><span class="mb-1 text-gray"><?=edadActual($paciente['fecha_nacimiento'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Teléfono</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['telefono'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Sexo</strong></p>
                            <p><span class="mb-1 text-gray"><?=printSexo($paciente['sexo'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Dirección</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombre_sector'])?> - <?=$this->e($paciente['nombre_comunidad'])?>, parroquea <?=$this->e($paciente['nombre_parroquia'])?>, municipio <?=$this->e($paciente['nombre_municipio'])?>, estado - <?=$this->e($paciente['nombre_estado'])?></span></p>        
                        </div>  
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Situación laboral</strong></p>
                            <p><span class="mb-1 text-gray"><?=printSexo($paciente['ocupacion'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                </div>
                <!-- parte de los medicamentos -->
                
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-12 d-flex wrap">
                    <h4 class="col-12">Datos de la vivienda</h4>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Casa:</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['tipo'])?></span></p>   
                        </div>   
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Estructura:</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['estructura'])?></span></p>  
                        </div>   
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Techo:</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['techo'])?></span></p>
                        </div>   
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Piso:</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['piso'])?> </span></p>
                        </div>   
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Número habitaciones:</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['numero_habitaciones'])?> </span></p>  
                        </div>   
                    </div>
                    <div class="text-left mt-3">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <?php if( count($historias) == 0 ): ?>
                <div class="col-12 box-historia">
                    <p class="text-center">Parece que este usuario no tiene historia clinica</p>
                    <div class="col-12 row justify-content-center">
                        <a href="/historia-medica/paciente/<?=$this->e( $paciente['codigo_paciente'] )?>" class="btn btn-primary">Registrar historia clinica</a>
                    </div>
                </div>
                <?php endif; ?>

                <?php if( !historiaIsActived($historias) && count($historias) != 0  ): ?>
                    <div class="col-12 box-historia">
                        <p class="text-center">El paciente no posee historia clinica actual</p>
                        <div class="col-12 row justify-content-center">
                            <a href="/historia-medica/paciente/<?=$this->e( $paciente['codigo_paciente'] )?>" class="btn btn-primary">Registrar nueva historia clinica</a>
                        </div>
                    </div>
                <?php endif; ?>

                <!-- historia activa -->
                <?php foreach( $historias as $historia ): ?>
                    <?php if( $historia['estatus_historia'] == 'on' ): ?>
                    <div class="col-12 box-historia-actual">
                        <!-- cabecera -->
                        <div class="col-12 row justify-content-end">
                            <h5 class="text-primary" style="margin-bottom: -18px;margin-right: -20px;margin-top: -1px;">Historia en curso</h5>
                        </div>
                        <div class="col-12 row">
                            <div class="date bg-primary p-2">
                                <p class="text-white mb-0">Fecha de creación</p>
                                <h3 class="text-white mt-0"><?=$this->e( $historia['creacion_historia'] )?></h3>
                                <p class="text-white mb-0">Hora: <?=$this->e($historia['hora_creacion'])?></p>
                            </div>
                            <div class="datos-generales row p-2">
                                <div class="group-text p-2">
                                    <p class="mb-0">Contacto de emergencia</p>
                                    <h4 class="mt-0"><?=$this->e($historia['contacto_emergencia'])?></h4>
                                </div>
                                <div class="group-text p-2">
                                    <p class="mb-0">Teléfono de emergencia</p>
                                    <h4 class="mt-0"><?=$this->e($historia['telefono_contacto_emergencia'])?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <hr>
                            <h4 class="">Motivo de admisión</h4>
                            <p><?=$this->e( $historia['motivo_admision'] )?></p>
                            <div class="mb-2 col-12"></div>
                            <h4 class="">Enfermedad actual</h4>
                            <p><?=$this->e( $historia['enfermedad_actual'] )?></p>
                            <div class="mb-2 col-12"></div>
                            <h4 class="">Diagnostico admisión</h4>
                            <p><?=$this->e( $historia['diagnostico_adminsion'] )?></p>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endforeach; ?>

                <!-- end #accordions-->
            </div> <!-- end row -->
            </div>
        </div>
    </div>
    
</div>


</div>



