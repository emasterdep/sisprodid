<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicos">Medicos</a></li>
                    <li class="breadcrumb-item active">Editar medico</li>
                </ol>
            </div>
            <h4 class="page-title">Editar Medico</h4>
        </div>
    </div>
</div>     

<?php if( isset($action) && $action == 'repeatcedula' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Cedula repetidad!</strong> Al parecer la cedula que desea introducir esta en uso en el sistema, por favor coloque otra
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Formulario para Editar medico</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/medicos/update" method="POST" class="row">
                            <div class="form-group mb-3 col-md-6">
                                <label for="cedulaMedico">Cedula</label>
                                <input type="number" id="cedulaMedico" name="cedulaMedico" class="form-control" required value="<?=$this->e($medico['cedula']); ?>">
                                <input type="hidden" name="lastCedula" value="<?=$this->e($medico['cedula']); ?>">
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="nameMedico">Nombre</label>
                                <input type="text" id="nameMedico" name="nameMedico" class="form-control" required value="<?=$this->e($medico['nombres']); ?>">
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="lastnameMedico">Apellido</label>
                                <input type="text" id="lastnameMedico" name="lastnameMedico" class="form-control" required value="<?=$this->e($medico['apellidos']); ?>">
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="phone">Número de teléfono</label>
                                <input type="number" id="phone" name="phone" class="form-control" required value="<?=$this->e($medico['telefono']); ?>">
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="datebirth">Fecha de nacimiento</label>
                                <input type="date" id="datebirth" name="datebirth" class="form-control" required value="<?=$this->e($medico['fecha_nacimiento']); ?>">
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="genero">Genero</label>
                                <select name="genero" id="genero" class="form-control" value="<?=$this->e($medico['sexo']); ?>" required>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                                <input type="hidden" name="id_medico" value="<?=$this->e($medico['codigo_persona']); ?>">
                            </div>
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
