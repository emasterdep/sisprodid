<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicos">Medicos</a></li>
                    <li class="breadcrumb-item">Listado de especialidades</li>
                </ol>
            </div>
            <h4 class="page-title">Listado de especialidades</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12 row">
        <div class="col-md-3 col-12">
            <div class="card-box">
                <h4 class="header-title">Crear especialidad</h4>
                <p>Formulario para crear especialidades para medicos</p>
                <hr>
                <form action="/medicos/especialidades/create" method="POST">
                    <label for="servicename">Titulo</label>
                    <input type="text" name="especialidadName" id="servicename" class="form-control">
                    <div class="form-group mt-2">
                    <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-9 col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-12">
                        <h4 class="header-title">Listado de servicios</h4>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php 
                        if( isset($especialidades) ):
                            $i = 0;
                            foreach($especialidades as $especialidad){ ?>
                            <tr>
                                <th scope="row"><?=$this->e($i+1)?></th>
                                <td><?=$this->e($especialidad['nombre_especialidad'])?></td>
                                <td>
                                    <div>
                                        <a href="/medicos/especialidades/delete?especialidad=<?=$this->e($especialidad['codigo_especialidad'])?>" class="btn btn-danger">
                                            <i class="fas fa-trash "></i>
                                        </a>
                                        <a href="/medicos/especialidades/edit?especialidad=<?=$this->e($especialidad['codigo_especialidad'])?>" class="btn btn-warning">
                                            <i class="fas fa-pencil-alt "></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $i++;
                            }
                        endif; ?>
                        </tbody>
                    </table>
                </div>
            </div> <!-- end card-box -->
        </div>
    </div> <!-- end col -->
</div>