<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicos">medicos</a></li>
                    <li class="breadcrumb-item"><a href="/medicos/especialidades">Especialidades</a></li>
                    <li class="breadcrumb-item">Actualizar especialidad</li>
                </ol>
            </div>
            <h4 class="page-title">Actualizar especialidad</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para actualizar especialidad</h4>
                <div class="row">
                    <div class="col-12">
                        <form action="/medicos/especialidades/update" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameState">Nombre</label>
                                <input type="text" id="nameEspecialidad" name="nameEspecialidad" class="form-control" value="<?=$this->e($especialidad['nombre_especialidad'])?>">
                                <input type="hidden" name="idEspecialidad" value="<?=$this->e($especialidad['codigo_especialidad'])?>">
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Actulizar dato</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
