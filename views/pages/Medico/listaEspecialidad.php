<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicos">Medicos</a></li>
                    <li class="breadcrumb-item active"><?=$this->e($medico['nombres'].' '.$medico['apellidos']); ?></li>
                </ol>
            </div>
            <h4 class="page-title">Listado de especialidades</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción exitosa!</strong> Se le ha asignado la especialidad al medico sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['action']) && $_GET['action'] == 'deleteEspecialidad' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción exitosa!</strong> Eliminado la especialidad del medico satisfactoriamente
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>


<div class="row">
<div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Detalles del medico</h4>
                <div class="row col-12">
                    <div class="group col-md-3 col-12">
                        <p class="mb-1">Nombre Apellido</p>
                        <h5><?=$this->e($medico['nombres'].' '.$medico['apellidos']); ?></h5>
                    </div>
                    <div class="group col-md-3 col-12">
                        <p class="mb-1">Cedula</p>
                        <h5><?=$this->e($medico['cedula']); ?></h5>
                    </div>
                    <div class="group col-md-3 col-12">
                        <p class="mb-1">Teléfono</p>
                        <h5><?=$this->e($medico['telefono']); ?></h5>
                    </div>
                    <div class="group col-md-3 col-12">
                        <p class="mb-1">Fecha de nacimiento</p>
                        <h5><?=$this->e($medico['fecha_nacimiento']); ?></h5>
                    </div>
                </div>
            </div>
        </div>
</div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Listado de especialidades</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/medicos/especialidad/asignar" method="POST" class="row">
                            <div class="form-group mb-3 col-md-10 col-12">
                                <input type="hidden" name="medico" value="<?=$this->e($medico['codigo_medico']); ?>">
                                <select name="especialidad" id="especialidad" class="form-control">
                                    <?php 
                                    if( isset($especialidades) ):
                                        foreach($especialidades as $especialidad): ?>
                                    <option value="<?=$especialidad['codigo_especialidad']?>"><?=$especialidad['nombre_especialidad']?></option>
                                    <?php 
                                        endforeach; 
                                    endif;
                                        ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2 col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Agregar a médico</button>
                            </div>
                        </form>
                    </div> <!-- end col -->
                </div>

                <hr>
            <div class="table-responsive">
                <h4 class="header-title mb-3">Especialidades del médico</h4>
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Nombre especialidad</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        if( isset($especialidadesMedico) ): 
                        $i = 0;
                            if( count($especialidadesMedico) == 0): ?>
                            <tr>
                                <td>NO HAY REGISTROS</td>
                            </tr>
                        <?php endif;
                        foreach($especialidadesMedico as $especialidad){ ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($especialidad['nombre_especialidad'])?></td>
                            <td>
                                <div>
                                    <a href="/medicos/especialidad/deleteMedico?code_especialidad=<?=$this->e($especialidad['codigo_especialidad_medico'])?>&medico=<?=$this->e($medico['codigo_medico']); ?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } endif; ?>
                    </tbody>
                </table>
            </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
