<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Medicos</a></li>
                </ol>
            </div>
            <h4 class="page-title">Listado de medicos</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row mb-2">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de Medicos</h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end">
                    <a class="btn btn-outline-primary icon mr-1" href="/medicos/download/descargarxlxs" target="__blank">
                        <i class="mdi mdi-file-excel "></i>
                    </a>
                    <a href="/medicos/create" class="btn btn-primary">+ Crear Medico</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Cedula</th>
                        <th>Teléfono</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        if( isset($medicos) ): 
                        $i = 0;
                            if( count($medicos) == 0): ?>
                            <tr>
                                <td>NO HAY REGISTROS</td>
                            </tr>
                        <?php endif;
                        foreach($medicos as $medico){ ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($medico['nombres'])?></td>
                            <td><?=$this->e($medico['apellidos'])?></td>
                            <td><?=$this->e($medico['cedula'])?></td>
                            <td><?=$this->e($medico['telefono'])?></td>
                            <td>
                                <div>
                                    <a href="/medicos/delete?id_medico=<?=$this->e($medico['codigo_medico'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/medicos/edit?id_medico=<?=$this->e($medico['codigo_medico'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                    <a href="/medicos/<?=$this->e($medico['codigo_medico'])?>" class="btn btn-success">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } endif; ?>
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>