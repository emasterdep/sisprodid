<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicamentos">medicamentos</a></li>
                </ol>
            </div>
            <h4 class="page-title">Listado de Medicamentos</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row mb-2">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de Medicamento</h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end">
                    <a href="/medicamentos/create" class="btn btn-primary">+ Crear Medicamento</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        if( isset($medicinas) ): 
                        $i = 0;
                            if( count($medicinas) == 0): ?>
                            <tr>
                                <td colspan="7" class="text-center">NO HAY REGISTROS</td>
                            </tr>
                        <?php endif;
                        foreach($medicinas as $medicina){ ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><img src="/<?=$this->e($medicina['imagen'])?>" alt="" style="width:70px;"></td>
                            <td><?=$this->e($medicina['nombre_medicamento'])?></td>
                            <td>
                                <div>
                                    <a href="/medicamentos/delete?medicamento=<?=$this->e($medicina['codigo_medicamento'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/medicamentos/edit?medicamento=<?=$this->e($medicina['codigo_medicamento'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } endif; ?>
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>