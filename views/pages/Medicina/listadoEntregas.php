<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item">Entregas medicamentos</li>
                </ol>
            </div>
            <h4 class="page-title">Listado de entregas de medicamentos</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['search']) && $_GET['search'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El paciente que usted busco no se encuentra registrado, por favor verifique si escribio correctamente el número de identificación personal
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row mb-2">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Ultimas entregas realizadas</h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end">
                    <a class="btn btn-outline-primary icon mr-1" href="/medicamentos/entregas/downloadXlxs" target="__blank">
                        <i class="mdi mdi-file-excel "></i>
                    </a>
                    <a href="/pacientes/entrega/seleccionar-paciente" class="btn btn-primary">+ Entregar medicamento</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>Los medicamentos entregados en un periodo mayor de una semana (7 días) no se podran borrar, editar por integridad de la información.</p>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-centered table-borderless table-hover table-nowrap mb-0" id="datatable">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>cedula</th>
                        <th>N° Medicamentos</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        if( isset($entregas) ): 
                        $i = 0;
                            if( count($entregas) == 0): ?>
                            <tr>
                                <td colspan="7" class="text-center">NO HAY REGISTROS</td>
                            </tr>
                        <?php endif;
                        foreach($entregas as $entrega){ ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                                              
                            <td><?=$this->e($entrega['nombres'])?> <?=$this->e($entrega['apellidos'])?> </a></td>
                            <td><?=$this->e($entrega['cedula'])?></td>
                            <td><?=$this->e($entrega['cantidad_entregada'])?> de <?=$this->e($entrega['nombre_medicamento'])?> ...</td>
                            <td><?=$this->e($entrega['date_creation'])?></td>
                            <td>
                                <div>
                                    <a href="/pacientes/entrega/historial?codigo_paciente=<?=$this->e($entrega['codigo_paciente'])?>" class="btn btn-success">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } endif; ?>
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>