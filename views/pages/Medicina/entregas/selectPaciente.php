<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">Entregas medicamentos</a></li>
                    <li class="breadcrumb-item active">Seleccionar paciente (paso 1 de 2)</li>
                </ol>
            </div>
            <h4 class="page-title">Seleccionar paciente (paso 1 de 2)</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-2">Seleccione la acción correspondiente</h4>
                <div class="row col-12">
                    <a href="/pacientes/create" class="btn btn-outline-primary waves-effect waves-light mr-1">
                        <i class="mdi mdi mdi-account "></i> Paciente nuevo
                    </a>
                    <button type="button" id="btn-entrega-existente" class="btn btn-primary waves-effect waves-light">
                        <i class="mdi mdi mdi-account "></i> Paciente existente
                    </button>
                </div>
                <div class="col-12 card-paciente-existe hidden" id="card-paciente-existe">
                    <form action="/medicamentos/entrega" method="GET">
                        <div class="form-group mb-3 col-md-12">
                            <label for="cedulaMedico">Cédula</label>
                            <input type="number" placeholder="Introduzca la cédula del paciente a entregar medicamento.." id="pacienteId" name="cedula" class="form-control" required>
                            <p class="text-success hidden" id="namePaciente">Enrique Sanchez gonzales</p>
                        </div>
                        <div class="form-group col-12 mb-3">
                            <button type="submit" class="btn btn-primary btn-block" id="btnSelect-paciente" disabled="true">Continuar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('pacienteId').addEventListener('blur',()=>{
        //verifica que no este vacio
        if( document.getElementById('pacienteId').value.length == 0){
            document.getElementById('namePaciente').classList.add('hidden');
        } else{
            axios.get('/pacientes/find-json-paciente?cedula='+document.getElementById('pacienteId').value)
            .then(function (response) {
                    return response.data;
            })
            .then(function(value){ console.log(value)
                document.getElementById('namePaciente').classList.remove('hidden');
                if( value.cedula !== undefined ){
                    document.getElementById('namePaciente').innerHTML = `Paciente: ${value.nombres} ${value.apellidos} registrado el ${value.date_creation}`;
                    document.getElementById('namePaciente').classList.add('text-success');
                    document.getElementById('namePaciente').classList.remove('text-danger');
                    setTimeout(() => {
                        document.getElementById('btnSelect-paciente').removeAttribute('disabled');
                    },2000);
                } else{
                    document.getElementById('namePaciente').innerHTML = value;
                    document.getElementById('namePaciente').classList.remove('text-success');
                    document.getElementById('namePaciente').classList.add('text-danger');
                    document.getElementById('btnSelect-paciente').setAttribute('disabled','true');
                }
            })
            .catch(function (error) {
                    console.error(error);       
            });
        }
    });

    document.getElementById('btn-entrega-existente').addEventListener('click',()=>{
        document.getElementById('card-paciente-existe').classList.remove('hidden');
        document.getElementById('card-creation-paciente').classList.add('hidden');
    });
    
</script>
