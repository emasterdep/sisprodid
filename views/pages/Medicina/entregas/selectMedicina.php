<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">Entregas medicamentos</a></li>
                    <li class="breadcrumb-item active">Seleccionar Medicamentos (paso 1 de 2)</li>
                </ol>
            </div>
            <h4 class="page-title">Seleccionar Medicamentos (paso 2 de 2)</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-12 d-flex wrap">
                    <h4 class="col-12">Datos del paciente</h4>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <input type="hidden" name="paciente" id="codigoPaciente" value="<?=$this->e($paciente['codigo_paciente'])?>">
                            <p class=" mb-0 font-13"><strong>Nombre</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Cédula</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['cedula'])?></span></p>        
                        </div>   
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Fecha de nacimiento</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['fecha_nacimiento'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Edad</strong></p>
                            <p><span class="mb-1 text-gray"><?=edadActual($paciente['fecha_nacimiento'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Teléfono</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['telefono'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Sexo</strong></p>
                            <p><span class="mb-1 text-gray"><?=printSexo($paciente['sexo'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Dirección</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombre_sector'])?> - <?=$this->e($paciente['nombre_comunidad'])?>, parroquea <?=$this->e($paciente['nombre_parroquia'])?>, municipio <?=$this->e($paciente['nombre_municipio'])?>, estado - <?=$this->e($paciente['nombre_estado'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                </div>
                <!-- parte de los medicamentos -->
                <div class="col-12 d-flex wrap mt-3" style="background: #fbfbfb;padding: 20px;border: 1px solid lightgray;">
                    <h4 class="col-12">Lista de medicamentos a entregar</h4>
                    <div class="col-12">
                        <hr>
                        <div class="col-12 mb-2">
                            <table class="table table-centered table-borderless table-hover table-nowrap mb-0">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Medicamento</th>
                                        <th>Presentación</th>
                                        <th>Fecha caducidad</th>
                                        <th>Cantidad a dar</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody id="list-medicinas">
                                    <tr>
                                        <th colspan="5"> <p class="text-center">No hay medicamentos seleccionados</p> 
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 row justify-content-end">
                        <button id="growupModal" class="btn btn-outline-primary waves-effect waves-light" data-toggle="modal" data-target="#listado-medicamentos">
                            Agregar medicamento
                        </button>
                    </div>
                    <div class="col-12">
                        <div class="form-group mb-2 mt-3 col-md-12">
                            <label for="observacion">Observación</label>
                            <textarea id="observacion" cols="30" rows="5" class="form-control" placeholder="Coloque una observación..."></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex wrap justify-content-end mt-2 mb-2">
                    <button onclick="sendDataEntrega()" class="btn btn-primary waves-effect waves-light">Entregar medicamentos</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal listado de pacientes -->
<div id="listado-medicamentos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Listado de medicamentos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <p>Se listarán los medicamentos que estan disponibles, verifique por el panel de lotes de medicamentos si tiene suficiente</p>
                    <div class="table-responsive">
                    <table class="table table-centered table-borderless table-hover table-nowrap mb-0 table-responsive" id="datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>Medicamento</th>
                                <th>Existencia</th>
                                <th>Presentación</th>
                                <th>Fecha caducidad</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                if( isset($lotes) ): 
                                    if( count($lotes) == 0): ?>
                                    <tr>
                                        <td>NO HAY REGISTROS</td>
                                    </tr>
                                <?php endif;
                                foreach($lotes as $medicina){ ?>
                                <tr id="<?=$this->e($medicina['codigo_lote'])?>">
                                    <td class="nombre"><?=$this->e($medicina['nombre_medicamento'])?></td>
                                    <td class="cantidad"><?=$this->e($medicina['cantidad_existencia'])?></td>
                                    <td class="presentacion"><?=$this->e($medicina['presentacion'])?></td>
                                    <td class="vencimiento"><?=$this->e($medicina['fecha_vencimiento'])?></td>
                                    <td>
                                        <div>
                                            <a Onclick="createPedido('<?=$this->e($medicina['codigo_lote'])?>')" medicina="<?=$this->e($medicina['codigo_lote'])?>" href="javascript:void(0)" class="btn btn-primary boton-medicinas text-white">
                                                Seleccionar
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                } endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->


<script>
    window.listaMedicinas = [];

    function createPedido(element){
        
        var idRowMedicina = element; console.log(element);
        if( verifyExist(idRowMedicina) ){
            let medicina = {
                codigo: idRowMedicina,
                nombre: document.querySelector(`#${idRowMedicina} .nombre`).innerHTML,
                cantidad: 0,
                vencimiento: document.querySelector(`#${idRowMedicina} .vencimiento`).innerHTML,
                presentacion: document.querySelector(`#${idRowMedicina} .presentacion`).innerHTML
            }
            window.listaMedicinas.push(medicina);
            console.log(medicina);
            drawListMedicinas();
        }
        $('#listado-medicamentos').modal('hide');
    } 

    function verifyExist(comparador)
    {
        if(window.listaMedicinas.length == 0){
            return true;
        } else{
            let centinela = true;
            window.listaMedicinas.forEach(element=>{
                if(element.codigo == comparador){
                    centinela = false;
                }
            });
            return centinela;
        }
    }

    function drawListMedicinas()
    {
        let lista = '';
        if(Object.keys(window.listaMedicinas).length == 0){
            document.getElementById('list-medicinas').innerHTML = '<tr><td colspan="5"><p class="text-center">No hay medicamentos seleccionados</p></td></tr>';
        }else{
            window.listaMedicinas.forEach(element=>{
                lista += `
                <tr>
                    <th>${element.nombre}</th>
                    <th>${element.presentacion}</th>
                    <th>${element.vencimiento}</th>
                    <th><input onchange="updateCantidadMedicina('${element.codigo}')" type="number" id="${element.codigo}-cantidad" min="0" class="form-control" value="1"></th>
                    <th>
                        <a onclick="deleteMedicina('${element.codigo}')" href="javascript:void(0)" class="btn btn-danger mr-1 eliminar-medicamento">
                            <i class="fas fa-trash "></i>
                        </a>
                    </th>
                </tr>`;
            });
            document.getElementById('list-medicinas').innerHTML = lista;
        }
    }

    function updateCantidadMedicina(medicina)
    {
        window.listaMedicinas.forEach((element,index)=>{ console.log(index);
            if(element.codigo == medicina){
                window.listaMedicinas[index].cantidad = document.getElementById(medicina+'-cantidad').value;
            }
        });
    }

    function deleteMedicina(medicina)
    {
        window.listaMedicinas.forEach((element,index)=>{ console.log(index);
            if(element.codigo == medicina){
                delete window.listaMedicinas[index];
            }
        });
        drawListMedicinas();
    }

    function sendDataEntrega()
    {
        var bodyFormData = new FormData();

        bodyFormData.set('observacion',document.getElementById('observacion').value);
        bodyFormData.set('paciente',document.getElementById('codigoPaciente').value);

        let entregasMedicina = "";
        entregasMedicina += "[";
        window.listaMedicinas.forEach((element,index)=>{ console.log(index);
            entregasMedicina += `{"codigo":"${element.codigo}","cantidad":"${element.cantidad}"}`;
            if(window.listaMedicinas[index+1] !== undefined){ entregasMedicina += ","; }
        });
        entregasMedicina += "]";
        bodyFormData.set('medicinas',entregasMedicina);

        axios({
            method: 'post',
            url: '/medicamentos/entregar-paciente',
            data: bodyFormData,
            headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (response) {
            //handle success
            var location = window.location.protocol+'//'+window.location.host+'/pacientes/entrega-medicamento?action=success';
            console.log(location);
            window.location = location;
            console.log(response);
        })
        .catch(function (response) {
            //handle error
            console.log(response);
        });
    }

</script>
