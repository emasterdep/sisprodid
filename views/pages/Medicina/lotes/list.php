<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item">Lotes de medicamentos</li>
                </ol>
            </div>
            <h4 class="page-title">Lotes de medicamentos</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row mb-2">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Lotes de Medicamento</h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end">
                    <a class="btn btn-outline-primary icon mr-1" href="/medicamentos/lotes/descargarxlxs" target="__blank">
                        <i class="mdi mdi-file-excel "></i>
                    </a>
                    <a href="/medicamentos/lotes/create" class="btn btn-primary">+ Registrar lote</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>medicamento</th>
                        <th>Almacen</th>
                        <th>cantidad</th>
                        <th>Presentación</th>
                        <th>fecha emisión</th>
                        <th>fecha de caducación</th>
                        <th>acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        if( isset($loteMedicamneto) ): 
                        $i = 0;
                            if( count($loteMedicamneto) == 0): ?>
                            <tr>
                                <td colspan="7" class="text-center">NO HAY REGISTROS</td>
                            </tr>
                        <?php endif;
                        foreach($loteMedicamneto as $lote){ ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($lote['nombre_medicamento'])?></td>
                            <td><?=$this->e($lote['ubicacion'])?></td>
                            <td><?=$this->e($lote['cantidad_existencia'])?></td>
                            <td><?=$this->e($lote['presentacion'])?></td>
                            <td><?=$this->e($lote['fecha_emision'])?></td>
                            <td><?=$this->e($lote['fecha_vencimiento'])?></td>
                            <td>
                                <div>
                                    <a href="/medicamentos/lotes/delete?lote=<?=$this->e($lote['codigo_lote'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/medicamentos/lotes/edit?lote=<?=$this->e($lote['codigo_lote'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } endif; ?>
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>