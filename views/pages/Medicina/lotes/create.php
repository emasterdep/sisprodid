<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicamentos">Lote de medicamentos</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registrar lote</a></li>
                </ol>
            </div>
            <h4 class="page-title">Registrar lote de medicamentos</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'noisimage' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>El archivo que selecciono no es una imagen!</strong> por favor, seleccione una imagen de tipo 'png','jpg','jpeg'
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['action']) && $_GET['action'] == 'imageflag' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>La imagen es muy pesada!</strong> por favor, seleccione una imagen con un tamaño menor a 5 megasbytes
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Registrar lote de medicamento</h4>
                <div class="row">
                    <div class="col-12">
                        <form action="/medicamentos/lotes/save" method="POST" class="row">
                            <div class="form-group mb-3 col-md-12">
                                <label for="medicamento_id">Medicamento a registrar</label>
                                <select name="medicamento_id" id="medicamento_id" class="form-control">
                                    <?php foreach($medicinas as $medicina): ?>
                                    <option value="<?=$this->e($medicina['codigo_medicamento']);?>"><?=$this->e($medicina['nombre_medicamento']);?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="almacen_id">Almacen a guardar</label>
                                <select name="almacen_id" id="almacen_id" class="form-control">
                                <?php foreach($almacenes as $almacen): ?>
                                    <option value="<?=$this->e($almacen['codigo_almacen']);?>"><?=$this->e($almacen['titulo']);?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="date_emision">Fecha de emisión</label>
                                <input type="date" name="date_emision" id="date_emision" class="form-control">
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="date_vencimiento">Fecha de vencimiento</label>
                                <input type="date" name="date_vencimiento" id="date_vencimiento" class="form-control">
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="presentacion">Presentación</label>
                                <select name="presentacion" id="presentacion" class="form-control">
                                    <option value="Frasco">Frasco</option>
                                    <option value="Caja">Caja</option>
                                    <option value="Tabletas">Tabletas</option>
                                    <option value="Unidad">Unidad</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="cantidad">Cantidad</label>
                                <input type="number" name="cantidad" id="cantidad" class="form-control">
                            </div>
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
