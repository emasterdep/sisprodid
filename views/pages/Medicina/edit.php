<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicamentos">medicamentos</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Editar medicamento</a></li>
                </ol>
            </div>
            <h4 class="page-title">Editar medicamento</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'noisimage' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>El archivo que selecciono no es una imagen!</strong> por favor, seleccione una imagen de tipo 'png','jpg','jpeg'
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['action']) && $_GET['action'] == 'imageflag' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>La imagen es muy pesada!</strong> por favor, seleccione una imagen con un tamaño menor a 5 megasbytes
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Formulario Medicamento</h4>
                <div class="row">
                    <div class="col-12">
                        <form action="/medicamentos/update" method="POST" enctype="multipart/form-data" class="row">
                            <div class="form-group mb-3 col-md-12">
                                <label for="tituloMedicamento">Titulo</label>
                                <input value="<?=$this->e($medicina['nombre_medicamento'])?>" type="text" id="tituloMedicamento" name="tituloMedicamento" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <div class="img-last">
                                    <img src="/<?=$this->e($medicina['imagen'])?>" alt="" style="width: 30%;margin-bottom: 20px;">
                                </div>
                                <label for="ubicacionAlmacen">Imagen de referencia</label>
                                <input type="file" name="imgMedicamento" id="">
                                <input type="hidden" name="imgMedicamentoAntigua" value="<?=$this->e($medicina['imagen'])?>">
                                <input type="hidden" name="id_medicina" value="<?=$this->e($medicina['codigo_medicamento'])?>">
                            </div>
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
