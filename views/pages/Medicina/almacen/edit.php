<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicamentos">medicamentos</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Editar Almacenes</a></li>
                </ol>
            </div>
            <h4 class="page-title">Editar almacen</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'repeattitulo' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Titulo de almacen ya existe!</strong> introdusca otro titulo para este almacen ya que existe un titulo parecido
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Formulario Almacen</h4>
                <p>Un almacen posee secciones donde se colocaran los lotes/cajas de medicamentos para su posterior entrega. Las secciones totales son entonces la capacidad (en número) de secciones para lotes de medicina. Las secciones disponibles son la cantidad de secciones que no estan siendo usadas en este momento.</p>
                <div class="row">
                    <div class="col-12">
                        <form action="/medicamentos/almacenes/update" method="POST" class="row">
                            <div class="form-group mb-3 col-md-12">
                                <label for="tituloAlmacen">Titulo</label>
                                <input value="<?=$this->e($almacen['titulo']); ?>" type="text" id="tituloAlmacen" name="tituloAlmacen" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="ubicacionAlmacen">Ubicación</label>
                                <input value="<?=$this->e($almacen['ubicacion']); ?>" type="text" id="ubicacionAlmacen" name="ubicacionAlmacen" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="seccionesTotales">Secciones totales</label>
                                <input value="<?=$this->e($almacen['disponibilidad_total']); ?>" type="number" id="seccionesTotales" name="seccionesTotales" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="seccionesDisponibles">Secciones disponibles</label>
                                <input value="<?=$this->e($almacen['disponible']); ?>" type="number" id="seccionesDisponibles" name="seccionesDisponibles" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="anchoAlmacen">Ancho en metros</label>
                                <input value="<?=$this->e($almacen['ancho']); ?>" type="text" id="anchoAlmacen" name="anchoAlmacen" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="largoAlmacen">Largo en metros</label>
                                <input value="<?=$this->e($almacen['largo']); ?>" type="text" id="largoAlmacen" name="largoAlmacen" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="tipoAlmacen">Tipo</label>
                                <select name="tipoAlmacen" id="tipoAlmacen" class="form-control" required value="<?=$this->e($almacen['tipo']); ?>">
                                    <option value="Habitacion">Habitacion</option>
                                    <option value="Container">Container</option>
                                </select>
                            </div>
                            <div class="form-group col-12 mb-3">
                            <input value="<?=$this->e($almacen['codigo_almacen']); ?>" type="hidden" name="id_almacen">
                                <button type="submit" class="btn btn-primary btn-block">Guardar cambios</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
