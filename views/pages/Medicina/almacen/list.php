<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/medicamentos">medicamentos</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Almacenes</a></li>
                </ol>
            </div>
            <h4 class="page-title">Listado de almacenes</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row mb-2">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de Almacenes</h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end">
                    <a href="/medicamentos/almacenes/create" class="btn btn-primary">+ Crear Almacen</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Ubicación</th>
                        <th>Secciones disponibles</th>
                        <th>Total de secciones</th>
                        <th>Medidas </th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        if( isset($almacenes) ): 
                        $i = 0;
                            if( count($almacenes) == 0): ?>
                            <tr>
                                <td colspan="7" class="text-center">NO HAY REGISTROS</td>
                            </tr>
                        <?php endif;
                        foreach($almacenes as $almacen){ ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($almacen['titulo'])?></td>
                            <td><?=$this->e($almacen['ubicacion'])?></td>
                            <td><?=$this->e($almacen['disponible'])?></td>
                            <td><?=$this->e($almacen['disponibilidad_total'])?></td>
                            <td><?=$this->e($almacen['ancho'])?> M (ancho) X <?=$this->e($almacen['largo'])?> M (largo)</td>
                            <td>
                                <div>
                                    <a href="/medicamentos/almacenes/delete?almacen=<?=$this->e($almacen['codigo_almacen'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/medicamentos/almacenes/edit?almacen=<?=$this->e($almacen['codigo_almacen'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } endif; ?>
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>