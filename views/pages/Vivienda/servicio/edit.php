<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/viviendas">Vivienda</a></li>
                    <li class="breadcrumb-item"><a href="/viviendas/service">Servicios</a></li>
                    <li class="breadcrumb-item">Actualizar servicio</li>
                </ol>
            </div>
            <h4 class="page-title">Actualizar servicio</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para actualizar de servicio</h4>
                <div class="row">
                    <div class="col-12">
                        <form action="/viviendas/service/update" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameState">Nombre</label>
                                <input type="text" id="nameService" name="nameService" class="form-control" value="<?=$this->e($servicio['nombre_servicio'])?>">
                                <input type="hidden" name="idService" value="<?=$this->e($servicio['codigo_servicio'])?>">
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Actulizar dato</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
