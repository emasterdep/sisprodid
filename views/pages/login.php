<?php $this->layout('layouts/guest') ?>
        <div class="account-pages mt-5 mb-5" >
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7 ">
                        
                   

                    </div>
                    <div class="col-md-5 ">
                        <div class="card">

                            <div class="card-body p-4">
                                <?php if( isset($_GET['status']) ){ ?>
                                <div class="alert alert-danger" role="alert">
                                <a href="javascript:void(0)" class="alert-link">Noticia:</a> Usuario o contraseña incorrecta
                                </div>
                                <?php } ?>
                                <?php if( isset($_GET['permise']) ){ ?>
                                <div class="alert alert-warning" role="alert">
                                <a href="javascript:void(0)" class="alert-link">Noticia:</a> Para acceder al contenido deberá estar logueado
                                </div>
                                <?php } ?>
                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="<?=assets('img/logo-light.png')?>" alt="" height="26"></span>
                                    </a>
                                    <p class="text-muted mb-4 mt-3">Ingrese su dirección de correo electrónico y contraseña para acceder al panel de administración.</p>
                                </div>

                                <h5 class="auth-title">Entrar al sistema</h5>

                                <form action="/login" method="POST">

                                    <div class="form-group mb-3">
                                        <label for="emailaddress">Dirección de correo electrónico</label>
                                        <input class="form-control" type="email" name="email" id="emailaddress" required="" placeholder="Ingresa tu email">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="password">Contraseña</label>
                                        <input class="form-control" type="password" name="password" required="" id="password" placeholder="Contraseña del usuario">
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-primary btn-block" type="submit"> Iniciar sesión </button>
                                    </div>

                                </form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <!-- end row -->

                    </div> <!-- end col -->


                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <footer class="footer footer-alt">
            <span><img src="<?=assets('img/uptos.png')?>" alt="" height="50"></span><br>
            <?=date('Y')?> &copy; Sisprodi - Sistema Para Departamento de Diabetes  
        </footer>
        
    </body>
</html>