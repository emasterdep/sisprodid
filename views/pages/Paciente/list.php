<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item">Pacientes</li>
                </ol>
            </div>
            <h4 class="page-title">Listado de paciente</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['search']) && $_GET['search'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El paciente que usted busco no se encuentra registrado, por favor verifique si escribio correctamente el número de identificación personal
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row mb-2">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de pacientes</h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end">
                    <a class="btn btn-outline-primary icon mr-1" href="/pacientes/descargarxlxs" target="__blank">
                        <i class="mdi mdi-file-excel "></i>
                    </a>
                    <a href="/pacientes/create" class="btn btn-primary">+ Registrar paciente</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-centered table-borderless table-hover table-nowrap mb-0" id="datatable">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>cedula</th>
                        <th>Genero</th>
                        <th>Fecha de nacimiento</th>
                        <th>Teléfono</th>
                        <th>Tipo diabetes</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        if( isset($pacientes) ): 
                        $i = 0;
                            if( count($pacientes) == 0): ?>
                            <tr>
                                <td colspan="7" class="text-center">NO HAY REGISTROS</td>
                            </tr>
                        <?php endif;
                        foreach($pacientes as $paciente){ ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                                              
                            <td><a href="/pacientes/<?=$this->e($paciente['codigo_paciente'])?>"><?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?> </a></td>
                            <td><?=$this->e($paciente['cedula'])?></td>
                            <td><?=$this->e($paciente['sexo'])?></td>
                            <td><?=$this->e($paciente['fecha_nacimiento'])?></td>
                            <td><?=$this->e($paciente['telefono'])?></td>
                            <td><?=$this->e($paciente['tipo_diabetes'])?></td>
                            <td>
                                <div>
                                    <a href="/pacientes/delete?paciente=<?=$this->e($paciente['codigo_paciente'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/pacientes/edit-data?paciente=<?=$this->e($paciente['codigo_paciente'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                    <a href="/pacientes/<?=$this->e($paciente['codigo_paciente'])?>" class="btn btn-success">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="/medicamentos/entrega?cedula=<?=$this->e($paciente['cedula'])?>" class="btn btn-primary text-white">
                                        Entregar medicamentos
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                        } endif; ?>
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>