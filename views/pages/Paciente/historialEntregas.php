<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">Paciente</a></li>
                    <li class="breadcrumb-item active">Historial de entregas de medicamentos</li>
                </ol>
            </div>
            <h4 class="page-title">Historial de entregas de medicamentos</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-12 d-flex wrap">
                    <h4 class="col-12">Datos del paciente</h4>
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <input type="hidden" name="paciente" id="codigoPaciente" value="<?=$this->e($paciente['codigo_paciente'])?>">
                            <p class=" mb-0 font-13"><strong>Nombre</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Cédula</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['cedula'])?></span></p>        
                        </div>   
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Fecha de nacimiento</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['fecha_nacimiento'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Edad</strong></p>
                            <p><span class="mb-1 text-gray"><?=edadActual($paciente['fecha_nacimiento'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Teléfono</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['telefono'])?></span></p>        
                        </div>   
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Sexo</strong></p>
                            <p><span class="mb-1 text-gray"><?=printSexo($paciente['sexo'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                    <div class="col-12 col-md-3">
                        <div class="group-text">
                            <p class=" mb-0 font-13"><strong>Dirección</strong></p>
                            <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombre_sector'])?> - <?=$this->e($paciente['nombre_comunidad'])?>, parroquea <?=$this->e($paciente['nombre_parroquia'])?>, municipio <?=$this->e($paciente['nombre_municipio'])?>, estado - <?=$this->e($paciente['nombre_estado'])?></span></p>        
                        </div>  
                    </div><!-- fin div -->
                </div>
                <!-- parte de los medicamentos -->
                
            </div>
        </div>
    </div>
</div>

<!-- entregas de medicamentos -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-12 d-flex wrap">
                    <h4 class="col-12 mb-3">Historial de entregas</h4>
                    <p>Las entregas no pueden editarse, puedes actualizar la correción de la falla a la hora de entregar, por ende te recomendamos validar los datos antes de enviarlos</p>
                    <?php foreach($entregas as $entrega): ?>
                        <div class="col-12 card-historia-entregas">
                            <div class="col-12 col-md-2">
                                <div class="group-text">
                                    <p class=" mb-0 font-13"><strong>Fecha</strong></p>
                                    <p><span class="mb-1 text-gray"><?=$this->e($entrega['date_creation'])?></span></p>        
                                </div>   
                            </div>
                            <div class="col-12 col-md-5">
                                <div class="group-text">
                                    <p class=" mb-0 font-13"><strong>Observación</strong></p>
                                    <p><span class="mb-1 text-gray"><?=$this->e($entrega['observacion'])?></span></p>        
                                </div>   
                            </div>
                            <div class="col-12 col-md-5">
                                <?php if($entrega['status_falla'] !== null && $entrega['status_falla'] !== 'no'): ?>
                                <div class="group-text">
                                    <p class=" mb-0 font-13 text-danger"><strong>Correción de falla</strong></p>
                                    <p><span class="mb-1 text-gray" id="fallatext<?=$this->e($entrega['codigo_entrega'])?>"><?=$this->e($entrega['actualizacion_falla'])?></span></p>        
                                </div>   
                                <?php endif; ?>
                                <div class="d-flex justify-content-end">
                                    <button onclick="editFallaModal('<?=$this->e($entrega['codigo_entrega'])?>')" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#correcion-falla-entrega">Editar entrega <i class="fas fa-pencil-alt"></i></button>
                                </div>
                            </div>
                            <div class="col-12">
                                <hr>
                                <h5>Lista medicamentos</h5>
                                <ul class="lista-medicinas-entrega">
                                    <?php foreach(getMedicamentos($entrega['codigo_entrega']) as $medicamento): ?>
                                    <li><strong><?=$this->e($medicamento['nombre_medicamento'])?></strong> - presentación <?=$this->e($medicamento['presentacion'])?> | cantidad entrega: <strong><?=$this->e($medicamento['cantidad_entregada'])?></strong> </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!-- parte de los medicamentos -->

            </div>
        </div>
    </div>
</div>

<!-- modal correcion falla -->
<div id="correcion-falla-entrega" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar información de entrega</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <form method="POST" action="/medicamentos/entrega/edit">
                <input type="hidden" name="pacienteCode" value="<?=$this->e($paciente['codigo_paciente'])?>">
                <input type="hidden" id="codigoEntregaMedicamento" name="codigoEntregaMedicamento" value="">
                <p>Por la seguridad de la información no es posible editar el registro principal, por ello puede escribir la actualización de la entrega para saber que evento inesperado ocurrio, sin embargo la información principal del mismo no será editada.</p>
                <div class="form-group mb-3 col-md-12">
                    <label for="descripcionFallaCorrecion"><strong>Describa la correción al error</strong></label>
                    <textarea name="descripcionFallaCorrecion" id="descripcionFallaCorrecion" cols="30" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12 row justify-content-end">
                    <button class="btn btn-primary" type="submit">Corregir falla</button>
                </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
<script>

 function editFallaModal(codigo){ console.log(codigo);
    document.getElementById('codigoEntregaMedicamento').value = codigo; 
    if(document.getElementById('fallatext'+codigo) != undefined || document.getElementById('fallatext'+codigo) != null ){
        var fallaText = document.getElementById('fallatext'+codigo).innerHTML;
        document.getElementById('descripcionFallaCorrecion').innerHTML = fallaText;
    }
 }

</script>
