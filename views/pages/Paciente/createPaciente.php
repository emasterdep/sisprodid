<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">pacientes</a></li>
                    <li class="breadcrumb-item active">Registrar paciente (paso 1 de 2)</li>
                </ol>
            </div>
            <h4 class="page-title">Registrar paciente (paso 1 de 2)</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'repeatcedula' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Medico existe!</strong> Al parecer el medico que desea ingresar ya existe en el sistema
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <h4 class="header-title mb-3">Datos personales</h4>
                    </div>
                    <div class="col-md-6 col-12 row justify-content-end">
                        <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">
                            <i class="mdi mdi-plus-circle"></i> Registrar a partir de usuario
                        </button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <form action="/pacientes/savePersona" method="POST" class="row">
                            <div class="form-group mb-3 col-md-6">
                                <label for="cedulaMedico">Cedula</label>
                                <input type="number" id="cedulaPaciente" name="cedulaPaciente" class="form-control" required>
                                <p class="text-danger hidden" id="alertCedula">La persona ya existe en el sistema</p>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="nameMedico">Nombre</label>
                                <input type="text" id="nameMedico" name="nameMedico" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="lastnameMedico">Apellido</label>
                                <input type="text" id="lastnameMedico" name="lastnameMedico" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="phone">Número de teléfono</label>
                                <input type="number" id="phone" name="phone" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="datebirth">Fecha de nacimiento</label>
                                <input type="date" id="datebirth" name="datebirth" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="genero">Genero</label>
                                <select name="genero" id="genero" class="form-control" required>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="diabetes">Diabetes</label>
                                <select name="diabetes" id="diabetes" class="form-control" required>
                                    <option value="1">Tipo 1</option>
                                    <option value="2">Tipo 2</option>
                                    <option value="3">Gestacional</option>
                                    <option value="4">Otro</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="glucometro">Posee glucometro</label>
                                <select name="glucometro" id="glucometro" class="form-control" required>
                                    <option value="si">Si</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="fumador">Fumador</label>
                                <select name="fumador" id="fumador" class="form-control" required>
                                    <option value="si">Si</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="ocupacion">Estado laboral</label>
                                <select name="ocupacion" id="ocupacion" class="form-control" required>
                                    <option value="Empleado publico">Empleado publico</option>
                                    <option value="Empleado privado">Empleado privado</option>
                                    <option value="Trabajador independiente">Trabajador independiente</option>
                                    <option value="Desempleado">Desempleado</option>
                                </select>
                            </div>
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block" id="btnSendPersonaPaciente">Continuar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>

<!-- modal listado de pacientes -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Seleccione la persona previamente registrada</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="col-12">
                    <div class="form-group mb-3 col-md-12">
                        <label for="diabetes">Seleccione el tipo de diabetes</label>
                        <select name="diabetes" class="form-control" id="tipo-diabetes">
                            <option value="1">Tipo 1</option>
                            <option value="2">Tipo 2</option>
                            <option value="3">Gestacional</option>
                            <option value="4">Otro</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-centered table-borderless table-hover table-nowrap mb-0" id="datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Cedula</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                if( isset($personas) ): 
                                    if( count($personas) == 0): ?>
                                    <tr>
                                        <td>NO HAY REGISTROS</td>
                                    </tr>
                                <?php endif;
                                foreach($personas as $persona){ ?>
                                <tr>
                                    <td><?=$this->e($persona['nombres'])?></td>
                                    <td><?=$this->e($persona['apellidos'])?></td>
                                    <td><?=$this->e($persona['cedula'])?></td>
                                    <td>
                                        <div>
                                            <a paciente="<?=$this->e($persona['codigo_persona'])?>" href="javascript:void(0)" class="btn btn-primary paciente-seleccionar text-white">
                                                Seleccionar
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                } endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

<script>
    document.getElementById('cedulaPaciente').addEventListener('blur',()=>{
        axios.get('/pacientes/find-persona?persona='+document.getElementById('cedulaPaciente').value)
        .then(function (response) {
                return response.data;
        })
        .then(function(value){ console.log(value)
            if(value == true){
                document.getElementById('alertCedula').classList.remove('hidden');
                document.getElementById('btnSendPersonaPaciente').setAttribute('disabled','true');
            }else{
                document.getElementById('alertCedula').classList.add('hidden');
                document.getElementById('btnSendPersonaPaciente').removeAttribute('disabled');
            }
        })
        .catch(function (error) {
                console.error(error);       
        });
    });

    var usuarios = document.querySelectorAll('.paciente-seleccionar');
    usuarios.forEach(element=>{
        element.addEventListener('click',(e)=>{
            console.log(e.srcElement.getAttribute('paciente'));
            var location = window.location.protocol+'//'+window.location.host+'/pacientes/select-persona-create?user='+e.srcElement.getAttribute('paciente')+'&diabetes='+document.getElementById('tipo-diabetes').value;
            console.log(location);
            window.location = location;
        });
    });
        
</script>
