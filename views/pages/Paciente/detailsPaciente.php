<?php $this->layout('layouts/app') ?>

 <!-- start page title -->


<div class=" header-dashboard3">
   
        <div class="jumbotron" style="background: url('<?=assets('img/diabetes.jpg')?>') no-repeat ; background-size : cover; <?php if( $entrega !== null){ echo 'padding:1rem 2rem;'; } ?>  ">
            <div class="col-12 d-flex wrap justify-content-end">
                <?php if( $entrega !== null): ?>
                <div class="col-12 col-md-4 card-box mb-0">
                    <h5>Ultima entrega de medicamento</h5>
                    <div class="d-flex wrap col-12">
                        <div class="col-12">
                            <div class="group-text">
                                <p class=" mb-0 font-13"><strong>Fecha</strong></p><?php  ?>
                                <p><span class="mb-1 text-gray"><?=$this->e($entrega['date_creation'])?></span></p>        
                            </div>   
                        </div>
                        <div class="col-12">
                            <div class="group-text">
                                <p class=" mb-0 font-13"><strong>Medicamentos</strong></p>
                                <p><span class="mb-1 text-gray"><?php echo getTotalMedicamento( getMedicamentos($entrega['codigo_entrega']) ); ?></span></p>        
                            </div>   
                        </div>
                    </div>
                    <hr>
                    <a href="/pacientes/entrega/historial?codigo_paciente=<?=$this->e($paciente['codigo_paciente'])?>" class="btn text-success">Ver historial de entregas</a>
                </div>
                <?php endif; ?>
            </div>
        </div>
 
</div> 

<div class="row">
    <div class="col-lg-4 col-xl-4">
        <div class="card-box" style="margin-top: -120px;">
            <div class="col-12 flex-center justify-content-center">
                <img src="<?=assets('img/user-1.jpg')?>" class="rounded-circle img-thumbnail" alt="profile-image">
            </div>
            

            <h4 class="mb-0 text-center"><?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?></h4>
            <p class="text-muted text-center">Diabetes tipo <?=$this->e($paciente['tipo_diabetes'])?></p>

            <div class="text-left mt-3">
                <p class=" mb-0 font-13"><strong>Cedula:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['cedula'])?></span></p>

                <p class=" mb-0 font-13"><strong>Teléfono:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['telefono'])?></span></p>

                <p class=" mb-0 font-13"><strong>Fecha de nacimiento:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['fecha_nacimiento'])?></span></p>

                <p class=" mb-0 font-13"><strong>Genero:</strong></p>
                <p><span class="mb-1 text-gray"> <?php if($paciente['sexo'] == 'M') { echo 'Masculino'; } else{ echo 'Femenino'; } ?></span></p>

                <p class=" mb-0 font-13"><strong>Fumador:</strong></p>
                <p><span class="mb-1 text-gray"> <?=$this->e($paciente['glucometro'])?> es fumador</span></p>

                <p class=" mb-0 font-13"><strong>Glucometro:</strong></p>
                <p><span class="mb-1 text-gray"> <?=$this->e($paciente['glucometro'])?> posee glucometro</span></p>

                <p class=" mb-0 font-13"><strong>Situación laboral:</strong></p>
                <p><span class="mb-1 text-gray"> <?=$this->e($paciente['ocupacion'])?></span></p>

                <p class=" mb-0 font-13"><strong>Dirección:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombre_sector'])?> - <?=$this->e($paciente['nombre_comunidad'])?>, parroquea <?=$this->e($paciente['nombre_parroquia'])?>, municipio <?=$this->e($paciente['nombre_municipio'])?>, estado - <?=$this->e($paciente['nombre_estado'])?></span></p>
            </div>
            <a class="btn btn-outline-danger icon mr-1" href="/pacientes/detalles/downloadXlxs?paciente=<?=$this->e($paciente['codigo_paciente'])?>" target="__blank">
                <i class="mdi mdi-file-pdf"></i>
            </a>
            <a href="/pacientes/edit-data?paciente=<?=$this->e($paciente['codigo_paciente'])?>" class="btn btn-primary">Editar <i class="fas fa-pencil-alt "></i></a> 
            <a href="/pacientes/delete?paciente=<?=$this->e($paciente['codigo_paciente'])?>" class="btn btn-danger">Eliminar <i class="fas fa-trash "></i></a>
        </div> <!-- end card-box -->

        <div class="card-box">
            <h5>Datos de la vivienda</h5>
            <hr>
            <div class="text-left mt-3">
                <p class=" mb-0 font-13"><strong>Casa:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['tipo'])?></span></p>

                <p class=" mb-0 font-13"><strong>Estructura:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['estructura'])?></span></p>

                <p class=" mb-0 font-13"><strong>Techo:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['techo'])?></span></p>

                <p class=" mb-0 font-13"><strong>Piso:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['piso'])?> </span></p>

                <p class=" mb-0 font-13"><strong>Número habitaciones:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['numero_habitaciones'])?> </span></p>
            </div>
        </div>

    </div> <!-- end col-->

    <div class="col-lg-8 col-xl-8">
        <?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
        <div class="row">
            <div class="col-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            </div>
        </div>
        <?php endif; ?> 
        <div class="card-box">
           
            <ul class="nav nav-tabs nav-bordered nav-justified">
                <li class="nav-item">
                    <a href="#timeline" data-toggle="tab" aria-expanded="true" class="nav-link active" style="font-size: 16px; font-weight: bold;" >
                        <i class="mdi mdi-timeline mr-1"></i>Historial médico
                    </a>
                </li>
                <li class="nav-item" style="display:none;">
                    <a href="#settings" data-toggle="tab" aria-expanded="false" class="nav-link" style="font-size: 16px; font-weight: bold;" >
                        <i class="mdi mdi-file-document-box mr-1"></i>Registro morbilidad
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane show active" id="timeline">
                    <!-- Story Box-->
                    <div class="row">
                            <?php if( count($historias) == 0 ): ?>
                            <div class="col-12 box-historia">
                                <p class="text-center">Parece que este usuario no tiene historia clinica</p>
                                <div class="col-12 row justify-content-center">
                                    <a href="/historia-medica/paciente/<?=$this->e( $paciente['codigo_paciente'] )?>" class="btn btn-primary">Registrar historia clinica</a>
                                </div>
                            </div>
                            <?php endif; ?>

                            <?php if( !historiaIsActived($historias) && count($historias) != 0  ): ?>
                                <div class="col-12 box-historia">
                                    <p class="text-center">El paciente no posee historia clinica actual</p>
                                    <div class="col-12 row justify-content-center">
                                        <a href="/historia-medica/paciente/<?=$this->e( $paciente['codigo_paciente'] )?>" class="btn btn-primary">Registrar nueva historia clinica</a>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <!-- historia activa -->
                            <?php foreach( $historias as $historia ): ?>
                                <?php if( $historia['estatus_historia'] == 'on' ): ?>
                                <div class="col-12 box-historia-actual">
                                    <!-- cabecera -->
                                    <div class="col-12 row justify-content-end">
                                        <h5 class="text-primary" style="margin-bottom: -18px;margin-right: -20px;margin-top: -1px;">Historia en curso</h5>
                                    </div>
                                    <div class="col-12 row">
                                        <div class="date bg-primary p-2">
                                            <p class="text-white mb-0">Fecha de creación</p>
                                            <h3 class="text-white mt-0"><?=$this->e( $historia['creacion_historia'] )?></h3>
                                            <p class="text-white mb-0">Hora: <?=$this->e($historia['hora_creacion'])?></p>
                                        </div>
                                        <div class="datos-generales row p-2">
                                            <div class="group-text p-2">
                                                <p class="mb-0">Contacto de emergencia</p>
                                                <h4 class="mt-0"><?=$this->e($historia['contacto_emergencia'])?></h4>
                                            </div>
                                            <div class="group-text p-2">
                                                <p class="mb-0">Teléfono de emergencia</p>
                                                <h4 class="mt-0"><?=$this->e($historia['telefono_contacto_emergencia'])?></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                        <h4 class="">Motivo de admisión</h4>
                                        <p><?=$this->e( $historia['motivo_admision'] )?></p>
                                        <div class="mb-2 col-12"></div>
                                        <h4 class="">Enfermedad actual</h4>
                                        <p><?=$this->e( $historia['enfermedad_actual'] )?></p>
                                        <div class="mb-2 col-12"></div>
                                        <h4 class="">Diagnostico admisión</h4>
                                        <p><?=$this->e( $historia['diagnostico_adminsion'] )?></p>
                                    </div>
                                    <div class="col-12 row justify-content-end">
                                        <a data-toggle="modal" href="#finalizar-historia-actual" class="btn text-danger mr-1">Finalizar historia actual</a>
                                        <a href="/historia-medica/editar/<?=$this->e($historia['codigo_historia_actual'])?>" class="btn btn-primary mr-1">Editar <i class="fas fa-pencil-alt "></i></a> 
                                        <a href="/pacientes/detalle-historia/<?=$this->e( $historia['codigo_historia_actual'] )?>" class="btn btn-outline-primary">Ver más</a>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <div class="col-xl-12">
                                <hr class="mt-4">
                                <h4 class="mt-2">Historias archivadas</h4>
                                <div id="accordion" class="mb-3">

                                <?php foreach( $historias as $historia ): ?>
                                    <?php if( $historia['estatus_historia'] == 'off' ): ?>
                                    <div class="card mb-1">
                                        <div class="card-header header-historias-archivadas" id="headingOne" style="border: 1px solid lightgray;">
                                            <a class="text-dark" data-toggle="collapse" href="#<?=$this->e($historia['codigo_historia_actual'])?>" aria-expanded="true">
                                                <div class="row justify-content-end bg-primary mb-1">
                                                    <p style="margin-bottom: -8px;" class="status-title-historia">Estatus: <span class="text-success">Finalizada</span></p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-3">
                                                        <p class="p-subtitle-group">Fecha de creación</p>
                                                        <h5 class="mt-0"><?=$this->e( $historia['creacion_historia'] )?></h5>
                                                        <h5 class="p-content">Hora: <?=$this->e( $historia['hora_creacion'] )?> </h5>
                                                    </div>
                                                    <div class="col-9">
                                                        <p class="p-subtitle-group">Motivo de admisión</p>
                                                        <p class="p-content"><?=$this->e( substr( $historia['motivo_admision'], 0, 100 ) )?>... <span class="text-primary">Ver más</span></p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                            
                                        <div id="<?=$this->e($historia['codigo_historia_actual'])?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                            <h4 class="">Motivo de admisión</h4>
                                                <p><?=$this->e( $historia['motivo_admision'] )?></p>
                                                <div class="mb-2 col-12"></div>
                                                <h4 class="">Enfermedad actual</h4>
                                                <p><?=$this->e( $historia['enfermedad_actual'] )?></p>
                                                <div class="mb-2 col-12"></div>
                                                <h4 class="">Diagnostico admisión</h4>
                                                <p><?=$this->e( $historia['diagnostico_adminsion'] )?></p>
                                                <div class="mb-2 col-12"></div>
                                                <hr>
                                                <div class="mb-2 col-12"></div>
                                                <h3>Informe de salida</h3>
                                                <hr>
                                                <h4 class="">Condisión salida</h4>
                                                <p><?=$this->e( $historia['condicion_salida'] )?></p>
                                                <h4 class="">Diagnostico final</h4>
                                                <p><?=$this->e( $historia['diagnostico_final'] )?></p>
                                                <div class="col-12 row justify-content-end">
                                                    <a href="/pacientes/detalle-historia/<?=$this->e( $historia['codigo_historia_actual'] )?>" class="btn btn-outline-primary">Ver detalles</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                                <?php 
                                $result = true;
                                foreach( $historias as $historia ){
                                    if( $historia['estatus_historia'] == 'off' ){
                                        $result = false;
                                    }
                                }
                                if($result){
                                    echo 'No hay historias archivadas todavía';
                                }
                                ?>

                                </div> 
                            </div> <!-- end col -->
                            <!-- end #accordions-->
                    </div> <!-- end row -->

                </div>
                <!-- end timeline content-->

                <div class="tab-pane" id="settings">
                    
                </div>
                <!-- end settings content-->

            </div> <!-- end tab-content -->
        </div> <!-- end card-box-->

    </div> <!-- end col -->



<!-- modal para terminar la historia actual -->
<div id="finalizar-historia-actual" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Finalizar historia actual</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                    <form action="/historias/finalizar-historia" method="POST">
                        <input type="hidden" id="codigoPaciente" name="codigoPaciente" value="<?=$this->e($paciente['codigo_paciente'])?>">
                        <?php foreach( $historias as $historia ): ?>
                            <?php if( $historia['estatus_historia'] == 'on' ): ?>
                                <input type="hidden" id="codigo_historia_actual" name="codigo_historia_actual" value="<?=$this->e($historia['codigo_historia_actual'])?>">
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <div class="form-group mb-3 col-md-12">
                            <label for="tipoSalida">Condición salida</label>
                            <select name="tipoSalida" id="tipoSalida" class="form-control" required>
                                <option value="curacion">Curacion</option>
                                <option value="mejoria">Mejoria</option>
                                <option value="muerte">Muerte</option>
                                <option value="otro">Otro</option>
                            </select>
                        </div>
                        <div class="form-group mb-3 col-md-12">
                            <label for="diagnostico">Diagnostico final</label>
                            <textarea name="diagnostico" id="diagnostico" cols="30" rows="3" class="form-control" required></textarea>
                        </div>
                        <div class="form-group mb-3 col-md-12 row justify-content-end">
                            <button class="btn btn-primary" type="submit">Finalizar historia</button>
                        </div>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->







</div>
<!-- end row-->