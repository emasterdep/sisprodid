<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">pacientes</a></li>
                    <li class="breadcrumb-item active">Editar paciente (paso 2 de 2)</li>
                </ol>
            </div>
            <h4 class="page-title">Editar datos del paciente (paso 2 de 2)</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- card usuario -->
                <div class="card">
                    <div class="card-header">
                        <?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?>
                    </div> 
                    <div class="card-body">
                        <h5 class="card-title">Cedula: <?=$this->e($paciente['cedula'])?></h5>
                        <p class="card-text">Teléfono: <?=$this->e($paciente['telefono'])?> | Fecha de nacimiento: <?=$this->e($paciente['fecha_nacimiento'])?></p>
                        <a href="/pacientes/edit-data?paciente=<?=$this->e($paciente['codigo_paciente'])?>" class="btn btn-primary">Editar datos</a>
                    </div>
                </div>
                <h4 class="header-title mb-3">Datos de la vivienda</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/pacientes/update-vivienda" method="POST" class="row">   
                            <input type="hidden" name="personaId" value="<?=$this->e($paciente['codigo_persona'])?>">
                            <div class="form-group col-md-12">
                                <h5>Localidad</h5>
                            </div>   
                            <div class="form-group mb-3 col-md-6">
                                <label for="estadoVivienda">Estado</label>
                                <select name="estadoVivienda" id="estadoVivienda" class="form-control" required>
                                    <?php if( isset($estados) && count($estados) > 0): ?>
                                        <?php foreach($estados as $estado): ?>
                                        <option value="<?=$this->e($estado['codigo_estado'])?>" <?php if($estado['codigo_estado'] == $paciente['codigo_estado'] ){ echo 'selected="true"'; } ?>><?=$this->e($estado['nombre_estado'])?></option>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                           <p>Lo sentimos pero no hay estados registrados en el sistema, por favor registre de antemano la localidad</p> 
                                    <?php endif; ?>
                                </select>
                            </div>         
                            <div class="form-group mb-3 col-md-6">
                                <label for="municipieVivienda">Municipio</label>
                                <select name="municipieVivienda" id="municipieVivienda" class="form-control" required>
                                    <option value="<?=$this->e($paciente['codigo_municipio'])?>"><?=$this->e($paciente['nombre_municipio'])?></option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="parroqueaVivienda">Parroquea</label>
                                <select name="parroqueaVivienda" id="parroqueaVivienda" class="form-control" required>
                                    <option value="<?=$this->e($paciente['codigo_parroquia'])?>"><?=$this->e($paciente['nombre_parroquia'])?></option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="comunidadVivienda">Comunidad</label>
                                <select name="comunidadVivienda" id="comunidadVivienda" class="form-control" required>
                                    <option value="<?=$this->e($paciente['codigo_comunidad'])?>"><?=$this->e($paciente['nombre_comunidad'])?></option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="sectorVivienda">Sector</label>
                                <select name="sectorVivienda" id="sectorVivienda" class="form-control" required>
                                    <option value="<?=$this->e($paciente['codigo_sector'])?>"><?=$this->e($paciente['nombre_sector'])?></option>
                                </select>
                            </div>    
                            <div class="form-group col-md-12">
                                <h5>Información de vivienda</h5>
                            </div>   
                            <div class="form-group mb-3 col-md-6">
                                <label for="numberHabitaciones">Número de habitaciones</label>
                                <input value="<?=$this->e($paciente['numero_habitaciones'])?>" type="number" id="numberHabitaciones" name="numberHabitaciones" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="tipoVivienda">Tipo de vivienda</label>
                                <select name="tipoVivienda" id="tipoVivienda" class="form-control" required>
                                    <option <?php if( $paciente['tipo'] == 'Apartamento'){ echo 'selected="true"';} ?> value="Apartamento">Apartamento</option>
                                    <option <?php if( $paciente['tipo'] == 'Casa quinta'){ echo 'selected="true"';} ?> value="Casa quinta">Casa quinta</option>
                                    <option <?php if( $paciente['tipo'] == 'Casa'){ echo 'selected="true"';} ?> value="Casa">Casa</option>
                                    <option <?php if( $paciente['tipo'] == 'Rancho'){ echo 'selected="true"';} ?> value="Rancho">Rancho</option>
                                    <option <?php if( $paciente['tipo'] == 'Otro'){ echo 'selected="true"';} ?> value="Otro">Otro</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="estructuraVivienda">Estructura de la vivienda</label>
                                <select name="estructuraVivienda" id="estructuraVivienda" class="form-control" required>
                                    <option <?php if( $paciente['estructura'] == 'Bloques'){ echo 'selected="true"';} ?> value="Bloques">Bloques</option>
                                    <option <?php if( $paciente['estructura'] == 'Madera'){ echo 'selected="true"';} ?> value="Madera">Madera</option>
                                    <option <?php if( $paciente['estructura'] == 'Zinc'){ echo 'selected="true"';} ?> value="Zinc">Zinc</option>
                                    <option <?php if( $paciente['estructura'] == 'Otro'){ echo 'selected="true"';} ?> value="Otro">Otro</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="materialPiso">Material del piso</label>
                                <select name="materialPiso" id="materialPiso" class="form-control" required>
                                    <option <?php if( $paciente['piso'] == 'Baldosa'){ echo 'selected="true"';} ?> value="Baldosa">Baldosa</option>
                                    <option <?php if( $paciente['piso'] == 'Ceramica'){ echo 'selected="true"';} ?> value="Ceramica">Ceramica</option>
                                    <option <?php if( $paciente['piso'] == 'cemento'){ echo 'selected="true"';} ?> value="cemento">cemento</option>
                                    <option <?php if( $paciente['piso'] == 'tierra'){ echo 'selected="true"';} ?> value="tierra">tierra</option>
                                    <option <?php if( $paciente['piso'] == 'Otro'){ echo 'selected="true"';} ?> value="Otro">Otro</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="MaterialTecho">Material del techo</label>
                                <select name="MaterialTecho" id="MaterialTecho" class="form-control" required>
                                    <option <?php if( $paciente['techo'] == 'Cemento'){ echo 'selected="true"';} ?> value="Cemento">Cemento</option>
                                    <option <?php if( $paciente['techo'] == 'Madera'){ echo 'selected="true"';} ?> value="Madera">Madera</option>
                                    <option <?php if( $paciente['techo'] == 'Zinc'){ echo 'selected="true"';} ?> value="Zinc">Zinc</option>
                                    <option <?php if( $paciente['techo'] == 'Otro'){ echo 'selected="true"';} ?> value="Otro">Otro</option>
                                </select>
                            </div>
                            <input type="hidden" name="idPaciente" value="<?=$this->e($paciente['codigo_paciente'])?>" >

                            <input type="hidden" name="idVivienda" value="<?=$this->e($paciente['codigo_vivienda'])?>" >
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Registrar paciente</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>

<script>
    //esto es para filtrar municipios
    document.getElementById('estadoVivienda').addEventListener('change',()=>{
        axios.get('/municipios/find-estado?estado='+document.getElementById('estadoVivienda').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione un municipio</option>';
            value.forEach( element=>{
                optionDrawMunicipios += `<option value="${element.codigo_municipio}">${element.nombre_municipio}</option>`;
            });
            document.getElementById('municipieVivienda').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    //esto es para filtrar parroqueas
    document.getElementById('municipieVivienda').addEventListener('change',()=>{
        axios.get('/parroqueas/find-municipio?municipio='+document.getElementById('municipieVivienda').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione una parroquea</option>';
            value.forEach( element=>{
                optionDrawMunicipios += `<option value="${element.codigo_parroquia}">${element.nombre_parroquia}</option>`;
            });
            document.getElementById('parroqueaVivienda').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    //esto es para filtrar comunidad
    document.getElementById('parroqueaVivienda').addEventListener('change',()=>{
        axios.get('/comunidades/filter-by-parroquea?parroquea='+document.getElementById('parroqueaVivienda').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione una comunidad</option>';
            value.forEach( element=>{
                optionDrawMunicipios += `<option value="${element.codigo_comunidad}">${element.nombre_comunidad}</option>`;
            });
            document.getElementById('comunidadVivienda').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    //esto es para filtrar sectores
    document.getElementById('comunidadVivienda').addEventListener('change',()=>{
        axios.get('/sectores/filter-by-comunidad?comunidad='+document.getElementById('comunidadVivienda').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione una comunidad</option>';
            value.forEach( element=>{
                optionDrawMunicipios += `<option value="${element.codigo_sector}">${element.nombre_sector}</option>`;
            });
            document.getElementById('sectorVivienda').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });


</script>