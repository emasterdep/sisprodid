<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item"><a href="/municipios">Municipios</a></li>
                    <li class="breadcrumb-item"><a href="/parroquea">Parroquea</a></li>
                    <li class="breadcrumb-item active">Crear parroquea</li>
                </ol>
            </div>
            <h4 class="page-title">Crear parroquea</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para crear parroquea</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/creation-parroquea" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameState">Nombre de la parroquea</label>
                                <input type="text" id="nameParroquea" name="nameParroquea" class="form-control">
                                <input type="hidden" name="municipieCode" value="<?=$_GET['selectMunicipio']?>">
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
