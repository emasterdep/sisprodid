<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item"><a href="/municipios">Municipios</a></li>
                    <li class="breadcrumb-item active">Parroqueas</li>
                </ol>
            </div>
            <h4 class="page-title">Gestor de Parroqueas</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row justify-content-center">
                <!-- formulario filtrar estados -->
                <form class="col-12 row pb-2" method="GET" action="/parroqueas">
                    <h4 class="col-12">Filtre las parroqueas</h4>
                    <div class="col-12 row justify-content-end">
                        <div class="col-md-6 col-12">
                            <label for="filterEstado">Estados</label>
                            <select name="filterEstado" class="form-control" id="estadoFind">
                                <option value="null">Seleccione un estado</option>
                                <?php foreach($estados as $estado): ?>
                                    <option value="<?=$this->e($estado['codigo_estado'])?>" 
                                    <?php if( isset($_GET['filterEstado']) && $_GET['filterEstado'] == $estado['codigo_estado'] ) { echo 'selected'; } ?>><?=$this->e($estado['nombre_estado'])?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label for="filterMunicipio">Municipios</label>
                            <select name="filterMunicipio" class="form-control" id="municipieFind">
                                <?php if( isset($municipios) ): ?>
                                    <?php foreach($municipios as $municipio): ?>
                                        <option value="<?=$this->e($municipio['codigo_municipio'])?>" <?php if($municipio['codigo_municipio'] == $_GET['filterMunicipio']){ echo 'selected'; } ?>>
                                            <?=$this->e($municipio['nombre_municipio'])?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="col-md-2 col-12 mt-2">
                            <button class="btn btn-outline-primary btn-block">Filtrar</button>
                        </div>
                    </div>                    
                </form>

            </div>
        </div> <!-- end card-box -->

        <div class="card-box">
            <div class="row justify-content-center">
            <?php if( isset($selectMunicipio) ): ?>
            <div class="row mt-3 col-12">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de Parroqueas del municipio <?=$this->e($selectMunicipio['nombre_municipio'])?></h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end pb-2">
                    <a href="/parroqueas/create?selectMunicipio=<?=$this->e($selectMunicipio['codigo_municipio'])?>" class="btn btn-primary">+ Crear Parroquea</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if( count($parroqueas) > 0 ):
                            $i = 0;
                            foreach($parroqueas as $parroquea):
                         ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($parroquea['nombre_parroquia'])?></td>
                            <td>
                                <div>
                                    <a href="/parroqueas/delete?parroquia=<?=$this->e($parroquea['codigo_parroquia'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/parroqueas/edit?parroquia=<?=$this->e($parroquea['codigo_parroquia'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            endforeach; 
                            else: ?>
                        <tr>
                            <td>NO HAY REGISTROS</td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?php else: ?>
                <p>Filtre las parroquias con los selectores en la parte superior, y obtenga resultados más precisos. Recuerde precionar filtrar por cada campo que seleccione</p>
            <?php endif; ?>
            </div>
        </div>
    </div> <!-- end col -->
</div>
<script>
    
    //esto es para filtrar parroquias
    document.getElementById('estadoFind').addEventListener('change',()=>{
        axios.get('/municipios/find-estado?estado='+document.getElementById('estadoFind').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione un municipio</option>';
            value.forEach( municipio=>{
                optionDrawMunicipios += `<option value="${municipio.codigo_municipio}">${municipio.nombre_municipio}</option>`;
            });
            document.getElementById('municipieFind').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });

</script>
