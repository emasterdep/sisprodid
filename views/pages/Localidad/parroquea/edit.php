<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item"><a href="/municipios">Municipios</a></li>
                    <li class="breadcrumb-item"><a href="/parroquea">Parroquia</a></li>
                    <li class="breadcrumb-item active">Actualizar parroquea</li>
                </ol>
            </div>
            <h4 class="page-title">Actualizar Parroquia</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para actualizar Parroquia</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/parroqueas/update" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameState">Nombre de la parroquia</label>
                                <input type="text" id="nameState" name="nameParroquea" class="form-control" value="<?=$this->e($parroquia['nombre_parroquia'])?>">
                                <input type="hidden" name="idParroquea" value="<?=$this->e($parroquia['codigo_parroquia'])?>">
                                <input type="hidden" name="codeMunicipio" value="<?=$this->e($parroquia['codigo_municipio'])?>">
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Actulizar dato</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
