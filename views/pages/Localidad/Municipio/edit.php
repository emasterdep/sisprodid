<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item"><a href="/municipios">Municipios</a></li>
                    <li class="breadcrumb-item active">Actualizar municipio</li>
                </ol>
            </div>
            <h4 class="page-title">Actualizar Municipio</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para actualizar Municipio</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/municipios/update" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameState">Nombre de Municipio</label>
                                <input type="text" id="nameState" name="nameState" class="form-control" value="<?=$this->e($municipio[0]['nombre_municipio'])?>">
                                <input type="hidden" name="idMunicipio" value="<?=$this->e($municipio[0]['codigo_municipio'])?>">
                                <input type="hidden" name="codeState" value="<?=$this->e($municipio[0]['codigo_estado'])?>">
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Actulizar dato</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
