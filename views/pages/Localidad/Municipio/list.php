<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item active">Municipios</li>
                </ol>
            </div>
            <h4 class="page-title">Gestor de Municipios</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row">
                <!-- formulario filtrar estados -->
                <form class="col-12 row pb-2" method="GET" action="/municipios">
                    <p class="col-12">Filtre los municipios por estados</p>
                    <div class="col-md-10 col-12">
                    <select name="filterEstado" class="form-control">
                        <?php foreach($estados as $estado): ?>
                            <option value="<?=$this->e($estado['codigo_estado'])?>"><?=$this->e($estado['nombre_estado'])?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                    <div class="col-md-2 col-12">
                        <button class="btn btn-outline-primary btn-block">Filtrar</button>
                    </div>
                </form>

            </div>
            <?php if( isset($select) ): ?>
            <hr>
            <div class="row mt-3">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de Municipio del estado <?=$this->e($select['nombre_estado'])?></h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end pb-2">
                    <a href="/municipios/create?selectEstate=<?=$this->e($select['codigo_estado'])?>" class="btn btn-primary">+ Crear municipio</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if( count($municipios) > 0 ):
                            $i = 0;
                            foreach($municipios as $municipio):
                         ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($municipio['nombre_municipio'])?></td>
                            <td>
                                <div>
                                    <a href="/municipios/delete?municipio=<?=$this->e($municipio['codigo_municipio'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/municipios/edit?municipio=<?=$this->e($municipio['codigo_municipio'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            endforeach; 
                            else: ?>
                        <tr>
                            <td>NO HAY REGISTROS</td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?php endif; ?>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<!-- esto es para filtrar municipios -->
<script>
            document.getElementById('estadoFind').addEventListener('change',()=>{
            axios.get('/municipios/find-estado?estado='+document.getElementById('listaEstados').value)
            .then(function (response) {
                return response.data;
            })
            .then(function(value){
                console.log(value);
                var optionDrawEstados = '<option value="--">Seleccione un estado</option>';
                value.forEach( element=>{
                    optionDrawEstados += `<option value="${element.codigo_estado}">${element.nombre_estado}</option>`;
                });
                document.getElementById('created-state').innerHTML = optionDrawEstados;
            })
            .catch(function (error) {
                console.error(error);    
            });
        });
    </script>
