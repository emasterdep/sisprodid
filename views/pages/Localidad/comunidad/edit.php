<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item"><a href="/municipios">Municipios</a></li>
                    <li class="breadcrumb-item"><a href="/parroquea">Parroquia</a></li>
                    <li class="breadcrumb-item"><a href="/comunidades">Comunidades</a></li>
                    <li class="breadcrumb-item active">Actualizar comunidad</li>
                </ol>
            </div>
            <h4 class="page-title">Actualizar comunidad</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para actualizar Comunidad</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/comunidades/update" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameState">Nombre</label>
                                <input type="text" id="nameState" name="nameComunidad" class="form-control" value="<?=$this->e($comunidad['nombre_comunidad'])?>">
                                <input type="hidden" name="idComunidad" value="<?=$this->e($comunidad['codigo_comunidad'])?>">
                                <input type="hidden" name="codeParroquia" value="<?=$this->e($comunidad['codigo_parroquia'])?>">
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Actulizar dato</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
