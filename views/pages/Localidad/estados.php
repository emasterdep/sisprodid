<?php $this->layout('layouts/app') ?>

<!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item active">Estados</li>
                </ol>
            </div>
            <h4 class="page-title">Gestor de estados</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
 <div class="row">
        <div class="col-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
  </div>

<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de Estados</h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end">
                    <a href="/crear-estado" class="btn btn-primary">+ Crear estado</a>
                </div>
            </div>
            <p class="sub-header">
                Listados de estados disponibles en el sistema. Si no observa el estado que esta buscando, vaya a la sección de crear nuevo estado
            </p>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 0;
                        foreach($estados as $estado){ ?>
                        
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($estado['nombre_estado'])?></td>
                            <td>
                                <div>
                                    <a href="/estado/delete?estado=<?=$this->e($estado['codigo_estado'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>;
                                    
                                    <a href="/estado/edit?estado=<?=$this->e($estado['codigo_estado'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        
                        <?php
                        $i++;
                        } ?>
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
  
<script>
    Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.isConfirmed) {
        Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
        )
    }
    })
</script>
