<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item"><a href="/municipios">Municipios</a></li>
                    <li class="breadcrumb-item"><a href="/parroquias">Parroquias</a></li>
                    <li class="breadcrumb-item"><a href="/comunidades">Comunidades</a></li>
                    <li class="breadcrumb-item active">Sectores</li>
                </ol>
            </div>
            <h4 class="page-title">Gestor de sectores</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'success' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Acción realizada con exito!</strong> la acción que realizo fue realizada sin problemas
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<?php if( isset($_GET['unabledata']) && $_GET['unabledata'] == 'fail' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Lo sentimos!</strong> El dato que desea usar no esta disponible
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="row justify-content-center">
                <!-- formulario filtrar estados -->
                <form class="col-12 row pb-2" method="GET" action="/sectores">
                    <h4 class="col-12">Filtre los sectores</h4>
                    <div class="col-12 row">
                        <div class="col-md-6 col-12">
                            <label for="filterEstado">Estados</label>
                            <select name="filterEstado" class="form-control" id="filterEstado">
                            <option value="--">seleccione un estado</option>    
                                <?php foreach($estados as $estado): ?>
                                    <option value="<?=$this->e($estado['codigo_estado'])?>" 
                                    <?php if( isset($_GET['filterEstado']) && $_GET['filterEstado'] == $estado['codigo_estado'] ) { echo 'selected'; } ?>><?=$this->e($estado['nombre_estado'])?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-12">
                            <label for="filterMunicipio">Municipios</label>
                            <select name="filterMunicipio" class="form-control" id="filterMunicipio">
                                <?php if( isset($municipios) ): ?>
                                    <?php foreach($municipios as $municipio): ?>
                                        <option value="<?=$this->e($municipio['codigo_municipio'])?>"
                                        <?php if( isset($_GET['filterMunicipio']) && $_GET['filterMunicipio'] == $municipio['codigo_municipio'] ) { echo 'selected'; } ?>><?=$this->e($municipio['nombre_municipio'])?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-12 mt-2">
                            <label for="filterMunicipio">Parroqueas</label>
                            <select name="filterParroquea" class="form-control" id="filterParroquea">
                                <?php if( isset($parroqueas) ): ?>
                                    <?php foreach($parroqueas as $parroquia): ?>
                                        <option value="<?=$this->e($parroquia['codigo_parroquia'])?>"
                                        <?php if( isset($_GET['filterParroquea']) && $_GET['filterParroquea'] == $parroquia['codigo_parroquia'] ) { echo 'selected'; }?> ><?=$this->e($parroquia['nombre_parroquia'])?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-12 mt-2">
                            <label for="comunidadSelect">Comunidades</label>
                            <select name="comunidadSelect" class="form-control" id="comunidadSelect">
                                <?php if( isset($comunidades) ): ?>
                                    <?php foreach($comunidades as $comunidad): ?>
                                        <option value="<?=$this->e($comunidad['codigo_comunidad'])?>"
                                        <?php if( isset($_GET['comunidadSelect']) && $_GET['comunidadSelect'] == $comunidad['codigo_comunidad'] ) { echo 'selected'; }?> ><?=$this->e($comunidad['nombre_comunidad'])?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>   
                    <div class="col-12 row justify-content-end">
                        <div class="col-md-2 col-12 mt-2">
                            <button class="btn btn-outline-primary btn-block">Filtrar</button>
                        </div>
                    </div>                 
                </form>
            </div>
        </div> <!-- end card-box -->


        <div class="card-box">
            <div class="row justify-content-center">
            <?php if( isset($comunidadSelect) ): ?>
            <div class="row mt-3 col-12">
                <div class="col-md-6 col-12">
                    <h4 class="header-title">Listado de sectores de la comunidad <?=$this->e($comunidadSelect['nombre_comunidad'])?></h4>
                </div>
                <div class="col-md-6 col-12 row justify-content-end pb-2">
                    <a href="/sectores/create?selectComunidad=<?=$this->e($comunidadSelect['codigo_comunidad'])?>" class="btn btn-primary">+ Crear sector</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if( count($sectores) > 0):
                            $i = 0;
                            foreach($sectores as $sector):
                         ?>
                        <tr>
                            <th scope="row"><?=$this->e($i+1)?></th>
                            <td><?=$this->e($sector['nombre_sector'])?></td>
                            <td>
                                <div>
                                    <a href="/sectores/delete?sector=<?=$this->e($sector['codigo_sector'])?>" class="btn btn-danger">
                                        <i class="fas fa-trash "></i>
                                    </a>
                                    <a href="/sectores/edit?sector=<?=$this->e($sector['codigo_sector'])?>" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt "></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            endforeach; 
                            else: ?>
                        <tr>
                            <td>NO HAY REGISTROS</td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?php else: ?>
                <p>Filtre las comunidades con los selectores en la parte superior, y obtenga resultados más precisos. Recuerde precionar filtrar por cada campo que seleccione</p>
            <?php endif; ?>
            </div>
        </div>
    </div> <!-- end col -->
</div>

<script>
//esto es para filtrar municipios
    document.getElementById('filterEstado').addEventListener('change',()=>{
        axios.get('/municipios/find-estado?estado='+document.getElementById('filterEstado').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione un municipio</option>';
            value.forEach( element=>{
                optionDrawMunicipios += `<option value="${element.codigo_municipio}">${element.nombre_municipio}</option>`;
            });
            document.getElementById('filterMunicipio').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    //esto es para filtrar parroqueas
    document.getElementById('filterMunicipio').addEventListener('change',()=>{
        axios.get('/parroqueas/find-municipio?municipio='+document.getElementById('filterMunicipio').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione una parroquea</option>';
            value.forEach( element=>{
                optionDrawMunicipios += `<option value="${element.codigo_parroquia}">${element.nombre_parroquia}</option>`;
            });
            document.getElementById('filterParroquea').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    //esto es para filtrar comunidad
    document.getElementById('filterParroquea').addEventListener('change',()=>{
        axios.get('/comunidades/filter-by-parroquea?parroquea='+document.getElementById('filterParroquea').value)
        .then(function (response) {
            return response.data;
        })
        .then(function(value){
            console.log(value);
            var optionDrawMunicipios = '<option value="--">seleccione una comunidad</option>';
            value.forEach( element=>{
                optionDrawMunicipios += `<option value="${element.codigo_comunidad}">${element.nombre_comunidad}</option>`;
            });
            document.getElementById('comunidadSelect').innerHTML = optionDrawMunicipios;
        })
        .catch(function (error) {
            console.log(error);
        });
    });

</script>
