<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Localidad</a></li>
                    <li class="breadcrumb-item"><a href="/estados">Estados</a></li>
                    <li class="breadcrumb-item active">Crear Estado</li>
                </ol>
            </div>
            <h4 class="page-title">Crear Estado</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para creación de estado</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/created-state" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameState">Nombre de estado</label>
                                <input type="text" id="nameState" name="nameState" class="form-control">
                            </div>
                              <div class="form-group mb-3">
                                
                               <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal">Guardar</button>
                            
                            <!-- modal -->
                            
                            <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                             <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="createModalLabel">Atencion</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                            </div>

                            <div class="modal-body">
                                 Deseas guardar los datos
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                            </div>
  </div>
</div>

</div>

                            
                            
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>


<!-- axios guardar-->

