<?php $this->layout('layouts/app') ?>

  <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Upvex</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">UI</a></li>
                                            <li class="breadcrumb-item active">Ajustes</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Ajustes</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 


                        <h4 class="mb-4">Localidades</h4>
                        <div class="row">

                             <div class="col-lg-6 col-xl-2">
                                <!-- Simple card -->
                                <div class="card">
                                    <img class="card-img-top img-fluid" src="<?=assets('img/localidades/estado.jpg')?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Gestor de Estados</h5>
                                        
                                        <a href="/estados" class="btn btn-primary waves-effect waves-light">Gestionar</a>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-6 col-xl-2">
                                <!-- Simple card -->
                                <div class="card">
                                    <img class="card-img-top img-fluid" src="<?=assets('img/localidades/municipio.jpg')?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Gestor de Municipios</h5>
                                        
                                        <a href="/municipios" class="btn btn-primary waves-effect waves-light">Gestionar</a>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-6 col-xl-2">
                                <!-- Simple card -->
                                <div class="card">
                                    <img class="card-img-top img-fluid" src="<?=assets('img/localidades/parroquia.jpg')?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Gestor de Parroqueas</h5>
                                        
                                        <a href="/parroqueas" class="btn btn-primary waves-effect waves-light">Gestionar</a>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-6 col-xl-2">
                                <!-- Simple card -->
                                <div class="card">
                                    <img class="card-img-top img-fluid" src="<?=assets('img/localidades/comunidad.jpg')?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Gestor de Comunidad</h5>
                                        
                                        <a href="/comunidades" class="btn btn-primary waves-effect waves-light">Gestionar</a>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-6 col-xl-2">
                                <!-- Simple card -->
                                <div class="card">
                                    <img class="card-img-top img-fluid" src="<?=assets('img/localidades/sector.jpg')?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Gestor de Sector</h5>
                                        
                                        <a href="/sectores" class="btn btn-primary waves-effect waves-light">Gestionar</a>
                                    </div>
                                </div>
                            </div><!-- end col -->

                           

                            <div class="col-lg-6 col-xl-2">
                              
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->


                        <h4 class="mb-4">Otros</h4>
                        <div class="row">

                             <div class="col-lg-6 col-xl-2">
                                <!-- Simple card -->
                                <div class="card">
                                    <img class="card-img-top img-fluid" src="<?=assets('img/vivienda/servicios.jpg')?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Listado de servicios</h5>
                                        
                                        <a href="/viviendas/service" class="btn btn-primary waves-effect waves-light">Gestionar</a>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-6 col-xl-2">
                              
                            </div><!-- end col -->

                            <div class="col-lg-6 col-xl-2">
                               
                            </div><!-- end col -->

                            <div class="col-lg-6 col-xl-2">
                               
                            </div><!-- end col -->

                           

                            <div class="col-lg-6 col-xl-2">
                              
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->


                      
