<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">pacientes</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes/">Detalle paciente</a></li>
                    <li class="breadcrumb-item active">Editar historia clinica</li>
                </ol>
            </div>
            <h4 class="page-title">Editar historia clinica</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <p>Formulario para editar la siguiente historia actual</p>
                        <form action="/historia-medica/actual/update" method="POST" class="row">
                            <!-- titulo  -->
                            <div class="col-12 form-group bg-primary">
                                <h5 class="text-white">Contacto de emergencia</h5>
                                <input type="hidden" id="idHistoriaEdit" value="<?=$this->e($historia['codigo_historia_actual'])?>" name="idHistoriaEdit" class="form-control">
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="nombre">Nombre</label>
                                <input type="text" id="nombre" value="<?=$this->e($historia['contacto_emergencia'])?>" name="nombre" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="telefono">teléfono</label>
                                <input type="number" id="telefono" value="<?=$this->e($historia['telefono_contacto_emergencia'])?>" name="telefono" class="form-control" required>
                            </div>
                            <!-- titulo  -->
                            <div class="col-12 form-group bg-primary">
                                <h5 class="text-white">Justificación</h5>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="motivoAdmision">Motivo de admisión</label>
                                <textarea name="motivoAdmision" id="motivoAdmision" cols="30" rows="5" class="form-control" placeholder="Llene aquí..."><?=$this->e($historia['motivo_admision'])?></textarea>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="motivoEnferma">Enfermedad actual</label>
                                <textarea name="motivoEnferma" id="motivoEnferma" cols="30" rows="5" class="form-control" placeholder="Hacer el relato conciso y completo de las dolencias, indicando fecha de comienzo, duración y tratamiento a cada una de ellas..."><?=$this->e($historia['enfermedad_actual'])?></textarea>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="diagnosticoAdmision">Diagnostico admisión</label>
                                <textarea name="diagnosticoAdmision" id="diagnosticoAdmision" cols="30" rows="5" class="form-control" placeholder="Llene aquí..."><?=$this->e($historia['diagnostico_adminsion'])?></textarea>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="date">Fecha de ingreso</label>
                                <input type="date" id="date" name="date" value="<?=$this->e($historia['creacion_historia'])?>" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="hour">Hora de ingreso</label>
                                <input type="time" id="hour" name="hour" value="<?=$this->e($historia['hora_creacion'])?>" class="form-control" required>
                            </div>
                            <!-- titulo  -->
                            
                            <input value="<?=$this->e($historia['codigo_paciente'])?>" type="hidden" id="idPaciente" name="idPaciente">
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Editar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
