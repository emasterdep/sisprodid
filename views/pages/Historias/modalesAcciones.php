
<!-- modal crear examen -->
<div id="registro-examen-funcional" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registro de examen funcional</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <form id="form-registro-examen">
                <?php foreach( $historias as $historia ): ?>
                    <?php if( $historia['codigo_historia_actual'] == $select_historia ): ?>
                    <input type="hidden" id="codigoHistoriaActual" name="codigoHistoriaActual" value="<?=$historia['codigo_historia_actual']?>">
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="form-group mb-3 col-md-12">
                    <label for="tipoExamen">Tipo de examen</label>
                    <select name="tipoExamen" id="tipoExamen" class="form-control" >
                        <option value="Piel">Piel</option>
                        <option value="Cabeza">Cabeza</option>
                        <option value="Ojos">Ojos</option>
                        <option value="Respiratorio">Respiratorio</option>
                        <option value="Cardiovascular">Cardiovascular</option>
                        <option value="Digestivo">Digestivo</option>
                        <option value="Genitourinario">Genitourinario</option>
                        <option value="Endocrino">Endocrino</option>
                        <option value="Extremidades">Extremidades</option>
                        <option value="SNC">SNC</option>
                    </select>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="descripcion">Descripción</label>
                    <textarea name="descripcion" id="descripcion" cols="30" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="dato_objetivo">Dato objetivo</label>
                    <textarea name="dato_objetivo" id="dato_objetivo_examen" cols="30" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12 row justify-content-end">
                    <button class="btn btn-primary" type="submit">Guardar evolución del paciente</button>
                </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

<!-- modal editar examen -->
<div id="editar-examen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar examen funcional</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <form id="form-editar-examen">
                <input type="hidden" id="codigoexamen" name="codigoexamen" value="">
                <div class="form-group mb-3 col-md-12">
                    <label for="tipoExamen_edit">Tipo de examen</label>
                    <select name="tipoExamen_edit" id="tipoExamen_edit" class="form-control" >
                        <option value="Piel">Piel</option>
                        <option value="Cabeza">Cabeza</option>
                        <option value="Ojos">Ojos</option>
                        <option value="Respiratorio">Respiratorio</option>
                        <option value="Cardiovascular">Cardiovascular</option>
                        <option value="Digestivo">Digestivo</option>
                        <option value="Genitourinario">Genitourinario</option>
                        <option value="Endocrino">Endocrino</option>
                        <option value="Extremidades">Extremidades</option>
                        <option value="SNC">SNC</option>
                    </select>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="descripcion_edit">Descripción</label>
                    <textarea name="descripcion_edit" id="descripcion_edit" cols="30" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="dato_objetivo_examen_edit">Dato objetivo</label>
                    <textarea name="dato_objetivo_edit" id="dato_objetivo_examen_edit" cols="30" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12 row justify-content-end">
                    <button class="btn btn-primary" type="submit">Editar examen funcional</button>
                </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

<script>
    document.getElementById('form-registro-examen').addEventListener('submit',event=>{
        event.preventDefault();
        var envioData = '?historiaActual='+document.getElementById('codigoHistoriaActual').value+'&tipoExamen='+document.getElementById('tipoExamen').value+'&descripcion='+document.getElementById('descripcion').value+'&datoObj='+document.getElementById('dato_objetivo_examen').value;
        axios.get('/historias/save-examen-funcional'+envioData)
            .then(function (response) {
                $('#registro-examen-funcional').modal('hide');
                document.getElementById('form-registro-examen').reset();
                loadExamenesFuncionales();
                Swal.fire({
                    title: 'Examen funcional registrado con exito',
                    text: "su guardo el registro del examen en la base de datos",
                    icon: 'success',
                    timer: 3000,
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    });

    document.addEventListener("DOMContentLoaded", function(){
        loadExamenesFuncionales();
    });

    function loadExamenesFuncionales()
    {
        axios.get('/historias/return-examenes?historiaActual='+document.getElementById('codigoHistoriaActual').value)
            .then(function (response) {
                return response.data;
            })
            .then(function(value){ 
                drawExamentesFuncionales(value);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    function drawExamentesFuncionales(datos)
    {
        var cardItems = ''; 
        var statusHistoria = document.getElementById('status-historia').innerHTML;
        if ( datos.length == 0 ){ console.log('dato '+datos);
            cardItems = 'No hay examenes funcionales en esta historia';
        }else {
            datos.forEach(element=>{
                cardItems += `
                <div class="card-examentes">
                    <div class="bg-back">
                        <h4 class="text-white mb-0">Tipo de examen</h4>
                        <p class="mb-1">${element.nombre}</p>
                        <p>Creado: ${element.date_creation}</p>`;
                //verifica que no estoy en la historia actual
                if(statusHistoria != 'Historia finalizada'){
                    cardItems += 
                        `<div class="w-100 d-flex wrap justify-content-end" style="margin-top: -41px;padding-bottom: 9px;">
                            <a href="javascript:void(0)" examen="${element.codigo_examen}" class="btn btn-danger mr-1 eliminarExa">
                                <i class="fas fa-trash "></i>
                            </a>
                            <a data-toggle="modal" href="#editar-examen" examen="${element.codigo_examen}" class="btn btn-warning mr-1 editarExamen">
                                <i class="fas fa-pencil-alt "></i>
                            </a>
                        </div>`;
                }

                cardItems += `</div>
                    <div class="examenes-p">
                        <h5>Descripción</h5>
                        <p>${element.descripcion}</p>
                        <div class="col-12 mb-1"></div>
                        <h5>Dato objetivo</h5>
                        <p>${element.dato_objetivo}</p>
                    </div>
                </div>`;
            });
            document.getElementById('list-examen-funcional').innerHTML = cardItems;
            loadEventForExamenFunctionalButtom();
        }
    }

    function loadEventForExamenFunctionalButtom()
    {
        /** eliminar un registro de evolucion */
        document.querySelectorAll('.eliminarExa').forEach(element=>{
            element.addEventListener('click',e=>{
                Swal.fire({
                    title: '¿Esta seguro de eliminar un examen funcional del paciente?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminarlo'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        axios.get('/historias/delete-examenes?examen='+element.getAttribute('examen'))
                            .then(function (response) {
                                loadExamenesFuncionales();
                                Swal.fire({
                                    title: 'Examen funcional eliminada con exito',
                                    text: "su registro eliminado de la base de datos",
                                    icon: 'success',
                                    timer: 3000,
                                })
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    }
                })
            });
        });

        /** ver formulario de edicion un registro */
        document.querySelectorAll('.editarExamen').forEach(element=>{ console.log('editando')
            element.addEventListener('click',e=>{
                axios.get('/historias/find-examenes?examen='+element.getAttribute('examen'))
                    .then(function (response) {
                        return response.data;
                    })
                    .then(function(value){ console.log(value);
                        document.getElementById('tipoExamen_edit').value = value.nombre;
                        document.getElementById('descripcion_edit').value = value.descripcion;
                        document.getElementById('dato_objetivo_examen_edit').value = value.dato_objetivo;
                        document.getElementById('codigoexamen').value = element.getAttribute('examen');
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            });
        });
    }

    /** editar examen funcional */
    document.getElementById('form-editar-examen').addEventListener('submit',event=>{
        event.preventDefault();
        var envioData = '?examen='+document.getElementById('codigoexamen').value+'&tipoExamen='+document.getElementById('tipoExamen_edit').value+'&descripcion='+document.getElementById('descripcion_edit').value+'&datoObj='+document.getElementById('dato_objetivo_examen_edit').value;
        axios.get('/historias/edit-examenes'+envioData)
            .then(function (response) {
                $('#editar-examen').modal('hide');
                document.getElementById('form-editar-examen').reset();
                loadExamenesFuncionales();
                Swal.fire({
                    title: 'Examen funcional editado con exito',
                    text: "su guardo el registro del examen en la base de datos",
                    icon: 'success',
                    timer: 3000,
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    });


</script>