<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">pacientes</a></li>
                    <li class="breadcrumb-item active">Detalle historial paciente</li>
                </ol>
            </div>
            <h4 class="page-title">Detalle historial paciente</h4>
        </div>
    </div>
</div>     

<div class="row">
    <p class="p-2">Las historias clinicas en curso son las que se pueden editar y borrar, las historias finalizadas, por seguridad no pueden ser eliminadas, ni editadas por el registro fidedigno que debe tenerse del paciente y su evolución dentro del centro de salud</p>
    <div class="col-12">
        <div class="card-box d-flex wrap">
            <div class="col-12 col-md-3 column aling-item-center">
                <h4 class="mb-0 text-center"><?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?></h4>
                <p class="text-muted text-center">Diabetes tipo <?=$this->e($paciente['tipo_diabetes'])?></p>
            </div><!-- fin div -->
            <div class="col-12 col-md-5 row">
                <h4 class="col-12">Datos personales</h4>
                <div class="col-12 col-md-6">
                    <p class=" mb-0 font-13"><strong>Cedula:</strong></p>
                    <p><span class="mb-1 text-gray"><?=$this->e($paciente['cedula'])?></span></p>           
                </div><!-- fin div -->
                <div class="col-12 col-md-6">
                    <p class=" mb-0 font-13"><strong>Teléfono:</strong></p>
                    <p><span class="mb-1 text-gray"><?=$this->e($paciente['telefono'])?></span></p>
                </div><!-- fin div -->
                
            </div>

            <div class="col-12 col-md-4">
                <p class=" mb-0 font-13"><strong>Dirección:</strong></p>
                <p><span class="mb-1 text-gray"><?=$this->e($paciente['nombre_sector'])?> - <?=$this->e($paciente['nombre_comunidad'])?>, parroquea <?=$this->e($paciente['nombre_parroquia'])?>, municipio <?=$this->e($paciente['nombre_municipio'])?>, estado - <?=$this->e($paciente['nombre_estado'])?></span></p>
            </div>

        </div> <!-- end card-box -->

    </div> <!-- end col-->

    <div class="col-12">
        <div class="card-box">
        <h4>Historia clinica</h4>
        <div class="row">
            <!-- historia activa -->
            <?php foreach( $historias as $historia ): ?>
                <?php if( $historia['codigo_historia_actual'] == $select_historia ): ?>
                    <div class="col-12">
                        <!-- cabecera del estatus del proyecto -->
                        <?php if( $historia['estatus_historia'] == 'on' ): ?>
                        <div class="col-12 row justify-content-end">
                            <h5 id="status-historia" class="text-primary" style="margin-bottom: -18px;margin-right: -20px;margin-top: -1px;">Historia en curso</h5>
                        </div>
                        <?php else: ?>
                        <div class="col-12 row justify-content-end">
                            <h5 id="status-historia" class="text-warning" style="margin-bottom: -18px;margin-right: -20px;margin-top: -1px;">Historia finalizada</h5>
                        </div>
                        <?php endif; ?>
                        <!-- fin del div para cabecera estatus -->
                        <div class="col-12 row">
                            <div class="date bg-primary p-2">
                                <p class="text-white mb-0">Fecha de creación</p>
                                <h3 class="text-white mt-0"><?=$this->e( $historia['creacion_historia'] )?></h3>
                                <p class="text-white mb-0">Hora: <?=$this->e($historia['hora_creacion'])?></p>
                            </div>
                            <div class="datos-generales row p-2">
                                <div class="group-text p-2">
                                    <p class="mb-0">Contacto de emergencia</p>
                                    <h4 class="mt-0"><?=$this->e($historia['contacto_emergencia'])?></h4>
                                </div>
                                <div class="group-text p-2">
                                    <p class="mb-0">Teléfono de emergencia</p>
                                    <h4 class="mt-0"><?=$this->e($historia['telefono_contacto_emergencia'])?></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <hr>
                            <h4 class="">Motivo de admisión</h4>
                            <p><?=$this->e( $historia['motivo_admision'] )?></p>
                            <div class="mb-2 col-12"></div>
                            <h4 class="">Enfermedad actual</h4>
                            <p><?=$this->e( $historia['enfermedad_actual'] )?></p>
                            <div class="mb-2 col-12"></div>
                            <h4 class="">Diagnostico admisión</h4>
                            <p><?=$this->e( $historia['diagnostico_adminsion'] )?></p>
                            <hr>
                            <?php if( $historia['condicion_salida'] != null ): ?>
                            <h3>Informe de salida</h3>
                            <hr>
                                <h4 class="">Condición salida</h4>
                                <p><?=$this->e( $historia['condicion_salida'] )?></p>
                                <h4 class="">Diagnostico final</h4>
                                <p><?=$this->e( $historia['diagnostico_final'] )?></p>
                            <?php endif; ?>
                        </div>
                        <?php if( $historia['estatus_historia'] == 'on' ): ?>
                            <div class="col-12 row justify-content-end">
                                <a data-toggle="modal" href="#finalizar-historia-actual" class="btn text-danger mr-1">Finalizar historia actual</a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-xl-6">
        <div class="card-box">
            <h4>Evoluciones</h4>
            <?php foreach( $historias as $historia ): ?>
                <?php if( $historia['codigo_historia_actual'] == $select_historia ): ?>
                    <?php if( $historia['estatus_historia'] == 'on' ): ?>
                        <a data-toggle="modal" href="#registro-evaluacion" class="btn btn-outline-primary btn-block">Registrar nueva evolución</a>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <hr>
            <div class="content-data-evoluciones" id="content-data-evoluciones">
                <!-- se llena dinamico -->
            </div>
        </div> <!-- end card-box-->

    </div> <!-- end col -->

    <div class="col-12 col-lg-6">
        <div class="card-box">
            <h4>Examenes</h4>
            <?php foreach( $historias as $historia ): ?>
                <?php if( $historia['codigo_historia_actual'] == $select_historia ): ?>
                    <?php if( $historia['estatus_historia'] == 'on' ): ?>
                        <a data-toggle="modal" href="#registro-examen-funcional" class="btn btn-outline-primary btn-block">Registrar examen funcional</a>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <hr>
            <div class="list-examen-funcional" id="list-examen-funcional">
                <!-- se llena dinamico -->
            </div>
        </div>
    </div>

    <?php require_once __DIR__.'/modalesAcciones.php';?>

<!-- modal crear evaluacion -->
<div id="registro-evaluacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registro de evaluación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <form id="formulario-registro-evaluacion">
                <?php foreach( $historias as $historia ): ?>
                    <?php if( $historia['codigo_historia_actual'] == $select_historia ): ?>
                    <input type="hidden" id="codigoHistoriaActual" name="codigoHistoriaActual" value="<?=$historia['codigo_historia_actual']?>">
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="form-group mb-3 col-md-12">
                    <label for="hallazgos_evo">Estado de los hallazgos positivos anotados anteriormente</label>
                    <textarea name="hallazgos_evo" id="hallazgos_evo" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="dato_objetivo">Datos objetivo</label>
                    <textarea name="dato_objetivo" id="dato_objetivo" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="nuevossitomas_evo">Nuevos sintomas</label>
                    <textarea name="nuevossitomas_evo" id="nuevossitomas_evo" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="complicaciones_evo">Complicaciones</label>
                    <textarea name="complicaciones_evo" id="complicaciones_evo" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="cambioimpresion_evo">Cambio de impresion o diagnostico</label>
                    <textarea name="cambioimpresion_evo" id="cambioimpresion_evo" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="tratamiento_evo">Tratamiento seguido</label>
                    <textarea name="tratamiento_evo" id="tratamiento_evo" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="resultadotrata_evo">Resultados del tratamiento</label>
                    <textarea name="resultadotrata_evo" id="resultadotrata_evo" cols="30" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12 row justify-content-end">
                    <button class="btn btn-primary" type="submit">Guardar evolución del paciente</button>
                </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

<!-- modal crear evaluacion -->
<div id="editar-evaluacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar evaluación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <form id="formulario-edicion-evaluacion">
                <?php foreach( $historias as $historia ): ?>
                    <?php if( $historia['codigo_historia_actual'] == $select_historia ): ?>
                    <input type="hidden" id="codigoHistoriaActual" name="codigoHistoriaActual" value="<?=$historia['codigo_historia_actual']?>">
                    <?php endif; ?>
                <?php endforeach; ?>
                <input type="hidden" value="" name="codigoEvolucion" id="codigoEvolucion">
                <div class="form-group mb-3 col-md-12">
                    <label for="hallazgos_evo-edit">Estado de los hallazgos positivos anotados anteriormente</label>
                    <textarea name="hallazgos_evo-edit" id="hallazgos_evo-edit" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="dato_objetivo-edit">Datos objetivo</label>
                    <textarea name="dato_objetivo-edit" id="dato_objetivo-edit" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="nuevossitomas_evo-edit">Nuevos sintomas</label>
                    <textarea name="nuevossitomas_evo-edit" id="nuevossitomas_evo-edit" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="complicaciones_evo-edit">Complicaciones</label>
                    <textarea name="complicaciones_evo-edit" id="complicaciones_evo-edit" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="cambioimpresion_evo-edit">Cambio de impresion o diagnostico</label>
                    <textarea name="cambioimpresion_evo-edit" id="cambioimpresion_evo-edit" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="tratamiento_evo-edit">Tratamiento seguido</label>
                    <textarea name="tratamiento_evo-edit" id="tratamiento_evo-edit" cols="30" rows="3" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12">
                    <label for="resultadotrata_evo-edit">Resultados del tratamiento</label>
                    <textarea name="resultadotrata_evo-edit" id="resultadotrata_evo-edit" cols="30" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group mb-3 col-md-12 row justify-content-end">
                    <button class="btn btn-primary" type="submit">Guardar evolución del paciente</button>
                </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->


<!-- modal para ver -->
<div id="detalle-evaluacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detalle evolución del paciente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <h4>Fecha de evolución</h4>
                <p id="view_data"></p>
                <h4>Estado de los hallazgos positivos anotados anteriormente</h4>
                <p id="view_hallazgos"></p>
                <hr>
                <div class="mb-2 col-12"></div>
                <h4>Datos objetivo</h4>
                <p id="view_objetivo"></p>
                <hr>
                <div class="mb-2 col-12"></div>
                <h4>Nuevos sintomas</h4>
                <p id="view_sintomas"></p>
                <hr>
                <div class="mb-2 col-12"></div>
                <h4>Complicaciones</h4>
                <p id="view_complicaciones"></p>
                <hr>
                <div class="mb-2 col-12"></div>
                <h4>Cambio de impresion o diagnostico</h4>
                <p id="view_impresion"></p>
                <hr>
                <div class="mb-2 col-12"></div>
                <h4>Tratamiento seguido</h4>
                <p id="view_tratamiento"></p>
                <hr>
                <div class="mb-2 col-12"></div>
                <h4>Resultados del tratamiento</h4>
                <p id="view_resultado"></p>
                <div class="mb-2 col-12"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->


<!-- modal para terminar la historia actual -->
<div id="finalizar-historia-actual" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Finalizar historia actual</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                    <form action="/historias/finalizar-historia" method="POST">
                        <input type="hidden" id="codigoPaciente" name="codigoPaciente" value="<?=$this->e($paciente['codigo_paciente'])?>">
                        <?php foreach( $historias as $historia ): ?>
                            <?php if( $historia['codigo_historia_actual'] == $select_historia ): ?>
                                <input type="hidden" id="codigo_historia_actual" name="codigo_historia_actual" value="<?=$this->e($historia['codigo_historia_actual'])?>">
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <div class="form-group mb-3 col-md-12">
                            <label for="tipoSalida">Condición salida</label>
                            <select name="tipoSalida" id="tipoSalida" class="form-control" required>
                                <option value="curacion">Curacion</option>
                                <option value="mejoria">Mejoria</option>
                                <option value="muerte">Muerte</option>
                                <option value="otro">Otro</option>
                            </select>
                        </div>
                        <div class="form-group mb-3 col-md-12">
                            <label for="diagnostico">Diagnostico final</label>
                            <textarea name="diagnostico" id="diagnostico" cols="30" rows="3" class="form-control" required></textarea>
                        </div>
                        <div class="form-group mb-3 col-md-12 row justify-content-end">
                            <button class="btn btn-primary" type="submit">Finalizar historia</button>
                        </div>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->


</div>
<!-- end row-->

<!-- manejo de evoluciones -->
<script>
    document.getElementById('formulario-registro-evaluacion').addEventListener('submit',event=>{
        event.preventDefault();
        var envioData = `?historia_actual=${document.getElementById('codigoHistoriaActual').value}&hallazgos=${document.getElementById('hallazgos_evo').value}&data_objetivo=${document.getElementById('dato_objetivo').value}&sistomas=${document.getElementById('nuevossitomas_evo').value}&complicaciones=${document.getElementById('complicaciones_evo').value}&impresion=${document.getElementById('cambioimpresion_evo').value}&tratamiento=${document.getElementById('tratamiento_evo').value}&resultados=${document.getElementById('resultadotrata_evo').value}`;
        axios.get('/historias/save-evolucion'+envioData)
            .then(function (response) {
                $('#registro-evaluacion').modal('hide');
                document.getElementById('formulario-registro-evaluacion').reset();
                drawEvoluciones(response.data);
                Swal.fire({
                    title: 'Evolucion registrada con exito',
                    text: "su guardo el registro de evolucion en la base de datos",
                    icon: 'success',
                    timer: 3000,
                })
            })
            .catch(function (error) {
                console.log(error);
            });
        console.log('se esta enviando...');
    });

    /** pinta los datos de las evoluciones al cargar la pagina */
    document.addEventListener("DOMContentLoaded", function(){
        loadEvoluciones();
    });

    /** logica para pintar los datos */
    function drawEvoluciones(datos)
    {
        var cardItems = '';
        var statusHistoria = document.getElementById('status-historia').innerHTML;
        if ( datos.length == 0 ){
            cardItems = 'No hay registros de evoluciones del paciente';
        }else {
            datos.forEach(element=>{
                cardItems += `
                <div class="card-evoluciones">
                    <div class="title col-3">
                        <p class="mb-0">Fecha</p>
                        <h5 class="mt-0">${element.date_creation}</h5>
                    </div>
                    <div class="col-9">
                        <p class="mb-0">Dato objetivo</p>
                        <h5 class="mt-0">${element.dato_objetivo}</h5>
                        <div class="row justify-content-end">`;
                    if(statusHistoria !== 'Historia finalizada'){
                        cardItems +=  
                            `<a href="javascript:void(0)" evolucion="${element.codigo_evolucion}" class="btn btn-danger mr-1 eliminarEvo">
                                <i class="fas fa-trash "></i>
                            </a>
                            <a data-toggle="modal" href="#editar-evaluacion" evolucion="${element.codigo_evolucion}" class="btn btn-warning mr-1 editarEvo">
                                <i class="fas fa-pencil-alt "></i>
                            </a>`;
                    }
                    cardItems +=
                            `<a data-toggle="modal" href="#detalle-evaluacion" evolucion="${element.codigo_evolucion}" class="btn btn-success verEvo">
                                <i class="fas fa-eye"></i>
                            </a>
                        </div>
                    </div>
                </div>`;
            });
        }
        document.getElementById('content-data-evoluciones').innerHTML = cardItems;
        initLoadBotonesActionEvolucion();
    }

    function loadEvoluciones(){
        axios.get('/historias/return-evolution?historia='+document.getElementById('codigoHistoriaActual').value)
            .then(function (response) {
            return response.data;
            })
            .then(function(value){
                console.log(value);
                drawEvoluciones(value);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    //carga las funciones escuchas para cada boton en particular
    function initLoadBotonesActionEvolucion()
    {
        /** eliminar un registro de evolucion */
        document.querySelectorAll('.eliminarEvo').forEach(element=>{
            element.addEventListener('click',e=>{
                Swal.fire({
                    title: '¿Esta seguro de eliminar un registro de evolución?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminarlo'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        axios.get('/historias/delete-evolution?evolucion='+element.getAttribute('evolucion'))
                            .then(function (response) {
                                loadEvoluciones();
                                Swal.fire({
                                    title: 'Evolucion eliminada con exito',
                                    text: "su registro eliminado de la base de datos",
                                    icon: 'success',
                                    timer: 3000,
                                })
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    }
                })
            });
        });

        /** ver un registro */
        document.querySelectorAll('.verEvo').forEach(element=>{
            element.addEventListener('click',e=>{
                axios.get('/historias/find-evolution?evolucion='+element.getAttribute('evolucion'))
                    .then(function (response) {
                        return response.data;
                    })
                    .then(function(value){
                        document.getElementById('view_hallazgos').innerHTML = value.estado_hallazgo;
                        document.getElementById('view_objetivo').innerHTML = value.dato_objetivo;
                        document.getElementById('view_sintomas').innerHTML = value.nuevos_sintomas;
                        document.getElementById('view_complicaciones').innerHTML = value.complicaciones;
                        document.getElementById('view_impresion').innerHTML = value.cambio_impresion;
                        document.getElementById('view_tratamiento').innerHTML = value.tratamiento_seguido;
                        document.getElementById('view_resultado').innerHTML = value.resultado_tratamiento;
                        document.getElementById('view_data').innerHTML = value.date_creation;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            });
        });

        /** ver formulario de edicion un registro */
        document.querySelectorAll('.editarEvo').forEach(element=>{
            element.addEventListener('click',e=>{
                axios.get('/historias/find-evolution?evolucion='+element.getAttribute('evolucion'))
                    .then(function (response) {
                        return response.data;
                    })
                    .then(function(value){
                        document.getElementById('hallazgos_evo-edit').value = value.estado_hallazgo;
                        document.getElementById('dato_objetivo-edit').value = value.dato_objetivo;
                        document.getElementById('nuevossitomas_evo-edit').value = value.nuevos_sintomas;
                        document.getElementById('complicaciones_evo-edit').value = value.complicaciones;
                        document.getElementById('cambioimpresion_evo-edit').value = value.cambio_impresion;
                        document.getElementById('tratamiento_evo-edit').value = value.tratamiento_seguido;
                        document.getElementById('resultadotrata_evo-edit').value = value.resultado_tratamiento;
                        document.getElementById('codigoEvolucion').value = element.getAttribute('evolucion');
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            });
        });
    }

    //formulario de editar evolucion
    document.getElementById('formulario-edicion-evaluacion').addEventListener('submit',event=>{
        event.preventDefault();
        var envioData = `?historia_actual=${document.getElementById('codigoHistoriaActual').value}&hallazgos=${document.getElementById('hallazgos_evo-edit').value}&data_objetivo=${document.getElementById('dato_objetivo-edit').value}&sistomas=${document.getElementById('nuevossitomas_evo-edit').value}&complicaciones=${document.getElementById('complicaciones_evo-edit').value}&impresion=${document.getElementById('cambioimpresion_evo-edit').value}&tratamiento=${document.getElementById('tratamiento_evo-edit').value}&resultados=${document.getElementById('resultadotrata_evo-edit').value}&codigoEvaluacion=${document.getElementById('codigoEvolucion').value}`;
        axios.get('/historias/update-evolution'+envioData)
            .then(function (response) {
                $('#editar-evaluacion').modal('hide');
                loadEvoluciones();
                Swal.fire({
                    title: 'Evolucion actualizada con exito con exito',
                    text: "El registro de evolucion se actualizo en la base de datos",
                    icon: 'success',
                    timer: 3000,
                })
            })
            .catch(function (error) {
                console.log(error);
            });
        console.log('se esta enviando...');
    });

</script>