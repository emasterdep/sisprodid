<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes">pacientes</a></li>
                    <li class="breadcrumb-item"><a href="/pacientes/">Detalle paciente</a></li>
                    <li class="breadcrumb-item active">Registrar historia clinica</li>
                </ol>
            </div>
            <h4 class="page-title">Registrar nueva historia clinica</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'repeatcedula' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Medico existe!</strong> Al parecer el medico que desea ingresar ya existe en el sistema
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <p>Este formulario es para crear una historia actual</p>
                        <form action="/historia-medica/actual/save" method="POST" class="row">
                            <!-- titulo  -->
                            <div class="col-12 form-group bg-primary">
                                <h5 class="text-white">Contacto de emergencia</h5>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="nombre">Nombre</label>
                                <input type="text" id="nombre" name="nombre" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="telefono">teléfono</label>
                                <input type="number" id="telefono" name="telefono" class="form-control" required>
                            </div>
                            <!-- titulo  -->
                            <div class="col-12 form-group bg-primary">
                                <h5 class="text-white">Justificación</h5>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="motivoAdmision">Motivo de admisión</label>
                                <textarea name="motivoAdmision" id="motivoAdmision" cols="30" rows="5" class="form-control" placeholder="Llene aquí..."></textarea>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="motivoEnferma">Enfermedad actual</label>
                                <textarea name="motivoEnferma" id="motivoEnferma" cols="30" rows="5" class="form-control" placeholder="Hacer el relato conciso y completo de las dolencias, indicando fecha de comienzo, duración y tratamiento a cada una de ellas..."></textarea>
                            </div>
                            <div class="form-group mb-3 col-md-12">
                                <label for="diagnosticoAdmision">Diagnostico admisión</label>
                                <textarea name="diagnosticoAdmision" id="diagnosticoAdmision" cols="30" rows="5" class="form-control" placeholder="Llene aquí..."></textarea>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="date">Fecha de ingreso</label>
                                <input type="date" id="date" name="date" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="hour">Hora de ingreso</label>
                                <input type="time" id="hour" name="hour" class="form-control" required>
                            </div>
                            <!-- titulo  -->
                            
                            <input value="<?=$this->e($paciente['codigo_paciente'])?>" type="hidden" id="idPaciente" name="idPaciente">
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Continuar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
