<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/user">Usuario</a></li>
                    <li class="breadcrumb-item"><a href="/user/roles">Roles</a></li>
                    <li class="breadcrumb-item">Actualizar rol</li>
                </ol>
            </div>
            <h4 class="page-title">Actualizar rol</h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Formulario para actualizar de rol de usuario</h4>
                <div class="row">
                    <div class="col-12">
                        <form action="/user/roles/update" method="POST">
                            <div class="form-group mb-3">
                                <label for="nameRol">Nombre</label>
                                <input type="text" id="nameRol" name="nameRol" class="form-control" value="<?=$this->e($rol['nombre_rol'])?>">
                                <input type="hidden" name="idRol" value="<?=$this->e($rol['codigo_rol'])?>">
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Actulizar dato</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
