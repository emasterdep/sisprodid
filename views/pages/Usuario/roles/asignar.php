<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/user">Usuario</a></li>
                    <li class="breadcrumb-item"><a href="/user/roles">Roles</a></li>
                    <li class="breadcrumb-item">Asignar permisos</li>
                </ol>
            </div>
            <h4 class="page-title">Asignar permiso al Rol <?=$this->e($rol['nombre_rol'])?></h4>
        </div>
    </div>
</div>     

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Lista de permisos a asignar</h4>
                <div class="row">
                    <div class="col-12">
                        <form action="/user/rol/asign" method="GET" class="col-12 row">
                        <!-- localidad sistema  <h5 class="col-12 bg-gray" style="background: lightgray;padding: 7px;">Localidad</h5> -->
                            
                            <?php foreach($permises as $permise): ?>
                                <div class="form-group mb-3 col-md-3 col-6">
                                    <input <?php if( ifPermise($permise['nombre_permiso'],$rol['codigo_rol']) ) { echo 'checked'; } ?> type="checkbox" name="<?=$this->e($permise['codigo_permiso'])?>" id="" value="<?=$this->e($permise['codigo_permiso'])?>" class="form-checkbox">
                                    <label for="nameRol"><?=str_replace('_',' ',$permise['nombre_permiso'])?></label>
                                </div>
                            <?php endforeach; ?>
                            <!-- boton de enviar -->
                            <div class="form-group mb-3">
                                <input type="hidden" name="idRol" value="<?=$this->e($rol['codigo_rol'])?>">
                                <button type="submit" class="btn btn-primary btn-block">Guardar cambios</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
