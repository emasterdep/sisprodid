<?php $this->layout('layouts/app') ?>

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/">sisprodi</a></li>
                    <li class="breadcrumb-item"><a href="/usuarios">Usuario</a></li>
                    <li class="breadcrumb-item active">Editar usuario</li>
                </ol>
            </div>
            <h4 class="page-title">Editar usuario</h4>
        </div>
    </div>
</div>     

<?php if( isset($_GET['action']) && $_GET['action'] == 'repeatcedula' ): ?>
<div class="row">
    <div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Medico existe!</strong> Al parecer el medico que desea ingresar ya existe en el sistema
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Formulario de edición de usuario</h4>

                <div class="row">
                    <div class="col-12">
                        <form action="/user/update" method="POST" class="row">
                            <div class="form-group mb-3 col-md-6">
                                <label for="cedulaMedico">Cedula</label>
                                <input value="<?=$this->e($usuario['cedula'])?>" type="number" id="cedulaPaciente" name="cedulaPaciente" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="correoUser">Correo</label>
                                <input value="<?=$this->e($usuario['correo'])?>" type="email" id="correoUser" name="correoUser" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="nameUser">Nombre</label>
                                <input value="<?=$this->e($usuario['nombres'])?>" type="text" id="nameUser" name="nameUser" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="lastnameUser">Apellido</label>
                                <input value="<?=$this->e($usuario['apellidos'])?>" type="text" id="lastnameUser" name="lastnameUser" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="phone">Número de teléfono</label>
                                <input value="<?=$this->e($usuario['telefono'])?>" type="number" id="phone" name="phone" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="datebirth">Fecha de nacimiento</label>
                                <input value="<?=$this->e($usuario['fecha_nacimiento'])?>" type="date" id="datebirth" name="datebirth" class="form-control" required>
                            </div>
                            <!-- seccion para datos de usuario -->
                            <div class="form-group mb-3 col-md-6">
                                <label for="userNameID">Nombre de usuario</label>
                                <input value="<?=$this->e($usuario['username'])?>" type="text" id="userNameID" name="userNameID" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="passUser">Contraseña</label>
                                <input value="<?=$this->e($usuario['contraseña'])?>" type="password" id="passUser" name="passUser" class="form-control" required>
                                <input value="<?=$this->e($usuario['codigo_usuario'])?>" type="hidden" name="passOldUser">
                            </div>
                            <!-- seccion para rol de usuario-->
                            <div class="form-group mb-3 col-md-6">
                                <label for="genero">Genero</label>
                                <select value="<?=$this->e($usuario['sexo'])?>" name="genero" id="genero" class="form-control" required>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                            <div class="form-group mb-3 col-md-6">
                                <label for="rolUser">Rol a asignar</label>
                                <?php if( isset($roles) && count($roles) > 0): ?> 
                                <select value="<?=$this->e($usuario['codigo_rol'])?>" name="rolUser" id="rolUser" class="form-control" required>
                                    <?php foreach($roles as $rol): ?>
                                    <option value="<?=$this->e($rol['codigo_rol'])?>" <?php if($usuario['codigo_rol'] == $rol['codigo_rol']){ echo 'selected';} ?>><?=$this->e($rol['nombre_rol'])?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php else: ?>
                                    <p>No hay roles creados, por favor vaya a la sección de roles para crear un rol</p>
                                <?php endif; ?>
                            </div>
                            <input value="<?=$this->e($usuario['codigo_usuario'])?>" type="hidden" name="userId" >
                            <div class="form-group col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block">Continuar</button>
                            </div>
                        </form>
                    </div> <!-- end col -->

                </div>

            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
