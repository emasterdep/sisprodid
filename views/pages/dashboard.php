<?php $this->layout('layouts/app') ?>

 <!-- start page title -->

<div class="row header-dashboard1">
    <div class="col-12" >
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">sisprodi</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
            <h4 class="page-title" style="color: #fff !important">Escritorio</h4>
        </div>
    </div>
</div>     


<div class="row header-dashboard2">
    <div class="col-xl-3 col-md-6">
        <div class="card-box">
            

            <h4 class="header-title mt-0 mb-2">Total pacientes</h4>

            <div class="mt-1">
                <div class="float-left" dir="ltr">
                   <!--  <img src="<?=assets('img/avatar.png')?>" style="display: inline !important;" width="20%" alt="membrete salud"> -->
                </div>
                <div class="text-right">
                    <h2 class="mt-3 pt-1 mb-1"> <?=$tipos['todos']?></h2>
                    <a href="#"><p class="text-muted mb-0"> Ver pacientes</p></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div><!-- end col -->

    <div class="col-xl-3 col-md-6">
        <div class="card-box">
            <h4 class="header-title mt-0 mb-3">Diabetes tipo 1</h4>
             
            <div class="mt-1">
                <div class="float-left" dir="ltr">
                    <input data-plugin="knob" data-width="64" data-height="64" data-fgColor="#f05050 "
                        data-bgColor="#F9B9B9" value="<?php calculatePorcentaje($tipos['todos'],$tipos['tipo1']); ?>"
                        data-skin="tron" data-angleOffset="180" data-readOnly=true
                        data-thickness=".15"/>
                </div>
                <div class="text-right">
                    <h2 class="mt-3 pt-1 mb-1"> <?=$tipos['tipo1']?> </h2>
                    <a href="#"><p class="text-muted mb-0"> Ver pacientes</p></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div><!-- end col -->

    <div class="col-xl-3 col-md-6">
        <div class="card-box">
           

            <h4 class="header-title mt-0 mb-3">Diabetes tipo 2</h4>

            <div class="mt-1">
                <div class="float-left" dir="ltr">
                    <input data-plugin="knob" data-width="64" data-height="64" data-fgColor="#ffbd4a"
                        data-bgColor="#FFE6BA" value="<?php calculatePorcentaje($tipos['todos'],$tipos['tipo2']); ?>"
                        data-skin="tron" data-angleOffset="180" data-readOnly=true
                        data-thickness=".15"/>
                </div>
                <div class="text-right">
                    <h2 class="mt-3 pt-1 mb-1"> <?=$tipos['tipo2']?> </h2>
                     <a href="#"><p class="text-muted mb-0"> Ver pacientes</p></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div><!-- end col -->

    <div class="col-xl-3 col-md-6">
        <div class="card-box">
        

            <h4 class="header-title mt-0 mb-3">Diabetes gestacional</h4>

            <div class="mt-1">
                <div class="float-left" dir="ltr">
                    <input data-plugin="knob" data-width="64" data-height="64" data-fgColor="#23b397"
                        data-bgColor="#c8ece5" value="<?php calculatePorcentaje($tipos['todos'],$tipos['tipo3']); ?>"
                        data-skin="tron" data-angleOffset="<?php calculatePorcentaje($tipos['todos'],$tipos['tipo3']); ?>" data-readOnly=true
                        data-thickness=".15"/>
                </div>
                <div class="text-right">
                    <h2 class="mt-3 pt-1 mb-1"> <?=$tipos['tipo3']?> </h2>
                    <a href="#"><p class="text-muted mb-0"> Ver pacientes</p></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div><!-- end col -->

</div>
<!-- end row -->

<div class="row">
    
    <div class="col-xl-12">
        <div class="card-box historial-reciente-dashboard ">
           
            <h4 class="header-title mb-3">Pacientes recientes</h4>

            <div class="table-responsive">
                <table class="table table-centered table-borderless table-hover table-nowrap mb-0" id="datatable">
                    <thead class="thead-light">
                    <tr>
                        <th>Nombre</th>
                        <th>cedula</th>
                        <th>Genero</th>
                        <th>Fecha de nacimiento</th>
                        <th>Teléfono</th>
                        <th>Tipo diabetes</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if( count($pacientes) > 0){ 
                            foreach($pacientes as $paciente): ?>
                            <tr>
                                <td><a href="/pacientes/<?=$this->e($paciente['codigo_paciente'])?>"><?=$this->e($paciente['nombres'])?> <?=$this->e($paciente['apellidos'])?> </a></td>
                                <td><?=$this->e($paciente['cedula'])?></td>
                                <td><?=$this->e($paciente['sexo'])?></td>
                                <td><?=$this->e($paciente['fecha_nacimiento'])?></td>
                                <td><?=$this->e($paciente['telefono'])?></td>
                                <td><?=$this->e($paciente['tipo_diabetes'])?></td>
                            </tr>
                        <?php endforeach; 
                        }?>
                    </tbody>
                </table>
            </div> <!-- end table-responsive -->

        </div> <!-- end card-box-->
    </div> <!-- end col-->

</div>
<!-- end row -->    

