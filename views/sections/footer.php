
<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <span><img src="<?=assets('img/uptos.png')?>" alt="" height="25"></span>
                <?=date('Y')?> &copy; Sisprodi - Sistema Para Departamento de Diabetes 
            </div>
          
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                     <img src="<?=assets('img/membrete-ministerio-del-poder-popular para la salud.jpg')?>" width="50%" alt="membrete salud">
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->