<!-- Topbar Start -->
<div class="navbar-custom">


    <ul class="list-unstyled topnav-menu float-right mb-0">

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="<?=assets('img/user-1.jpg')?>" alt="user-image" class="rounded-circle">
                <span class="pro-user-name ml-1">
                    <i class="mdi mdi-chevron-down"></i> 
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-item noti-title" style="background-color: #0245ab;"> 
                    <h5 class="m-0 text-white">
                        Bienvenido
                    </h5>
                </div>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="fe-user"></i>
                    <span>Mi cuenta</span>
                </a>

                <!-- item-->
                <?php if ( ifPermise('ajustes') ): ?>
                <a href="/ajustes" class="dropdown-item notify-item">
                    <i class="fe-settings"></i>
                    <span>Ajustes</span>
                </a>
                <?php endif; ?>

                <div class="dropdown-divider"></div>

                <!-- item-->
                <a href="/logout" class="dropdown-item notify-item">
                    <i class="fe-log-out"></i>
                    <span>Salir</span>
                </a>

            </div>
        </li>
    </ul>


    <!-- LOGO -->
    <div class="logo-box">
        <a href="/dashboard" class="logo text-center">
            <span class="logo-lg">
                <img src="<?=assets('img/logo-light.png')?>" alt="" height="24">
                <!-- <span class="logo-lg-text-light">Upvex</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-sm-text-dark">X</span> -->
                <img src="<?=assets('img/logo-sm.png')?>" alt="" height="28">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile waves-effect">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </li>

    </ul>

       <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <?php if ( ifPermise('ver_paciente') ): ?>
                <li class="d-none d-sm-block">
                    <form class="app-search" action="/pacientes/search" method="POST">
                        <div class="app-search-box">
                            <div class="input-group">
                                <input type="text" name="cedula" class="form-control" placeholder="Cedula paciente...">
                                <div class="input-group-append">
                                    <button class="btn" type="submit">
                                        <i class="fe-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </li> 
                <?php endif; ?>
        </ul>



   <!-- <div class="membrete-salud">
        <img src="<?=assets('img/membrete-ministerio-del-poder-popular para la salud.jpg')?>" alt="membrete salud">
    </div> -->
</div>
<!-- end Topbar -->