            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">
                <div class="slimscroll-menu">
                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Navegación</li>
                            <li>
                                <a href="/dashboard">
                                    <i class="la la-dashboard"></i>
                                    <span> Escritorio </span>
                                </a>
                            </li>
                            <?php if ( ifPermise('todo_paciente') || ifPermise('ver_paciente') ): ?>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="la la-users"></i>
                                    <span> Pacientes</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <!-- separacion -->
                                    <?php if ( ifPermise('ver_paciente') ): ?>
                                    <li>
                                        <a href="/pacientes">Todos los pacientes</a>
                                    </li>
                                    <?php endif; ?>
                                    <!-- separacion -->
                                    <?php if ( ifPermise('crear_paciente') ): ?>
                                    <li>
                                        <a href="/pacientes/create">Registrar paciente</a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                            <?php endif; ?>
           
                            <?php if ( ifPermise('todo_medicos') || ifPermise('ver_medicos') ): ?>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="la la-cube"></i>
                                    <span>Medicos</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <!-- separacion -->
                                    <?php if ( ifPermise('ver_medicos') ): ?>
                                    <li>
                                        <a href="/medicos">Todos los medicos</a>
                                    </li>
                                    <?php endif; ?>
                                    <!-- separacion -->
                                    <?php if ( ifPermise('crear_medicos') ): ?>
                                     <li>
                                        <a href="/medicos/create">Registrar medico</a>
                                    </li>
                                    <?php endif; ?>
                                    <!-- separacion -->
                                    <?php if ( ifPermise('ver_especialidad') ): ?>
                                    <li>
                                        <a href="/medicos/especialidades">Especialidades</a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                            <?php endif; ?>

                            <?php if ( ifPermise('todo_medicamento') || ifPermise('ver_medicamento') ): ?>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="la la-medkit "></i>
                                    <span>Medicamentos</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <!-- separacion -->
                                    <?php if ( ifPermise('ver_almacenes') ): ?>
                                    <li>
                                        <a href="/medicamentos/almacenes">Almacenes</a>
                                    </li>
                                    <?php endif; ?>
                                    <!-- separacion -->
                                    <?php if ( ifPermise('ver_medicamento') ): ?>
                                    <li>
                                        <a href="/medicamentos">Medicinas</a>
                                    </li>
                                    <?php endif; ?>
                                    <!-- separacion -->
                                    <?php if ( ifPermise('ver_lotes') ): ?>
                                    <li>
                                        <a href="/medicamentos/lotes">Lotes</a>
                                    </li>
                                    <?php endif; ?>
                                    <!-- separacion -->
                                    <?php if ( ifPermise('todo_medicamento') ): ?>
                                    <li>
                                        <a href="/pacientes/entrega-medicamento">Listado de entregas</a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                            <?php endif; ?>


                            <?php if ( ifPermise('soy_admin') ): ?>
                            <li class="menu-title mt-2">Admin</li>
                            <?php endif; ?>

                            <?php if ( ifPermise('todo_usuario') || ifPermise('ver_usuario') ): ?>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="la la-user"></i>
                                    <span>Usuarios</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                     <li>
                                        <a href="/users">Todos los usuarios</a>
                                    </li>
                                     <li>
                                        <a href="/user/create">Registrar usuario</a>
                                    </li>
                                    <li>
                                        <a href="/user/roles">Roles</a>
                                    </li>
                                   
                                </ul>
                            </li>
                            <?php endif; ?>






                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->