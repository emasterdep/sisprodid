/**
@version 3.0
create database sistema_medico 
**/

/**seccion de localidad para la base de datos**/
CREATE TABLE estados (
	codigo_estado varchar(200) PRIMARY KEY,
	nombre_estado varchar(30),
	status_delete ENUM('on','off'),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE municipios (
	codigo_municipio varchar(200) PRIMARY KEY,
	codigo_estado varchar(200),
	nombre_municipio varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_estado) REFERENCES estados (codigo_estado) ON DELETE CASCADE
);

CREATE TABLE parroquias (
	codigo_parroquia varchar(200) PRIMARY KEY,
	codigo_municipio varchar(200),
	nombre_parroquia varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_municipio) REFERENCES municipios (codigo_municipio) ON DELETE CASCADE
);

CREATE TABLE comunidades (
	codigo_comunidad varchar(200) PRIMARY KEY,
	codigo_parroquia varchar(200),
	nombre_comunidad varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_parroquia) REFERENCES parroquias (codigo_parroquia) ON DELETE CASCADE
);

CREATE TABLE sectores (
	codigo_sector varchar(200) PRIMARY KEY,
	codigo_comunidad varchar(200),
	nombre_sector varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_comunidad) REFERENCES comunidades (codigo_comunidad) ON DELETE CASCADE
);
/** tablas para viviendas de los pacientes **/
CREATE TABLE viviendas (
	codigo_vivienda varchar(200) PRIMARY KEY,
	codigo_sector varchar(200),
	numero_habitaciones int,
	tipo varchar(20),
	estructura varchar(20),
	piso varchar(20),
	techo varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_sector) REFERENCES sectores (codigo_sector) ON DELETE CASCADE
);

CREATE TABLE servicios (
	codigo_servicio varchar(200) PRIMARY KEY,
	nombre_servicio varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE servicios_viviendas (
	codigo_servicio_vivienda varchar(200) PRIMARY KEY,
	codigo_vivienda varchar(200),
	codigo_servicio varchar(200),
	estado_servicio boolean,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_vivienda) REFERENCES viviendas (codigo_vivienda) ON DELETE CASCADE,
	FOREIGN KEY (codigo_servicio) REFERENCES servicios (codigo_servicio) ON DELETE CASCADE
);

/** tablas para usuarios y roles **/
CREATE TABLE roles (
	codigo_rol varchar(200) PRIMARY KEY,
	nombre_rol varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE permisos (
	codigo_permiso varchar(200) PRIMARY KEY,
	nombre_permiso varchar(50),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE permisos_roles (
	codigo_permiso_rol varchar(200) PRIMARY KEY,
	codigo_permiso varchar(200),
	codigo_rol varchar(200),
	estado_permiso boolean,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (codigo_permiso) REFERENCES permisos (codigo_permiso) ON DELETE CASCADE,
	FOREIGN KEY (codigo_rol) REFERENCES roles (codigo_rol) ON DELETE CASCADE
);
/** tablas relacionadas a personas **/
CREATE TABLE personas (
	codigo_persona varchar(200) PRIMARY KEY,
	cedula varchar(10) UNIQUE,
	nombres varchar(20),
	apellidos varchar(20),
	sexo varchar(2),
	fecha_nacimiento DATE, 
	telefono varchar(12),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE usuarios (
	codigo_usuario varchar(200) PRIMARY KEY,
	correo varchar(20) UNIQUE,
	contraseña varchar(200),
	username varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_usuario) REFERENCES personas (codigo_persona) ON DELETE CASCADE
);

CREATE TABLE roles_usuarios (
	codigo_rol_usuario varchar(200) PRIMARY KEY,
	codigo_usuario varchar(200),
	codigo_rol varchar(200),
	estado_rol varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_usuario) REFERENCES usuarios (codigo_usuario) ON DELETE CASCADE,
	FOREIGN KEY (codigo_rol) REFERENCES roles (codigo_rol) ON DELETE CASCADE
);
/**seccion de medicos del sistema**/
CREATE TABLE medicos (
	codigo_medico varchar(200) PRIMARY KEY,
	impremedico varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_medico) REFERENCES personas (codigo_persona) ON DELETE CASCADE
);

CREATE TABLE especialidades (
	codigo_especialidad varchar(200) PRIMARY KEY,
	nombre_especialidad varchar(25),	
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE especialidades_medicos (
	codigo_especialidad_medico varchar(200) PRIMARY KEY,
	codigo_especialidad varchar(200),
	codigo_medico varchar(200),
	date_get date,
	universidad varchar(50),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_medico) REFERENCES medicos (codigo_medico) ON DELETE CASCADE,
	FOREIGN KEY (codigo_especialidad) REFERENCES especialidades (codigo_especialidad) ON DELETE CASCADE
);

/** seccion del usuario general **/
CREATE TABLE pacientes (
	codigo_paciente varchar(200) PRIMARY KEY,
	codigo_vivienda varchar(200),
	date_creation TIMESTAMP,
	tipo_diabetes ENUM('1','2','3','4'),
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_vivienda) REFERENCES viviendas (codigo_vivienda) ON DELETE CASCADE
);

CREATE TABLE registros_morbilidades (
	codigo_morbilidad varchar(200) PRIMARY KEY,
	codigo_paciente varchar(200),
	motivo varchar(25),
	fecha_morbilidad date,
	conducta_seguir boolean,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_paciente) REFERENCES pacientes (codigo_paciente) ON DELETE CASCADE
);

/* mejorar esta parte de las historias de los usuarios - hasta las historias clinicas y de hospitalización
* junto con las historias actuales de cada tipo de historia

en el registro de morbilidad - tipo de tratamiento
 */
CREATE TABLE historias_clinicas(
	codigo_historia varchar(200) PRIMARY KEY,
	hora_creacion TIME,
	fecha_historia TIMESTAMP,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_historia) REFERENCES pacientes (codigo_paciente) ON DELETE CASCADE
);

CREATE TABLE historias_actuales (
	codigo_historia_actual varchar(200) PRIMARY KEY,
	codigo_historia varchar(200),
	creacion_historia DATE,
	hora_creacion TIME,
	contacto_emergencia varchar(20),
	telefono_contacto_emergencia varchar(13),
	motivo_admision TEXT,
	enfermedad_actual TEXT,
	diagnostico_adminsion TEXT,
	condicion_salida ENUM('curacion','mejoria','muerte','otro'),
	estatus_historia ENUM('on','off'),
	diagnostico_final TEXT,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_historia) REFERENCES historias_clinicas (codigo_historia) ON DELETE CASCADE
);

CREATE TABLE habitos (
	codigo_habito varchar(200) PRIMARY KEY,
	nombre varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE habitos_historia (
	codigo_habito_historia varchar(200) PRIMARY KEY,
	codigo_historia varchar(200),
	codigo_habito varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_historia) REFERENCES historias_actuales (codigo_historia_actual) ON DELETE CASCADE,
	FOREIGN KEY (codigo_habito) REFERENCES habitos (codigo_habito) ON DELETE CASCADE
);

CREATE TABLE evoluciones (
	codigo_evolucion varchar(200) PRIMARY KEY,
	codigo_historia_actual varchar(200),
	estado_hallazgo TEXT,
	dato_objetivo TEXT,
	nuevos_sintomas TEXT,
	complicaciones TEXT,
	cambio_impresion TEXT,
	tratamiento_seguido TEXT,
	resultado_tratamiento TEXT,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_historia_actual) REFERENCES historias_actuales (codigo_historia_actual) ON DELETE CASCADE
);

CREATE TABLE examenes_funcionales (
	codigo_examen varchar(200) PRIMARY KEY,
	codigo_historia_actual varchar(200),
	nombre varchar(100),
	descripcion TEXT,
	dato_objetivo TEXT,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_historia_actual) REFERENCES historias_actuales (codigo_historia_actual) ON DELETE CASCADE
);

/** seccion de medicamentos **/
CREATE TABLE almacenes (
	codigo_almacen varchar(200) PRIMARY KEY,
	titulo varchar(200) UNIQUE,
	ubicacion TEXT,
	disponible integer,
	disponibilidad_total integer,
	tipo varchar(100),
	ancho varchar(20),
	largo varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE medicamentos (
	codigo_medicamento varchar(200) PRIMARY KEY,
	nombre_medicamento varchar(30),
	imagen TEXT,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE solicitudes (
	codigo_solicitud varchar(200) PRIMARY KEY,
	cantidad integer,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE lotes (
	codigo_lote varchar(200) PRIMARY KEY,
	fecha_emision date,
	fecha_vencimiento date,
	cantidad_existencia integer,
	unidades integer,
	codigo_almacen varchar(200),
	codigo_medicamento varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_almacen) REFERENCES almacenes (codigo_almacen) ON DELETE CASCADE,
	FOREIGN KEY (codigo_medicamento) REFERENCES medicamentos (codigo_medicamento) ON DELETE CASCADE
);

CREATE TABLE entregas (
	codigo_entrega varchar(200) PRIMARY KEY,
	codigo_paciente varchar(200),
	observacion TEXT,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_paciente) REFERENCES pacientes (codigo_paciente) ON DELETE CASCADE
);

/** entregas de medicamentos a un paciente **/
CREATE TABLE entregas_medicamentos (
	codigo_entrega_medicamento varchar(200) PRIMARY KEY,
	codigo_entrega varchar(200),
	codigo_lote varchar(100),
	cantidad_entregada integer,
	dosis integer,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_entrega) REFERENCES entregas (codigo_entrega) ON DELETE CASCADE,
	FOREIGN KEY (codigo_lote) REFERENCES lotes (codigo_lote) ON DELETE CASCADE
);

/* modificaciones a las tablas existentes */
/* PACIENTES */
ALTER TABLE pacientes ADD FOREIGN KEY (codigo_paciente) REFERENCES personas (codigo_persona); 
ALTER TABLE pacientes ADD ocupacion TEXT;
ALTER TABLE pacientes ADD glucometro ENUM('si','no'); 
ALTER TABLE pacientes ADD fumador ENUM('si','no'); 
ALTER TABLE entregas ADD status_falla ENUM('si','no') DEFAULT 'no';
ALTER TABLE entregas ADD actualizacion_falla TEXT;

/* LOTES */
ALTER TABLE lotes ADD presentacion varchar(50); 



/* permisologia del sistema */
INSERT INTO permisos (codigo_permiso,nombre_permiso,date_creation,status_delete) VALUES 
('est01crear01','crear_estado','2021-08-20 20:00:00','on'),
('est01edita01','editar_estado','2021-08-20 20:00:00','on'),
('est01borra01','borrar_estado','2021-08-20 20:00:00','on'),
('est01ver01','ver_estado','2021-08-20 20:00:00','on'),
('mun02crear02','crear_municipio','2021-08-20 20:00:00','on'),
('mun02editar02','editar_municipio','2021-08-20 20:00:00','on'),
('mun02borrar02','borrar_municipio','2021-08-20 20:00:00','on'),
('mun02ver02','ver_municipio','2021-08-20 20:00:00','on'),
('par03crear03','crear_parroquia','2021-08-20 20:00:00','on'),
('par03editar03','editar_parroquia','2021-08-20 20:00:00','on'),
('par03borrar03','borrar_parroquia','2021-08-20 20:00:00','on'),
('par03ver03','ver_parroquia','2021-08-20 20:00:00','on'),
('com04crear04','crear_comunidad','2021-08-20 20:00:00','on'),
('com04editar04','editar_comunidad','2021-08-20 20:00:00','on'),
('com04borrar04','borrar_comunidad','2021-08-20 20:00:00','on'),
('com04ver04','ver_comunidad','2021-08-20 20:00:00','on'),
('ser05crear05','crear_servicio','2021-08-20 20:00:00','on'),
('ser05editar05','editar_servicio','2021-08-20 20:00:00','on'),
('ser05borrar05','borrar_servicio','2021-08-20 20:00:00','on'),
('ser05ver05','ver_servicio','2021-08-20 20:00:00','on'),
('rol06crear06','crear_rol','2021-08-20 20:00:00','on'),
('rol06editar06','editar_rol','2021-08-20 20:00:00','on'),
('rol06borrar06','borrar_rol','2021-08-20 20:00:00','on'),
('rol06ver06','ver_rol','2021-08-20 20:00:00','on');


/* permisos para medicamentos */
INSERT INTO permisos (codigo_permiso,nombre_permiso,date_creation,status_delete) VALUES  
('medicamenot01ver01','ver_medicamento','2021-08-20 20:00:00','on'),
('medicamenot02editar01','editar_medicamento','2021-08-20 20:00:00','on'),
('medicamenot03borrar01','borrar_medicamento','2021-08-20 20:00:00','on'),
('medicamenot04crear01','crear_medicamento','2021-08-20 20:00:00','on'),
('medicamenot05ver01','ver_lotes','2021-08-20 20:00:00','on'),
('medicamenot06editar01','editar_lotes','2021-08-20 20:00:00','on'),
('medicamenot07borrar01','borrar_lotes','2021-08-20 20:00:00','on'),
('medicamenot08crear01','crear_lotes','2021-08-20 20:00:00','on'),
('medicamenot09ver01','ver_almacenes','2021-08-20 20:00:00','on'),
('medicamenot010editar01','editar_almacenes','2021-08-20 20:00:00','on'),
('medicamenot011borrar01','borrar_almacenes','2021-08-20 20:00:00','on'),
('medicamenot012crear01','crear_almacenes','2021-08-20 20:00:00','on');

/* permisos para usuarios */

/* permisos para pacientes */
INSERT INTO permisos (codigo_permiso,nombre_permiso,date_creation,status_delete) VALUES  
('paciente01ver01','ver_paciente','2021-08-20 20:00:00','on'),
('paciente02edit01','editar_paciente','2021-08-20 20:00:00','on'),
('paciente03borrar01','borrar_paciente','2021-08-20 20:00:00','on'),
('paciente04crear01','crear_paciente','2021-08-20 20:00:00','on'),
('paciente05ver01','ver_historia','2021-08-20 20:00:00','on'),
('paciente06borrar01','borrar_historia','2021-08-20 20:00:00','on'),
('paciente07edit01','editar_historia','2021-08-20 20:00:00','on'),
('paciente08crear01','crear_historia','2021-08-20 20:00:00','on'),
('paciente09entrega01','paciente_entregamedicamento','2021-08-20 20:00:00','on'),
('paciente010morb01','registro_morbilidad','2021-08-20 20:00:00','on'),
('paciente011edit02','editar_morbilidad','2021-08-20 20:00:00','on'),
('paciente012borrar02','borrar_morbilidad','2021-08-20 20:00:00','on');

/* permisos para medicos */
INSERT INTO permisos (codigo_permiso,nombre_permiso,date_creation,status_delete) VALUES  
('medicos01ver01','ver_medicos','2021-08-20 20:00:00','on'),
('medicos02ver01','editar_medicos','2021-08-20 20:00:00','on'),
('medicos03ver01','borrar_medicos','2021-08-20 20:00:00','on'),
('medicos04crear01','crear_medicos','2021-08-20 20:00:00','on'),
('medicos05ver01','ver_especialidad','2021-08-20 20:00:00','on'),
('medicos06edit01','edit_especialidad','2021-08-20 20:00:00','on'),
('medicos07borrar01','borrar_especialidad','2021-08-20 20:00:00','on'),
('medicos08crear01','crear_especialidad','2021-08-20 20:00:00','on');

/* permisos para ajustes */
INSERT INTO permisos (codigo_permiso,nombre_permiso,date_creation,status_delete) VALUES  
('ajustes01tot01','ajustes','2021-08-20 20:00:00','on'),
('usuarios02ver01','ver_usuarios','2021-08-20 20:00:00','on'),
('usuarios03editar01','editar_usuarios','2021-08-20 20:00:00','on'),
('usuarios04borrar01','borrar_usuarios','2021-08-20 20:00:00','on'),
('usuarios05crear01','crear_usuarios','2021-08-20 20:00:00','on'),
('rol01ver01','ver_roles','2021-08-20 20:00:00','on'),
('rol02acciones02','acciones_roles','2021-08-20 20:00:00','on'),
('dashboard01ver02','ver_dashboard','2021-08-20 20:00:00','on');
('admin01todo02','soy_admin','2021-08-20 20:00:00','on');

INSERT INTO permisos (codigo_permiso,nombre_permiso,date_creation,status_delete) VALUES  
('medicos20todo','todo_medicos','2021-08-20 20:00:00','on'),
('paciente20todo','todo_paciente','2021-08-20 20:00:00','on'),
('medicamento20todo','todo_medicamento','2021-08-20 20:00:00','on'),
('usuario20todo','todo_usuario','2021-08-20 20:00:00','on');

/* agregando permisos al admin */
INSERT INTO roles (codigo_rol,nombre_rol,date_creation,status_delete) VALUES
('admin03rol02','Administrador','2021-08-20 20:00:00','on');

INSERT INTO permisos_roles (codigo_permiso_rol,codigo_permiso,codigo_rol,estado_permiso,date_creation) VALUES
('admin02todopermiso2021','admin01todo02','admin03rol02',1,'2021-08-20 20:00:00');

INSERT INTO roles_usuarios (codigo_rol_usuario,codigo_usuario,codigo_rol,estado_rol,date_creation,status_delete) VALUES
('admin01eadmin0102todo2021','admin0102todo2021','admin03rol02','on','2021-08-20 20:00:00','on');