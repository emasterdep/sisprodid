<?php

define('ENV',[
    'url_asset' => 'http://localhost:2000/assets/',
    'url_path'  => 'http://localhost:2000'
]);

define('DB',[
    "gsdb"      => 'mysql',
    "host"      => '127.0.0.1',
    "user"      => 'root',
    "pass"      => 'doctor',
    "name"      => 'sisprodi',
    'charset'   => 'utf8'
]);