<?php 

//cargar el core
require_once __DIR__.'/settings.php';
require_once __DIR__.'/../app/Core/sesion.php';
require_once __DIR__.'/../app/Helpers/Helpers.php';
require_once __DIR__.'/../app/Core/conection.php';
require_once __DIR__.'/../app/Core/Model.php';
require_once __DIR__.'/../app/Core/Permission.php';

//modelos
require_once __DIR__.'/../app/Models/Persona.php';
require_once __DIR__.'/../app/Models/Usuario.php';
require_once __DIR__.'/../app/Models/Estado.php';
require_once __DIR__.'/../app/Models/Municipio.php';
require_once __DIR__.'/../app/Models/Parroquea.php';
require_once __DIR__.'/../app/Models/Comunidad.php';
require_once __DIR__.'/../app/Models/Servicio.php';
require_once __DIR__.'/../app/Models/Rol.php';
require_once __DIR__.'/../app/Models/Permiso.php';
require_once __DIR__.'/../app/Models/Especialidad.php';
require_once __DIR__.'/../app/Models/medico.php';
require_once __DIR__.'/../app/Models/Almacen.php';
require_once __DIR__.'/../app/Models/Medicina.php';
require_once __DIR__.'/../app/Models/Lote.php';
require_once __DIR__.'/../app/Models/Paciente.php';
require_once __DIR__.'/../app/Models/Sector.php';
require_once __DIR__.'/../app/Models/Vivienda.php';
require_once __DIR__.'/../app/Models/Historia.php';

//cargar los controladores
require_once __DIR__.'/../app/Controllers/BaseController.php';
require_once __DIR__.'/../app/Controllers/UserController.php';
require_once __DIR__.'/../app/Controllers/LocalidadController.php';
require_once __DIR__.'/../app/Controllers/MunicipioController.php';
require_once __DIR__.'/../app/Controllers/ParroqueaController.php';
require_once __DIR__.'/../app/Controllers/ComunidadController.php';
require_once __DIR__.'/../app/Controllers/ServicioController.php';
require_once __DIR__.'/../app/Controllers/RolController.php';
require_once __DIR__.'/../app/Controllers/EspecialidadController.php';
require_once __DIR__.'/../app/Controllers/MedicoController.php';
require_once __DIR__.'/../app/Controllers/AlmacenController.php';
require_once __DIR__.'/../app/Controllers/MedicinaController.php';
require_once __DIR__.'/../app/Controllers/LoteController.php';
require_once __DIR__.'/../app/Controllers/SectorController.php';
require_once __DIR__.'/../app/Controllers/PacienteController.php';
require_once __DIR__.'/../app/Controllers/HistoriaController.php';