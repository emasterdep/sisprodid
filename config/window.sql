CREATE TABLE estados (
	codigo_estado varchar(200) PRIMARY KEY,
	nombre_estado varchar(30),
	status_delete ENUM('on','off'),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE municipios (
	codigo_municipio varchar(200) PRIMARY KEY,
	codigo_estado varchar(200),
	nombre_municipio varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_estado) REFERENCES estados (codigo_estado) ON DELETE CASCADE
);

CREATE TABLE parroquias (
	codigo_parroquia varchar(200) PRIMARY KEY,
	codigo_municipio varchar(200),
	nombre_parroquia varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_municipio) REFERENCES municipios (codigo_municipio) ON DELETE CASCADE
);

CREATE TABLE comunidades (
	codigo_comunidad varchar(200) PRIMARY KEY,
	codigo_parroquia varchar(200),
	nombre_comunidad varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_parroquia) REFERENCES parroquias (codigo_parroquia) ON DELETE CASCADE
);

CREATE TABLE sectores (
	codigo_sector varchar(200) PRIMARY KEY,
	codigo_comunidad varchar(200),
	nombre_sector varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_comunidad) REFERENCES comunidades (codigo_comunidad) ON DELETE CASCADE
);

CREATE TABLE viviendas (
	codigo_vivienda varchar(200) PRIMARY KEY,
	codigo_sector varchar(200),
	numero_habitaciones int,
	tipo varchar(20),
	estructura varchar(20),
	piso varchar(20),
	techo varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_sector) REFERENCES sectores (codigo_sector) ON DELETE CASCADE
);

CREATE TABLE servicios (
	codigo_servicio varchar(200) PRIMARY KEY,
	nombre_servicio varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE servicios_viviendas (
	codigo_servicio_vivienda varchar(200) PRIMARY KEY,
	codigo_vivienda varchar(200),
	codigo_servicio varchar(200),
	estado_servicio boolean,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_vivienda) REFERENCES viviendas (codigo_vivienda) ON DELETE CASCADE,
	FOREIGN KEY (codigo_servicio) REFERENCES servicios (codigo_servicio) ON DELETE CASCADE
);


CREATE TABLE roles (
	codigo_rol varchar(200) PRIMARY KEY,
	nombre_rol varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE permisos (
	codigo_permiso varchar(200) PRIMARY KEY,
	nombre_permiso varchar(50),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE permisos_roles (
	codigo_permiso_rol varchar(200) PRIMARY KEY,
	codigo_permiso varchar(200),
	codigo_rol varchar(200),
	estado_permiso boolean,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (codigo_permiso) REFERENCES permisos (codigo_permiso) ON DELETE CASCADE,
	FOREIGN KEY (codigo_rol) REFERENCES roles (codigo_rol) ON DELETE CASCADE
);

CREATE TABLE personas (
	codigo_persona varchar(200) PRIMARY KEY,
	cedula varchar(10) UNIQUE,
	nombres varchar(20),
	apellidos varchar(20),
	sexo varchar(2),
	fecha_nacimiento DATE, 
	telefono varchar(12),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE usuarios (
	codigo_usuario varchar(200) PRIMARY KEY,
	correo varchar(20) UNIQUE,
	contraseña varchar(200),
	username varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_usuario) REFERENCES personas (codigo_persona) ON DELETE CASCADE
);

CREATE TABLE roles_usuarios (
	codigo_rol_usuario varchar(200) PRIMARY KEY,
	codigo_usuario varchar(200),
	codigo_rol varchar(200),
	estado_rol varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_usuario) REFERENCES usuarios (codigo_usuario) ON DELETE CASCADE,
	FOREIGN KEY (codigo_rol) REFERENCES roles (codigo_rol) ON DELETE CASCADE
);

CREATE TABLE medicos (
	codigo_medico varchar(200) PRIMARY KEY,
	impremedico varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_medico) REFERENCES personas (codigo_persona) ON DELETE CASCADE
);

CREATE TABLE especialidades (
	codigo_especialidad varchar(200) PRIMARY KEY,
	nombre_especialidad varchar(25),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE especialidades_medicos (
	codigo_especialidad_medico varchar(200) PRIMARY KEY,
	codigo_especialidad varchar(200),
	codigo_medico varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_medico) REFERENCES medicos (codigo_medico) ON DELETE CASCADE,
	FOREIGN KEY (codigo_especialidad) REFERENCES especialidades (codigo_especialidad) ON DELETE CASCADE
);

CREATE TABLE pacientes (
	codigo_paciente varchar(200) PRIMARY KEY,
	codigo_vivienda varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_vivienda) REFERENCES viviendas (codigo_vivienda) ON DELETE CASCADE
);

CREATE TABLE registros_morbilidades (
	codigo_morbilidad varchar(200) PRIMARY KEY,
	codigo_paciente varchar(200),
	motivo varchar(25),
	fecha_morbilidad date,
	conducta_seguir boolean,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_paciente) REFERENCES pacientes (codigo_paciente) ON DELETE CASCADE
);

CREATE TABLE historiales_medicos (
	codigo_historial varchar(200) PRIMARY KEY,
	fecha_historial date,
	antecedente varchar(25),
	codigo_morbilidad varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_historial) REFERENCES pacientes (codigo_paciente) ON DELETE CASCADE,
	FOREIGN KEY (codigo_morbilidad) REFERENCES registros_morbilidades (codigo_morbilidad) ON DELETE CASCADE
);

CREATE TABLE historias_actuales (
	codigo_hactual varchar(200) PRIMARY KEY,
	codigo_historial varchar(200),
	descripcion varchar(200),
	valoracion_objetiva varchar(200),
	valoracion_subjetiva varchar(200),
	estado_paciente boolean,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_historial) REFERENCES historiales_medicos (codigo_historial) ON DELETE CASCADE
);

CREATE TABLE almacenes (
	codigo_almacen varchar(200) PRIMARY KEY,
	titulo varchar(200) UNIQUE,
	ubicacion TEXT,
	disponible integer,
	disponibilidad_total integer,
	tipo varchar(100),
	ancho varchar(20),
	largo varchar(20),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);


CREATE TABLE medicamentos (
	codigo_medicamento varchar(200) PRIMARY KEY,
	nombre_medicamento varchar(30),
	imagen TEXT,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE lotes (
	codigo_lote varchar(200) PRIMARY KEY,
	fecha_emision date,
	fecha_vencimiento date,
	cantidad_existencia integer,
	codigo_almacen varchar(200),
	codigo_medicamento varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_almacen) REFERENCES almacenes (codigo_almacen) ON DELETE CASCADE,
	FOREIGN KEY (codigo_medicamento) REFERENCES medicamentos (codigo_medicamento) ON DELETE CASCADE
);

CREATE TABLE patologias (
	codigo_patologia varchar(200) PRIMARY KEY,
	nombre_patologia varchar(30),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off')
);

CREATE TABLE historial_patologias (
	codigo_hpatologia varchar(200) PRIMARY KEY,
	codigo_patologia varchar(200),
	codigo_hactual varchar(200),
	tratamiento varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_patologia) REFERENCES patologias (codigo_patologia) ON DELETE CASCADE,
	FOREIGN KEY (codigo_hactual) REFERENCES historias_actuales (codigo_hactual) ON DELETE CASCADE
);

CREATE TABLE solicitudes_medicamentos (
	codigo_solicitud varchar(200) PRIMARY KEY,
	codigo_hactual varchar(200) NULL,
	codigo_morbilidad varchar(200) NULL,
	tratamiento varchar(200),
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_morbilidad) REFERENCES registros_morbilidades (codigo_morbilidad) ON DELETE CASCADE,
	FOREIGN KEY (codigo_hactual) REFERENCES historias_actuales (codigo_hactual) ON DELETE CASCADE
);

CREATE TABLE evoluciones (
	codigo_evolucion varchar(200) PRIMARY KEY,
	codigo_hactual varchar(200),
	datos_objetivos varchar(200),
	datos_sujetivos varchar(200),
	fecha_evolucion date,
	proxima_valoracion date,
	date_creation TIMESTAMP,
	date_update	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	status_delete ENUM('on','off'),
	FOREIGN KEY (codigo_hactual) REFERENCES historias_actuales (codigo_hactual) ON DELETE CASCADE
);

ALTER TABLE pacientes ADD FOREIGN KEY (codigo_paciente) REFERENCES personas (codigo_persona); 

INSERT INTO permisos (codigo_permiso,nombre_permiso,date_creation,status_delete) VALUES 
('est01crear01','crear_estado','2021-08-20 20:00:00','on'),
('est01edita01','editar_estado','2021-08-20 20:00:00','on'),
('est01borra01','borrar_estado','2021-08-20 20:00:00','on'),
('est01ver01','ver_estado','2021-08-20 20:00:00','on'),
('mun02crear02','crear_municipio','2021-08-20 20:00:00','on'),
('mun02editar02','editar_municipio','2021-08-20 20:00:00','on'),
('mun02borrar02','borrar_municipio','2021-08-20 20:00:00','on'),
('mun02ver02','ver_municipio','2021-08-20 20:00:00','on'),
('par03crear03','crear_parroquia','2021-08-20 20:00:00','on'),
('par03editar03','editar_parroquia','2021-08-20 20:00:00','on'),
('par03borrar03','borrar_parroquia','2021-08-20 20:00:00','on'),
('par03ver03','ver_parroquia','2021-08-20 20:00:00','on'),
('com04crear04','crear_comunidad','2021-08-20 20:00:00','on'),
('com04editar04','editar_comunidad','2021-08-20 20:00:00','on'),
('com04borrar04','borrar_comunidad','2021-08-20 20:00:00','on'),
('com04ver04','ver_comunidad','2021-08-20 20:00:00','on'),
('ser05crear05','crear_servicio','2021-08-20 20:00:00','on'),
('ser05editar05','editar_servicio','2021-08-20 20:00:00','on'),
('ser05borrar05','borrar_servicio','2021-08-20 20:00:00','on'),
('ser05ver05','ver_servicio','2021-08-20 20:00:00','on'),
('rol06crear06','crear_rol','2021-08-20 20:00:00','on'),
('rol06editar06','editar_rol','2021-08-20 20:00:00','on'),
('rol06borrar06','borrar_rol','2021-08-20 20:00:00','on'),
('rol06ver06','ver_rol','2021-08-20 20:00:00','on');