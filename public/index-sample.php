<?php 
use Slim\Factory\AppFactory;

require_once __DIR__ .'/sisprodi/vendor/autoload.php';
require_once __DIR__.'/sisprodi/config/loader.php';
require_once __DIR__.'/sisprodi/config/dependencies.php';

$app = AppFactory::create();

// Add Error Handling Middleware
$app->addErrorMiddleware(true, true, true);

require_once __DIR__.'/sisprodi/app/web.php';

$app->run();