<?php 

namespace App\Core;

use App\Models\Rol;

/**
 * manejador de permisos del sistema
 */
class Permission 
{

    public function SetPermisse($permission)
    {
        $_SESSION['user_permise'] = $permission;
    }

    public function returnPermise($permise)
    {
        foreach($_SESSION['user_permise'] as $permiso){ 
            if($permiso['nombre_permiso'] == $permise || $permiso['nombre_permiso'] == 'soy_admin'){        
                return true; 
            }
        }
        return false;
    }

    public function verifyPermise($permise)
    {
        $result = false;
        foreach($_SESSION['user_permise'] as $permiso){
            if($permiso['nombre_permiso'] == $permise || $permiso['nombre_permiso'] == 'soy_admin'){
                $result = true;
            }
        }
        if(!$result){
            header('Location:/no-permise'); 
        }
    }

    public function returnPermiseRol($permise,$idRol=false)
    {
        $roles = new Rol();
        $roles->setTable('roles');
        $listRoles = $roles->findPermise($idRol);

        foreach($listRoles as $permiso){ 
            if($permiso['nombre_permiso'] == $permise || $permiso['nombre_permiso'] == 'soy_admin'){        
                return true; 
            }
        }
        return false;
        
    }


}
