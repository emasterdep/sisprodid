<?php 

namespace App\Core;

class Model
{
    protected $conection;

    protected $query;

    protected $resp;

    protected $table;

    public function __construct()
    {
        $this->conection = \App\Core\conection();
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function setQuery($query)
    {
        $this->query = $query;
    }

    public function save()
    { 
        try 
        {
            $this->resp = $this->conection->prepare($this->query);
            $this->resp->execute();
            return $this->resp;
        } 
        catch (Exception $e) 
        {
            die($e->getMessage());	
        }
    }

    public function get()
    {
        try 
        {
            $this->resp = $this->conection->prepare($this->query);
            $this->resp->execute();

            return $this->resp->fetchAll();
        } 
        catch (Exception $e) 
        {
            die($e->getMessage());	
        }
    }

}