<?php 

namespace App\Models;

use App\Core\Model;

class Estado extends Model
{
    private $codigo_estado;

    Private $nombre_estado;

    //metodos de la clase persona
    public function SetCodigo_estado($codigo_estado)
    {
        $this->codigo_estado = filter_var( str_replace(' ','',$codigo_estado), FILTER_SANITIZE_ENCODED ) ;
    }

    public function SetNombre($name)
    {
        $this->nombre_estado = strtoupper( $name );
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_estado="'.$id.'"';
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_estado  = '".$this->nombre_estado."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_estado='".$id."'"; 
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_estado,nombre_estado,status_delete,date_creation) VALUES
            (
                '".$this->codigo_estado."', 
                '".$this->nombre_estado."', 
                'on',
                '".date("Y-m-d h:m:s")."')";
        $this->save();
    }

    public function select($order='ASC')
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        return $this->get(); 
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_estado='".$id."'";
        return $this->get();
    }
}
