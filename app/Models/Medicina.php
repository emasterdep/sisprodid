<?php 

namespace App\Models;

use App\Core\Model;

class Medicina extends Model
{
    private $codigo_medicamento;

    private $nombre_medicamento;

    private $imagen;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_medicamento = str_replace(' ','',$codigo);
    }

    public function SetTitulo($titulo)
    {
        $this->nombre_medicamento = strtoupper( $titulo );
    }
    
    public function SetImg($url)
    {
        $this->imagen = $url;
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_medicamento="'.$id.'"';
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            imagen              = '".$this->imagen."',
            nombre_medicamento  = '".$this->nombre_medicamento."',
            date_update         = '".date("Y-m-d h:m:s")."'
            WHERE codigo_medicamento='".$id."'"; 
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_medicamento,nombre_medicamento,imagen,status_delete,date_creation) VALUES
            (
                '".$this->codigo_medicamento."', 
                '".$this->nombre_medicamento."', 
                '".$this->imagen."', 
                'on',
                '".date("Y-m-d h:m:s")."')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        return $this->get(); 
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_medicamento='".$id."'";
        return $this->get();
    }

    public function getMedicamentosEntrega($id)
    {
        $this->query = "SELECT * FROM entregas_medicamentos INNER JOIN lotes ON lotes.codigo_lote=entregas_medicamentos.codigo_lote
        INNER JOIN medicamentos ON medicamentos.codigo_medicamento=lotes.codigo_medicamento 
        WHERE entregas_medicamentos.status_delete='on' AND entregas_medicamentos.codigo_entrega='".$id."'";
        return $this->get();
    }
}
