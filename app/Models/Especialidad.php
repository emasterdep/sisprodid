<?php 

namespace App\Models;

use App\Core\Model;

class Especialidad extends Model
{
    private $codigo_especialidad;

    private $nombre_especialidad;

    private $codigo_medico;

    private $codigo_especialidad_medico;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_especialidad = str_replace(' ','',$codigo);
    }

    public function SetNombre($nombre)
    {
        $this->nombre_especialidad = strtoupper($nombre);
    }

    public function SetCodigoMedico($codigo)
    {
        $this->codigo_medico = $codigo;
    }

    public function SetCodigoMedicoEspecialidad($codigo)
    {
        $this->codigo_especialidad_medico = str_replace(' ','',$codigo);
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_especialidad='".$this->nombre_especialidad."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_especialidad='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_especialidad,nombre_especialidad,date_creation,status_delete) VALUES
            (
                '".$this->codigo_especialidad."', 
                '".$this->nombre_especialidad."', 
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        return $this->get(); 
    }

    public function getEspecialidadesLibre($medicoId)
    {
        $this->query = "SELECT * FROM especialidades WHERE especialidades.codigo_especialidad 
            NOT IN (SELECT especialidades_medicos.codigo_especialidad FROM especialidades_medicos 
            WHERE especialidades_medicos.codigo_medico='".$medicoId."') AND especialidades.status_delete='on'";
            return $this->get(); 
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_especialidad='".$codigo."' "; 
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete="off" 
            WHERE codigo_especialidad="'.$id.'"';
    }

    public function medico($id)
    {
        $this->query = "SELECT * FROM especialidades_medicos INNER JOIN especialidades 
            ON especialidades.codigo_especialidad=especialidades_medicos.codigo_especialidad WHERE especialidades.status_delete='on' 
            AND especialidades_medicos.codigo_medico='".$id."' ORDER BY especialidades.date_creation"; 
        return $this->get(); 
    }

    public function createMedicoEspecialidad()
    {
        $this->query = "INSERT INTO especialidades_medicos (codigo_especialidad_medico,codigo_medico,codigo_especialidad,date_creation,status_delete) VALUES
        (
            '".$this->codigo_especialidad_medico."', 
            '".$this->codigo_medico."', 
            '".$this->codigo_especialidad."', 
            '".date("Y-m-d h:m:s")."',
            'on')";
        $this->save();
    }

    public function deleteEspecialidadMedico($id)
    {
        $this->query = "DELETE FROM especialidades_medicos WHERE codigo_especialidad_medico='".$id."'";
        $this->save();
    }

}
