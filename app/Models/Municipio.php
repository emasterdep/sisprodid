<?php 

namespace App\Models;

use App\Core\Model;

class Municipio extends Model
{
    private $codigo_municipio;

    Private $nombre_municipio;

    private $codigo_estado;

    //metodos de la clase persona
    public function SetCodigo_estado($codigo_estado)
    {
        $this->codigo_estado = str_replace(' ','',$codigo_estado);
    }

    public function SetNombre($name)
    {
        $this->nombre_municipio = strtoupper($name);
    }

    public function SetCodigo($code)
    {
        $this->codigo_municipio = str_replace(' ','',$code);
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_municipio='".$this->nombre_municipio."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_municipio='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_municipio,nombre_municipio,codigo_estado,date_creation,status_delete) VALUES
            (
                '".$this->codigo_municipio."', 
                '".$this->nombre_municipio."', 
                '".$this->codigo_estado."', 
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        if($id != false){
            $this->query .= " WHERE codigo_usuario = ".$id;
        } elseif ($email != false ) {
            $this->query .= " WHERE correo = '".$email."'";
        }
        //$this->query .= " ORDER BY ".$order.";";
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_municipio="'.$id.'"';
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_municipio='".$id."'";
        return $this->get();
    }

    public function whereEstado($codigo_estado)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_estado='".$codigo_estado."'";
        return $this->get();
    }
}
