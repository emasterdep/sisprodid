<?php 

namespace App\Models;

use App\Core\Model;

class Almacen extends Model
{
    private $codigo_almacen;

    private $ubicacion;

    private $disponible;

    Private $disponibilidad_total;

    private $tipo;

    private $ancho;

    private $largo;

    private $titulo;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_almacen = str_replace(' ','',$codigo);
    }

    public function SetTitulo($titulo)
    {
        $this->titulo = strtoupper( $titulo );
    }
    
    public function SetUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;
    }

    public function SetTotalDisponible($total)
    {
        $this->disponibilidad_total = $total;
    }

    public function SetDisponible($disponible)
    {
        $this->disponible = $disponible;
    }

    public function SetTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function SetAncho($medida)
    {
        $this->ancho = $medida;
    }

    public function SetLargo($medida)
    {
        $this->largo = $medida;
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_almacen="'.$id.'"';
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            titulo              = '".$this->titulo."',
            ubicacion           = '".$this->ubicacion."',
            disponibilidad_total  = '".$this->disponibilidad_total."',
            disponible          = '".$this->disponible."',
            tipo                = '".$this->tipo."',
            ancho               = '".$this->ancho."',
            largo               = '".$this->largo."',
            date_update         = '".date("Y-m-d h:m:s")."'
            WHERE codigo_almacen='".$id."'"; 
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_almacen,titulo,ubicacion,disponibilidad_total,disponible,tipo,ancho,largo,status_delete,date_creation) VALUES
            (
                '".$this->codigo_almacen."', 
                '".$this->titulo."', 
                '".$this->ubicacion."', 
                '".$this->disponibilidad_total."', 
                '".$this->disponible."', 
                '".$this->tipo."', 
                '".$this->ancho."', 
                '".$this->largo."', 
                'on',
                '".date("Y-m-d h:m:s")."')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        return $this->get(); 
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_almacen='".$id."'";
        return $this->get();
    }
}
