<?php 

namespace App\Models;

use App\Core\Model;
use App\Core\conection;

class vivienda extends Model
{
    private $codigo_vivienda;

    private $codigo_sector;

    private $numero_habitaciones;

    private $tipo;

    private $estructura;

    private $piso;

    private $techo;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_vivienda = str_replace(' ','',$codigo);
    }

    public function SetSector($sector)
    { 
        $this->codigo_sector = $sector;
    }

    public function SetNumeroHabitaciones($numero)
    {
        $this->numero_habitaciones = $numero;
    }

    public function SetTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function SetEstructura($estructura)
    {
        $this->estructura = $estructura;
    }

    public function SetPiso($piso)
    {
        $this->piso = $piso;
    }

    public function SetTecho($techo)
    {
        $this->techo = $techo;
    }

    public function GetCodigo()
    {
        return $this->codigo_vivienda;
    }

    //metodos del crud
    public function delete($id)
    {
        $this->query = 'DELETE * FROM '.$this->table.' where codigo_persona='.$id;
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            codigo_sector       = '".$this->codigo_sector."', 
            numero_habitaciones = '".$this->numero_habitaciones."', 
            tipo                = '".$this->tipo."', 
            estructura          = '".$this->estructura."', 
            piso                = '".$this->piso."', 
            techo               = '".$this->techo."',
            date_update         = '".date("Y-m-d h:m:s")."'
            WHERE codigo_vivienda='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_vivienda,codigo_sector,numero_habitaciones,tipo,estructura,piso,techo,date_creation,status_delete) VALUES
            (
                '".$this->codigo_vivienda."', 
                '".$this->codigo_sector."', 
                '".$this->numero_habitaciones."', 
                '".$this->tipo."', 
                '".$this->estructura."', 
                '".$this->piso."', 
                '".$this->techo."', 
                '".date("Y-m-d h:m:s")."', 
                'on')";
        $this->save();
    }

    public function select($order='ASC',$id=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ORDER BY ".$order;

        return $this->get();
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE codigo_persona ='".$codigo."' ORDER BY date_creation";
        return $this->get();
    }

    public function findCedula($cedula)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE cedula='".$cedula."'"; 
        return $this->get();
    }
}
