<?php 

namespace App\Models;

use App\Core\Model;
use App\Core\conection;

class Persona extends Model
{
    private $codigo_persona;

    private $cedula;

    private $nombres;

    private $apellidos;

    private $sexo;

    private $fecha_nacimiento;

    private $telefono;

    //metodos de la clase persona
    public function SetCodigo_persona($codigo)
    {
        $this->codigo_persona = str_replace(' ','',$codigo);
    }

    public function SetCedula($cedula)
    { 
        $this->cedula = $cedula;
    }

    public function SetNombres($nombres)
    {
        $this->nombres = strtoupper($nombres);
    }

    public function SetApellidos($apellidos)
    {
        $this->apellidos = strtoupper($apellidos);
    }

    public function SetSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    public function SetFecha_nacimiento($fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    public function SetTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    //metodos getters
    public function getNombres()
    {
        return $this->nombres;
    }

    public function getCedula()
    {
        return $this->cedula;
    }

    public function getCodigo_persona()
    {
        return $this->codigo_persona;
    }

    //metodos del crud
    public function delete($id)
    {
        $this->query = 'DELETE * FROM '.$this->table.' where codigo_persona='.$id;
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombres             = '".$this->nombres."', 
            apellidos           = '".$this->apellidos."', 
            sexo                = '".$this->sexo."', 
            fecha_nacimiento    = '".$this->fecha_nacimiento."', 
            telefono            = '".$this->telefono."',
            date_update         = '".date("Y-m-d h:m:s")."'
            WHERE codigo_persona ='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_persona,cedula,nombres,apellidos,sexo,fecha_nacimiento,telefono,date_creation,status_delete) VALUES
            (
                '".$this->codigo_persona."', 
                '".$this->cedula."', 
                '".$this->nombres."', 
                '".$this->apellidos."', 
                '".$this->sexo."', 
                '".$this->fecha_nacimiento."', 
                '".$this->telefono."', 
                '".date("Y-m-d h:m:s")."', 
                'on')";
        $this->save();
    }

    public function select($order='ASC')
    {
        $this->query = "SELECT * FROM ".$this->table."WHERE status_delete='on' ORDER BY ".$order;
        return $this->get();
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE codigo_persona='".$codigo."' ORDER BY date_creation";
        return $this->get();
    }

    public function findCedula($cedula)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE cedula='".$cedula."'"; 
        return $this->get();
    }

    public function noPacientes()
    {
        $this->query = "SELECT * FROM personas WHERE personas.codigo_persona 
        NOT IN (SELECT pacientes.codigo_paciente FROM pacientes 
        WHERE pacientes.codigo_paciente=personas.codigo_persona) AND status_delete='on'";
        return $this->get();
    }
}
