<?php 

namespace App\Models;

use App\Core\Model;

class Servicio extends Model
{
    private $codigo_servicio;

    private $nombre_servicio;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_servicio = str_replace(' ','',$codigo);
    }

    public function SetNombre($nombre)
    {
        $this->nombre_servicio = strtoupper($nombre);
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_servicio='".$this->nombre_servicio."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_servicio='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_servicio,nombre_servicio,date_creation,status_delete) VALUES
            (
                '".$this->codigo_servicio."', 
                '".$this->nombre_servicio."', 
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        return $this->get(); 
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_servicio='".$codigo."' "; 
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete="off" 
            WHERE codigo_servicio="'.$id.'"';
    }

}
