<?php 

namespace App\Models;

use App\Core\Model;
use App\Core\conection;

class Paciente extends Model
{
    private $codigo_paciente;

    private $codigo_vivienda;

    private $tipo_diabetes;

    private $ocupacion;

    private $glucometro;

    private $fumador;

    private $observacion;

    private $codigo_entrega;

    private $status_falla;

    private $actualizacion_falla;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_paciente = str_replace(' ','',$codigo);
    }

    public function SetCodigoVivienda($codigo)
    { 
        $this->codigo_vivienda = $codigo;
    }

    public function SetDiabete($tipo)
    {
        $this->tipo_diabetes = $tipo; 
    }

    public function SetOcupacion($ocupacion)
    {
        $this->ocupacion = $ocupacion;
    }

    public function SetGlucometro($valor)
    {
        $this->glucometro = $valor;
    }

    public function SetFumador($valor)
    {
        $this->fumador = $valor;
    }

    /**
     * Entrega medicamentos 
     * SECCION
     */
    public function SetObservacion($valor)
    {
        $this->observacion = $valor;
    }

    public function SetCodeEntrega($valor)
    {
        $this->codigo_entrega = $valor;
    }

    public function SetStatusFalla($status)
    {
        $this->status_falla = $status;
    }

    public function SetFallaUpdate($correcion)
    {
        $this->actualizacion_falla = $correcion;
    }

    public function GetCodeEntrega()
    {
        return $this->codigo_entrega;
    }

    //metodos del crud
    public function delete($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            status_delete ='off' 
            WHERE codigo_paciente='".$id."'"; 
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            tipo_diabetes   = '".$this->tipo_diabetes."', 
            ocupacion       = '".$this->ocupacion."', 
            glucometro      = '".$this->glucometro."', 
            fumador         = '".$this->fumador."', 
            date_update     = '".date("Y-m-d h:m:s")."'
            WHERE codigo_paciente='".$id."'"; 
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_paciente,codigo_vivienda,tipo_diabetes,ocupacion,glucometro,fumador,date_creation,status_delete) VALUES
            (
                '".$this->codigo_paciente."', 
                '".$this->codigo_vivienda."', 
                '".$this->tipo_diabetes."', 
                '".$this->ocupacion."', 
                '".$this->glucometro."', 
                '".$this->fumador."', 
                '".date("Y-m-d h:m:s")."', 
                'on')";
        $this->save();
    }

    public function select()
    {
        $this->query = "SELECT * FROM pacientes 
            INNER JOIN personas on personas.codigo_persona=pacientes.codigo_paciente 
            INNER JOIN viviendas on viviendas.codigo_vivienda=pacientes.codigo_vivienda 
            INNER JOIN sectores on sectores.codigo_sector=viviendas.codigo_sector
            INNER JOIN comunidades on comunidades.codigo_comunidad=sectores.codigo_comunidad
            INNER JOIN parroquias on parroquias.codigo_parroquia=comunidades.codigo_parroquia
            INNER JOIN municipios on municipios.codigo_municipio=parroquias.codigo_municipio
            INNER JOIN estados on estados.codigo_estado=municipios.codigo_estado
            WHERE pacientes.status_delete='on' ORDER BY pacientes.date_creation DESC";
        return $this->get();
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM pacientes 
            INNER JOIN personas on personas.codigo_persona=pacientes.codigo_paciente 
            INNER JOIN viviendas on viviendas.codigo_vivienda=pacientes.codigo_vivienda 
            INNER JOIN sectores on sectores.codigo_sector=viviendas.codigo_sector
            INNER JOIN comunidades on comunidades.codigo_comunidad=sectores.codigo_comunidad
            INNER JOIN parroquias on parroquias.codigo_parroquia=comunidades.codigo_parroquia
            INNER JOIN municipios on municipios.codigo_municipio=parroquias.codigo_municipio
            INNER JOIN estados on estados.codigo_estado=municipios.codigo_estado
            WHERE pacientes.status_delete='on' AND pacientes.codigo_paciente='".$codigo."' ORDER BY pacientes.date_creation ";
        return $this->get();
    }

    public function findCedula($cedula)
    {
        $this->query = "SELECT * FROM personas WHERE cedula='".$cedula."' AND status_delete='on'";
        return $this->get();
    }

    /**
     * @return array
     * retorna un array con los pacientes y sus tipos
     */
    public function convertNumberTipoPaciente($datos)
    {
        $tipo1 = 0;
        $tipo2 = 0;
        $tipo3 = 0;

        foreach($datos as $paciente){
            if($paciente['tipo_diabetes'] == '1'){
                $tipo1++;
            }
            if($paciente['tipo_diabetes'] == '2'){
                $tipo2++;
            }
            if($paciente['tipo_diabetes'] == '3'){
                $tipo3++;
            }
        }
        return ['tipo1'=>$tipo1,'tipo2'=>$tipo2,'tipo3'=>$tipo3,'todos'=>count($datos)];
    }

    public function entregarMedicamento()
    {
        $this->query = "INSERT INTO entregas (codigo_entrega,codigo_paciente,observacion,status_falla,date_creation,status_delete) VALUES
            (
                '".$this->codigo_entrega."', 
                '".$this->codigo_paciente."', 
                '".$this->observacion."',
                '".$this->status_falla."',
                '".date("Y-m-d h:m:s")."', 
                'on')";
        $this->save(); 
    }

    public function asignarMedicamentoEntrega($codeMain, $codeEntrega, $codeLote, $cantidad)
    {
        $this->query = "INSERT INTO entregas_medicamentos (codigo_entrega_medicamento,codigo_entrega,codigo_lote,cantidad_entregada,date_creation,status_delete) VALUES
            (
                '".$codeMain."', 
                '".$codeEntrega."', 
                '".$codeLote."',
                '".$cantidad."',
                '".date("Y-m-d h:m:s")."', 
                'on')";
        $this->save();
    }

    public function getEntregasMedicinas()
    {
        $this->query = "SELECT * FROM entregas 
        INNER JOIN pacientes ON pacientes.codigo_paciente=entregas.codigo_paciente 
        INNER JOIN entregas_medicamentos ON entregas_medicamentos.codigo_entrega=entregas.codigo_entrega 
        INNER JOIN personas ON personas.codigo_persona=pacientes.codigo_paciente 
        INNER JOIN lotes ON lotes.codigo_lote=entregas_medicamentos.codigo_lote 
        INNER JOIN medicamentos ON medicamentos.codigo_medicamento=lotes.codigo_medicamento 
        WHERE entregas.codigo_entrega 
        IN (SELECT entregas_medicamentos.codigo_entrega FROM entregas_medicamentos) AND pacientes.status_delete='on'
        GROUP BY entregas.codigo_entrega";
        return $this->get();
    }

    public function getEntregasPaciente($id)
    {
        $this->query = "SELECT * FROM entregas 
        INNER JOIN pacientes ON pacientes.codigo_paciente=entregas.codigo_paciente
        WHERE pacientes.codigo_paciente='".$id."' AND pacientes.status_delete='on'";
        return $this->get();
    }

    public function updateFallaEntrega($codigo)
    {
        $this->query = "UPDATE entregas  SET 
            actualizacion_falla     = '".$this->actualizacion_falla."', 
            status_falla            = '".$this->status_falla."', 
            date_update     = '".date("Y-m-d h:m:s")."'
            WHERE codigo_entrega='".$codigo."'"; echo $this->query;
        $this->save();
    }
}
