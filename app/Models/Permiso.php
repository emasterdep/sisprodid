<?php 

namespace App\Models;

use App\Core\Model;

class Permiso extends Model
{
    private $codigo_permiso;

    private $nombre_permiso;

    private $codigo_permiso_rol;

    private $estado_permiso;

    private $codigo_rol;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_permiso_rol = str_replace(' ','',$codigo);
    }

    public function SetNombre($nombre)
    {
        $this->nombre_rol = strtoupper($nombre);
    }

    public function SetCodigoRol($code)
    {
        $this->codigo_rol = strtoupper($code);
    }

    public function SetCodigoPermiso($code)
    {
        $this->codigo_permiso = strtoupper($code);
    }

    public function SetEstatus($status)
    {
        $this->estado_permiso = $status;
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_rol='".$this->nombre_rol."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_rol='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_permiso_rol,codigo_permiso,codigo_rol,estado_permiso,date_creation) VALUES
            (
                '".$this->codigo_permiso_rol."', 
                '".$this->codigo_permiso."', 
                '".$this->codigo_rol."',
                '".$this->estado_permiso."', 
                '".date("Y-m-d h:m:s")."')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND NOT nombre_permiso='soy_admin'";
        return $this->get(); 
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE codigo_permiso_rol='".$codigo."' "; 
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'DELETE FROM '.$this->table.' WHERE codigo_permiso_rol="'.$id.'"';
    }

    public function deletePermises($idRol)
    {
        $this->query = 'DELETE FROM permisos_roles
            WHERE codigo_rol="'.$idRol.'"';
    }

}
