<?php 

namespace App\Models;

use App\Core\Model;

class Rol extends Model
{
    private $codigo_rol;

    private $nombre_rol;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_rol = str_replace(' ','',$codigo);
    }

    public function SetNombre($nombre)
    {
        $this->nombre_rol = strtoupper($nombre);
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_rol='".$this->nombre_rol."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_rol='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_rol,nombre_rol,date_creation,status_delete) VALUES
            (
                '".$this->codigo_rol."', 
                '".$this->nombre_rol."', 
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND NOT nombre_rol='Administrador' ";
        return $this->get(); 
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_rol='".$codigo."' "; 
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete="off" 
            WHERE codigo_rol="'.$id.'"';
    }

    public function findPermise($codigo)
    {
        $this->query = "SELECT * FROM roles 
        INNER JOIN permisos_roles on permisos_roles.codigo_rol=roles.codigo_rol
        INNER JOIN permisos on permisos.codigo_permiso=permisos_roles.codigo_permiso
        WHERE roles.status_delete='on' AND roles.codigo_rol='".$codigo."'"; 
        return $this->get(); 
    }

}
