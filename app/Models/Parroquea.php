<?php 

namespace App\Models;

use App\Core\Model;

class Parroquea extends Model
{
    private $codigo_parroquia;

    Private $nombre_parroquia;

    private $codigo_municipio;

    //metodos de la clase persona
    public function SetCodigo_municipio($codigo_estado)
    {
        $this->codigo_municipio = str_replace(' ','',$codigo_estado);
    }

    public function SetNombre($name)
    {
        $this->nombre_parroquia = strtoupper($name);
    }

    public function SetCodigo($code)
    {
        $this->codigo_parroquia = $code;
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_parroquia='".$this->nombre_parroquia."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_parroquia='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_parroquia,nombre_parroquia,codigo_municipio,date_creation,status_delete) VALUES
            (
                '".str_replace(' ','',$this->codigo_parroquia)."', 
                '".$this->nombre_parroquia."', 
                '".$this->codigo_municipio."', 
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        if($id != false){
            $this->query .= " WHERE codigo_usuario = ".$id;
        } elseif ($email != false ) {
            $this->query .= " WHERE correo = '".$email."'";
        }
        //$this->query .= " ORDER BY ".$order.";";
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_parroquia="'.$id.'"';
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_parroquia='".$id."'";
        return $this->get();
    }

    public function whereMunicipio($codigo_estado)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_municipio='".$codigo_estado."'";
        return $this->get();
    }
}
