<?php 

namespace App\Models;

use App\Core\Model;

class Medico extends Model
{
    private $codigo_medico;

    private $impremedico;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_medico = str_replace(' ','',$codigo);
    }

    public function SetImpremedico($name)
    {
        $this->impremedico = strtoupper($name);
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            impremedico='".$this->impremedico."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_medico='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_medico,impremedico,date_creation,status_delete) VALUES
            (
                '".$this->codigo_medico."', 
                '".$this->impremedico."', 
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." INNER JOIN personas ON personas.codigo_persona=".$this->table.".codigo_medico WHERE medicos.status_delete='on' AND personas.status_delete='on' ORDER BY personas.date_creation";
        return $this->get(); 
    }

    public function find($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." INNER JOIN personas ON personas.codigo_persona=".$this->table.".codigo_medico WHERE ".$this->table.".status_delete='on' AND codigo_medico='".$codigo."' "; 
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete="off" 
            WHERE codigo_medico="'.$id.'"';
    }

}
