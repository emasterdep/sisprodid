<?php 

namespace App\Models;

use App\Core\Model;

class Usuario extends Model
{
    private $codigo_usuario;

    Private $correo;

    private $password;

    private $username;

    /**
     * para asignar roles a un usuario
     */
    Private $codigo_rol_usuario;

    private $codigo_rol;

    private $estado_rol;

    public function SetCodeRolUser($code)
    {
        $this->codigo_rol_usuario = str_replace(' ','',$code);
    }

    public function SetCodeRol($codeRol)
    {
        $this->codigo_rol = $codeRol;
    }

    public function SetEstado($status)
    {
        $this->estado_rol = $status;
    }

    //metodos de la clase persona
    public function SetCodigo_usuario($codigo_usuario)
    {
        $this->codigo_usuario = str_replace(' ','',$codigo_usuario);
    }

    public function SetCorreo($correo)
    {
        $this->correo = $correo;
    }

    public function SetPassword($password)
    {
        $this->password = $password;
    }

    public function SetUsername($username)
    {
        $this->username = $username;
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete="off" 
            WHERE codigo_usuario="'.$id.'"';
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            correo              = '".$this->correo."', 
            contraseña          = '".$this->password."', 
            username            = '".$this->username."', 
            date_update         = '".date("Y-m-d h:m:s")."' 
            WHERE codigo_usuario= '".$id."'";
        $this->save();
    }

    public function updateRol($id)
    {
        $this->query = "UPDATE roles_usuarios SET 
            codigo_rol  = '".$this->codigo_rol."',
            estado_rol  = '".$this->estado_rol."'
            WHERE codigo_usuario ='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_usuario,correo,contraseña,username,date_creation,status_delete) VALUES
            (
                '".$this->codigo_usuario."', 
                '".$this->correo."', 
                '".$this->password."', 
                '".$this->username."',
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC')
    {
        $this->query = "SELECT * FROM usuarios 
        INNER JOIN personas on personas.codigo_persona=usuarios.codigo_usuario 
        INNER JOIN roles_usuarios on roles_usuarios.codigo_usuario=usuarios.codigo_usuario 
        INNER JOIN roles on roles.codigo_rol=roles_usuarios.codigo_rol 
        WHERE usuarios.status_delete='on'";
        return $this->get();
    }

    public function login($password,$username)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND correo='".$username."'"; 
        $datos = $this->get();
        if (count($datos) > 0) {
            if ( password_verify($password,$datos[0]['contraseña'] ) ) {
                return ['mnsj'=>'..esta logueado','status'=>true,'user'=>$datos];
            } else {
                return ['mnsj'=>'Usuario o contraseña incorrectos','status'=>false];
            }
            
        } else {
            return ['mnsj'=>'Usuario o contraseña incorrectos','status'=>false];
        }
        
    }

    public function asignRol()
    {
        $this->query = "INSERT INTO roles_usuarios (codigo_rol_usuario,codigo_usuario,codigo_rol,estado_rol,date_creation,status_delete) VALUES
            (
                '".$this->codigo_rol_usuario."', 
                '".$this->codigo_usuario."', 
                '".$this->codigo_rol."', 
                '".$this->estado_rol."',
                '".date("Y-m-d h:m:s")."',
                'on')"; 
        $this->save();
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM usuarios 
        INNER JOIN personas on personas.codigo_persona=usuarios.codigo_usuario 
        INNER JOIN roles_usuarios on roles_usuarios.codigo_usuario=usuarios.codigo_usuario 
        INNER JOIN roles on roles.codigo_rol=roles_usuarios.codigo_rol 
        WHERE usuarios.status_delete='on' AND usuarios.codigo_usuario='".$id."'";
        return $this->get();
    }

    public function getPermission($email)
    {
        $this->query = "SELECT * FROM usuarios 
        INNER JOIN personas on personas.codigo_persona=usuarios.codigo_usuario 
        INNER JOIN roles_usuarios on roles_usuarios.codigo_usuario=usuarios.codigo_usuario 
        INNER JOIN roles on roles.codigo_rol=roles_usuarios.codigo_rol 
        INNER JOIN permisos_roles on permisos_roles.codigo_rol=roles.codigo_rol
        INNER JOIN permisos on permisos.codigo_permiso=permisos_roles.codigo_permiso
        WHERE usuarios.status_delete='on' AND usuarios.correo='".$email."'";
        return $this->get();
    }

}
