<?php 

namespace App\Models;

use App\Core\Model;

class Historia extends Model
{
    private $codigo_historia_actual;

    Private $codigo_historia;

    //clave primaria de la tabla historias_clinicas
    private $primary_historia;

    Private $creacion_historia;

    Private $hora_creacion;

    Private $contacto_emergencia;

    Private $telefono_contacto_emergencia;

    Private $motivo_admision;

    Private $enfermedad_actual;

    Private $diagnostico_adminsion;

    Private $condicion_salida;

    Private $diagnostico_final;

    private $estatus_historia;

    //metodos de la clase persona
    public function SetCodigoHistoriaActual($codigo)
    {
        $this->codigo_historia_actual = str_replace(' ','',$codigo);
    }

    public function SetcodigoHistoria($codigo)
    {
        $this->codigo_historia = $codigo;
    }

    public function SetPrimaryHistoria($code)
    {
        $this->primary_historia = $code;
    }

    public function SetEstatus($status)
    {
        $this->estatus_historia = $status;
    }

    public function SetDate($date)
    {
        $this->creacion_historia = $date;
    }

    public function SetHora($hour)
    {
        $this->hora_creacion = $hour;
    }

    public function SetcontactoNombre($nombre)
    {
        $this->contacto_emergencia = $nombre;
    }

    public function SetTelefono($phone)
    {
        $this->telefono_contacto_emergencia = $phone;
    }

    public function SetMotivoAdmision($text)
    {
        $this->motivo_admision = $text;
    }

    public function SetEnfermedad($text)
    {
        $this->enfermedad_actual = $text;
    }

    public function SetDiagnostico($text)
    {
        $this->diagnostico_adminsion = $text;
    }

    public function SetCondisionSalida($tipe)
    {
        $this->condicion_salida = $tipe;
    }

    public function SetDiagnosticoFinal($text)
    {
        $this->diagnostico_final = $text;
    }

    public function getCodigoHistoria()
    {
        return $this->primary_historia;
    }

    public function statusHistorial($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            estatus_historia  = '".$this->estatus_historia."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_historia_actual='".$id."'"; 
        $this->save();
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_estado="'.$id.'"';
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_estado  = '".$this->nombre_estado."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_estado='".$id."'"; 
        $this->save();
    }

    public function createHistoria()
    {
        $this->query = "INSERT INTO historias_clinicas 
            (codigo_historia,fecha_historia,hora_creacion,status_delete,date_creation) VALUES
            (
                '".$this->primary_historia."', 
                '".$this->creacion_historia."', 
                '".$this->hora_creacion."',  
                'on',
                '".date("Y-m-d h:m:s")."')"; echo $this->query;
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO historias_actuales
            (codigo_historia_actual,codigo_historia,creacion_historia,hora_creacion,contacto_emergencia,telefono_contacto_emergencia,motivo_admision,enfermedad_actual,diagnostico_adminsion,status_delete,date_creation,estatus_historia) VALUES
            (
                '".$this->codigo_historia_actual."', 
                '".$this->codigo_historia."', 
                '".$this->creacion_historia."', 
                '".$this->hora_creacion."', 
                '".$this->contacto_emergencia."', 
                '".$this->telefono_contacto_emergencia."', 
                '".$this->motivo_admision."', 
                '".$this->enfermedad_actual."', 
                '".$this->diagnostico_adminsion."', 
                'on',
                '".date("Y-m-d h:m:s")."',
                'on')";
        $this->save();
    }

    public function select($order='ASC')
    {
        $this->query = "SELECT * FROM historias_clinicas 
            INNER JOIN pacientes ON pacientes.codigo_paciente=historias_clinicas.codigo_historia";
        return $this->get(); 
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM historias_clinicas 
            INNER JOIN pacientes ON pacientes.codigo_paciente=historias_clinicas.codigo_historia 
            INNER JOIN historias_actuales ON historias_actuales.codigo_historia=historias_clinicas.codigo_historia
            WHERE pacientes.codigo_paciente='".$id."' AND pacientes.status_delete='on'";
        return $this->get();
    }

    /**
     * @return array 
     * se usa cuando se quiere saber si existe la historia
     */
    public function findHistoria($id)
    {
        $this->query = "SELECT * FROM historias_clinicas 
            INNER JOIN pacientes ON pacientes.codigo_paciente=historias_clinicas.codigo_historia
            WHERE pacientes.codigo_paciente='".$id."' AND pacientes.status_delete='on'";
        return $this->get();
    }

    /**
     * @return array 
     * se usa cuando se quiere saber si existe la historia
     */
    public function findHActual($id)
    {
        $this->query = "SELECT * FROM historias_clinicas 
            INNER JOIN pacientes ON pacientes.codigo_paciente=historias_clinicas.codigo_historia
            INNER JOIN historias_actuales ON historias_actuales.codigo_historia=historias_clinicas.codigo_historia
            WHERE historias_actuales.codigo_historia_actual='".$id."' AND pacientes.status_delete='on'";
        return $this->get();
    }

    public function registerEvolution($codeMain,$codeHistoria,$hallazgo,$dataObjetive,$sistomas,$complicaciones,$impresion,$tratamiento,$resultadoTra)
    {
        $this->query = "INSERT INTO evoluciones 
            (codigo_evolucion,codigo_historia_actual,estado_hallazgo,dato_objetivo,nuevos_sintomas,complicaciones,cambio_impresion,tratamiento_seguido,resultado_tratamiento,status_delete,date_creation) VALUES
            (
                '".$codeMain."', 
                '".$codeHistoria."', 
                '".$hallazgo."', 
                '".$dataObjetive."', 
                '".$sistomas."', 
                '".$complicaciones."', 
                '".$impresion."', 
                '".$tratamiento."', 
                '".$resultadoTra."', 
                'on',
                '".date("Y-m-d h:m:s")."')";
        $this->save();
    }

    public function updateEvolucion($codeMain,$codeHistoria,$hallazgo,$dataObjetive,$sistomas,$complicaciones,$impresion,$tratamiento,$resultadoTra)
    {
        $this->query = "UPDATE evoluciones SET 
            codigo_historia_actual  = '".$codeHistoria."', 
            estado_hallazgo         = '".$hallazgo."', 
            dato_objetivo           = '".$dataObjetive."', 
            nuevos_sintomas         = '".$sistomas."', 
            complicaciones          = '".$complicaciones."', 
            cambio_impresion        = '".$impresion."', 
            tratamiento_seguido     = '".$tratamiento."', 
            resultado_tratamiento   = '".$resultadoTra."', 
            date_update             = '".date("Y-m-d h:m:s")."'
            WHERE codigo_evolucion='".$codeMain."'"; echo $this->query;
        $this->save();
    }

    public function getEvoluciones($code)
    {
        $this->query = "SELECT * FROM evoluciones 
            WHERE codigo_historia_actual='".$code."' AND status_delete='on' ORDER BY date_creation";
        return $this->get();
    }

    public function deleteEvolucion($codeEvo)
    {
        $this->query = "UPDATE evoluciones SET 
            status_delete  = 'off',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_evolucion='".$codeEvo."'"; 
        $this->save();
    }

    public function findEvolucion($code)
    {
        $this->query = "SELECT * FROM evoluciones 
            WHERE codigo_evolucion='".$code."' AND status_delete='on'";
        return $this->get();
    }

    public function finalizarHistoria($codigo)
    {
        $this->query = "UPDATE historias_actuales SET 
            condicion_salida  = '".$this->condicion_salida."',
            diagnostico_final  = '".$this->diagnostico_final."',
            estatus_historia  = '".$this->estatus_historia."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_historia_actual='".$codigo."'"; 
        $this->save();
    }

    public function registerExamen($codigoMain,$codigo_historia_actual,$nombre,$descripcion,$datoObj)
    {
        $this->query = "INSERT INTO examenes_funcionales 
            (codigo_examen,codigo_historia_actual,nombre,descripcion,dato_objetivo,status_delete,date_creation) VALUES
            (
                '".$codigoMain."', 
                '".$codigo_historia_actual."', 
                '".$nombre."',  
                '".$descripcion."',  
                '".$datoObj."',  
                'on',
                '".date("Y-m-d h:m:s")."')";
        $this->save();
    }

    public function findExamenes($code)
    {
        $this->query = "SELECT * FROM examenes_funcionales 
            WHERE codigo_historia_actual='".$code."' AND status_delete='on' ORDER BY date_creation";
        return $this->get();
    }

    public function deleteExamen($code)
    {
        $this->query = "UPDATE examenes_funcionales SET 
            status_delete  = 'off',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_examen='".$code."'"; 
        $this->save();
    }

    public function findExamen($code)
    {
        $this->query = "SELECT * FROM examenes_funcionales 
        WHERE codigo_examen='".$code."' AND status_delete='on'";
        return $this->get();
    }

    public function updateExamen($codigoMain,$nombre,$descripcion,$datoObj)
    {
        $this->query = "UPDATE examenes_funcionales SET
            nombre          = '".$nombre."',
            descripcion     = '".$descripcion."',
            dato_objetivo   = '".$datoObj."', 
            date_update     = '".date("Y-m-d h:m:s")."' 
            WHERE codigo_examen='".$codigoMain."'"; echo $this->query;
        $this->save();
    }

    public function updateHActual($idHistoria)
    {
        $this->query = "UPDATE historias_actuales SET
            hora_creacion               = '".$this->hora_creacion."',
            contacto_emergencia         = '".$this->contacto_emergencia."',
            telefono_contacto_emergencia          = '".$this->telefono_contacto_emergencia."',
            motivo_admision             = '".$this->motivo_admision."',
            enfermedad_actual           = '".$this->enfermedad_actual."',
            diagnostico_adminsion       = '".$this->diagnostico_adminsion."',
            creacion_historia          = '".$this->creacion_historia."',
            date_update                 = '".date("Y-m-d h:m:s")."' 
            WHERE codigo_historia_actual='".$idHistoria."'"; echo $this->query;
        $this->save();
    }
}
