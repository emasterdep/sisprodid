<?php 

namespace App\Models;

use App\Core\Model;

class Comunidad extends Model
{
    private $codigo_comunidad;

    Private $nombre_comunidad;

    private $codigo_parroquia;

    //metodos de la clase persona
    public function SetCodigo_parroquia($codigo_estado)
    {
        $this->codigo_parroquia = str_replace(' ','',$codigo_estado);
    }

    public function SetNombre($name)
    {
        $this->nombre_comunidad = strtoupper($name);
    }

    public function SetCodigo($code)
    {
        $this->codigo_comunidad = $code;
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_comunidad='".$this->nombre_comunidad."',
            date_update    = '".date("Y-m-d h:m:s")."'
            WHERE codigo_comunidad='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_comunidad,nombre_comunidad,codigo_parroquia,date_creation,status_delete) VALUES
            (
                '".str_replace(' ','',$this->codigo_comunidad)."', 
                '".$this->nombre_comunidad."', 
                '".$this->codigo_parroquia."', 
                '".date("Y-m-d h:m:s")."',
                'on')"; 
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        if($id != false){
            $this->query .= " WHERE codigo_usuario = ".$id;
        } elseif ($email != false ) {
            $this->query .= " WHERE correo = '".$email."'";
        }
        //$this->query .= " ORDER BY ".$order.";";
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_comunidad="'.$id.'"';
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_comunidad='".$id."'";
        return $this->get();
    }

    public function whereParroquia($codigo_estado)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_parroquia='".$codigo_estado."'";
        return $this->get();
    }
}
