<?php 

namespace App\Models;

use App\Core\Model;

class Lote extends Model
{
    private $codigo_lote;

    private $fecha_emision;

    private $fecha_vencimiento;

    private $cantidad_existencia;

    private $codigo_almacen;

    private $presentacion;

    private $codigo_medicamento;

    //metodos de la clase persona
    public function SetCodigo($codigo)
    {
        $this->codigo_lote = str_replace(' ','',$codigo);
    }

    public function SetEmision($date)
    {
        $this->fecha_emision = $date;
    }

    public function SetVencimiento($date)
    {
        $this->fecha_vencimiento = $date;
    }
    
    public function SetCantidad($number)
    {
        $this->cantidad_existencia = $number;
    }

    public function SetMedicamento($code)
    {
        $this->codigo_medicamento = $code;
    }

    public function SetAlmacen($code)
    {
        $this->codigo_almacen = $code;
    }

    public function SetPresentacion($text)
    {
        $this->presentacion = $text;
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_lote="'.$id.'"';
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            fecha_emision       = '".$this->fecha_emision."',
            fecha_vencimiento   = '".$this->fecha_vencimiento."',
            cantidad_existencia = '".$this->cantidad_existencia."',
            codigo_almacen      = '".$this->codigo_almacen."',
            codigo_medicamento  = '".$this->codigo_medicamento."',
            presentacion        = '".$this->presentacion."',
            date_update         = '".date("Y-m-d h:m:s")."'
            WHERE codigo_lote='".$id."'"; 
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_lote,fecha_emision,fecha_vencimiento,cantidad_existencia,codigo_almacen,codigo_medicamento,presentacion,status_delete,date_creation) VALUES
            (
                '".$this->codigo_lote."', 
                '".$this->fecha_emision."', 
                '".$this->fecha_vencimiento."', 
                '".$this->cantidad_existencia."', 
                '".$this->codigo_almacen."', 
                '".$this->codigo_medicamento."', 
                '".$this->presentacion."', 
                'on',
                '".date("Y-m-d h:m:s")."')";
        $this->save();
    }

    public function select($order='ASC',$id=false,$email=false)
    {
        $this->query = "SELECT * FROM medicamentos INNER JOIN lotes on 
        lotes.codigo_medicamento=medicamentos.codigo_medicamento 
        INNER JOIN almacenes on almacenes.codigo_almacen=lotes.codigo_almacen 
        WHERE lotes.status_delete='on' ORDER BY lotes.date_creation DESC";
        return $this->get(); 
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE codigo_lote='".$id."' AND status_delete='on' ";
        return $this->get();
    }

    public function inExistencia()
    {
        $this->query = "SELECT * FROM medicamentos INNER JOIN lotes on 
        lotes.codigo_medicamento=medicamentos.codigo_medicamento 
        INNER JOIN almacenes on almacenes.codigo_almacen=lotes.codigo_almacen 
        WHERE lotes.status_delete='on' AND lotes.cantidad_existencia>0 ORDER BY lotes.date_creation DESC";
        return $this->get(); 
    }

    public function updateExistenciaLote($id,$NuevaCantidad)
    {
        $this->query = "UPDATE ".$this->table." SET 
            cantidad_existencia = '".$NuevaCantidad."',
            date_update         = '".date("Y-m-d h:m:s")."'
            WHERE codigo_lote='".$id."'"; 
        $this->save();
    }
}
