<?php 

namespace App\Models;

use App\Core\Model;

class Sector extends Model
{
    private $codigo_sector;

    Private $nombre_sector;

    private $codigo_comunidad;

    //metodos de la clase persona
    public function SetCodigo_comunidad($codigo)
    {
        $this->codigo_comunidad = str_replace(' ','',$codigo);
    }

    public function SetNombre($name)
    {
        $this->nombre_sector = strtoupper($name);
    }

    public function SetCodigo($code)
    {
        $this->codigo_sector = str_replace(' ','',$code);
    }

    public function update($id)
    {
        $this->query = "UPDATE ".$this->table." SET 
            nombre_sector   = '".$this->nombre_sector."',
            date_update     = '".date("Y-m-d h:m:s")."'
            WHERE codigo_sector='".$id."'";
        $this->save();
    }

    public function insert()
    {
        $this->query = "INSERT INTO ".$this->table." (codigo_sector,nombre_sector,codigo_comunidad,date_creation,status_delete) VALUES
            (
                '".$this->codigo_sector."', 
                '".$this->nombre_sector."', 
                '".$this->codigo_comunidad."', 
                '".date("Y-m-d h:m:s")."',
                'on')"; 
        $this->save();
    }

    public function select($order='ASC')
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' ";
        return $this->get(); 
    }

    public function delete($id)
    {
        $this->query = 'UPDATE '.$this->table.' SET 
            status_delete = "off" 
            WHERE codigo_sector="'.$id.'"';
    }

    public function find($id)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_sector='".$id."'";
        return $this->get();
    }

    public function whereComunidad($codigo)
    {
        $this->query = "SELECT * FROM ".$this->table." WHERE status_delete='on' AND codigo_comunidad='".$codigo."'"; 
        return $this->get();
    }
}
