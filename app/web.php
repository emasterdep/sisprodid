<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

//sistema de autenticación
$app->post('/login','App\Controllers\UserController:login');
$app->get('/logout','App\Controllers\UserController:logout');
$app->get('/','App\Controllers\UserController:view')->setName('index');

$app->get('/dashboard','App\Controllers\UserController:dashboard')->setName('dashboard');

$app->get('/ajustes','App\Controllers\UserController:ajustes')->setName('ajustes');
/**
 * @author Edward Romero
 * @since 02-08-2021
 * secciones para acciones del sistema
 */
$app->get('/estados','App\Controllers\LocalidadController:view')->setName('estadosList');
$app->get('/crear-estado','App\Controllers\LocalidadController:show')->setName('createState');

$app->get('/estado/delete','App\Controllers\LocalidadController:deleteStado');
$app->get('/estado/edit','App\Controllers\LocalidadController:storeStado');
$app->post('/created-state','App\Controllers\LocalidadController:storageState');
$app->post('/estado/update','App\Controllers\LocalidadController:updateStado');

/**
 * acciones para |cipios
 */
$app->get('/municipios','App\Controllers\MunicipioController:view');
$app->get('/municipios/create','App\Controllers\MunicipioController:show');
$app->get('/municipios/edit','App\Controllers\MunicipioController:storeMunicipio');

$app->post('/creation-municipio','App\Controllers\MunicipioController:storageState');
$app->post('/municipios/update','App\Controllers\MunicipioController:updateMunicipio');
$app->get('/municipios/delete','App\Controllers\MunicipioController:deleteMunicipio');

$app->get('/municipios/find-estado','App\Controllers\MunicipioController:filterByEstado');

/**
 * acciones para parroqueas
 */
$app->get('/parroqueas','App\Controllers\ParroqueaController:view');
$app->get('/parroqueas/create','App\Controllers\ParroqueaController:show');
$app->get('/parroqueas/edit','App\Controllers\ParroqueaController:storeParroquia');

$app->post('/creation-parroquea','App\Controllers\ParroqueaController:storageParroquea');
$app->get('/parroqueas/delete','App\Controllers\ParroqueaController:deleteParroquia');
$app->post('/parroqueas/update','App\Controllers\ParroqueaController:updateParroquia');

$app->get('/parroqueas/find-municipio','App\Controllers\ParroqueaController:findByMunicipio');
/**
 * acciones para comunidades
 */
$app->get('/comunidades','App\Controllers\ComunidadController:view');
$app->get('/comunidades/create','App\Controllers\ComunidadController:show');
$app->get('/comunidades/edit','App\Controllers\ComunidadController:storeComunidad');

$app->post('/creation-comunidad','App\Controllers\ComunidadController:storageComunidad');
$app->get('/comunidades/delete','App\Controllers\ComunidadController:deleteParroquia');
$app->post('/comunidades/update','App\Controllers\ComunidadController:updateComunidad');

$app->get('/comunidades/filter-by-parroquea','App\Controllers\ComunidadController:filterByParroquea');
/**
 * acciones para sectores
 */
$app->get('/sectores','App\Controllers\SectorController:view');
$app->get('/sectores/create','App\Controllers\SectorController:show');
$app->get('/sectores/edit','App\Controllers\SectorController:storeComunidad');

$app->post('/sectores/save','App\Controllers\SectorController:saveSector');
$app->get('/sectores/delete','App\Controllers\SectorController:delete');
$app->post('/sectores/update','App\Controllers\SectorController:updateSector');

$app->get('/sectores/filter-by-comunidad','App\Controllers\SectorController:filterByComunidad');
/**
 * acciones para viviendas
 */

 //servicios
$app->get('/viviendas/service','App\Controllers\ServicioController:view');
$app->post('/viviendas/service/create','App\Controllers\ServicioController:create');
$app->get('/viviendas/service/delete','App\Controllers\ServicioController:delete');
$app->get('/viviendas/service/edit','App\Controllers\ServicioController:show');
$app->post('/viviendas/service/update','App\Controllers\ServicioController:update');

/**
 * acciones para roles de usuario
 */
$app->get('/user/roles','App\Controllers\RolController:view');
$app->post('/user/roles/create','App\Controllers\RolController:create');
$app->get('/user/roles/delete','App\Controllers\RolController:delete');
$app->get('/user/roles/edit','App\Controllers\RolController:show');
$app->post('/user/roles/update','App\Controllers\RolController:update');
$app->get('/user/rol','App\Controllers\RolController:showAsign');
$app->get('/user/rol/asign','App\Controllers\RolController:AsignData');

/**
 * acciones para gestion de usuarios
 */
$app->get('/users','App\Controllers\UserController:listUser');
$app->get('/user/create','App\Controllers\UserController:show');
$app->post('/user/save','App\Controllers\UserController:create');
$app->get('/user/delete','App\Controllers\UserController:delete');
$app->get('/user/edit','App\Controllers\UserController:editView');
$app->post('/user/update','App\Controllers\UserController:updateUser');

$app->get('/no-permise','App\Controllers\UserController:noPermise');
/**
 * acciones para roles de especialidades medicas
 */
$app->get('/medicos/especialidades','App\Controllers\EspecialidadController:view');
$app->post('/medicos/especialidades/create','App\Controllers\EspecialidadController:create');
$app->get('/medicos/especialidades/delete','App\Controllers\EspecialidadController:delete');
$app->get('/medicos/especialidades/edit','App\Controllers\EspecialidadController:show');
$app->post('/medicos/especialidades/update','App\Controllers\EspecialidadController:update');

/**
 * acciones para medicos
 */
$app->get('/medicos','App\Controllers\MedicoController:view');
$app->get('/medicos/create','App\Controllers\MedicoController:create');
$app->post('/medicos/save','App\Controllers\MedicoController:saveMedico');
$app->get('/medicos/delete','App\Controllers\MedicoController:delete');
$app->get('/medicos/edit','App\Controllers\MedicoController:show');
$app->post('/medicos/update','App\Controllers\MedicoController:update');
$app->get('/medicos/{medico}','App\Controllers\MedicoController:detailsMedico');
$app->post('/medicos/especialidad/asignar','App\Controllers\MedicoController:createEspecilidadMedico');
$app->get('/medicos/especialidad/deleteMedico','App\Controllers\MedicoController:deleteEspecialidadMedico');

$app->get('/medicos/download/descargarxlxs','App\Controllers\MedicoController:downloadListMedicos');
/**
 * acciones para almacenes - seccion almacenes
 */
$app->get('/medicamentos/almacenes','App\Controllers\AlmacenController:view');
$app->get('/medicamentos/almacenes/create','App\Controllers\AlmacenController:create');
$app->post('/medicamentos/almacenes/save','App\Controllers\AlmacenController:saveAlmacen');
$app->get('/medicamentos/almacenes/delete','App\Controllers\AlmacenController:delete');
$app->get('/medicamentos/almacenes/edit','App\Controllers\AlmacenController:show');
$app->post('/medicamentos/almacenes/update','App\Controllers\AlmacenController:update');

$app->get('/medicamentos/entregas/downloadXlxs','App\Controllers\MedicinaController:downloadEntregaXlxs');
/**
 * acciones para almacenes - seccion almacenes
 */
$app->get('/medicamentos','App\Controllers\MedicinaController:view');
$app->get('/medicamentos/create','App\Controllers\MedicinaController:create');
$app->post('/medicamentos/save','App\Controllers\MedicinaController:saveMedicamento');
$app->get('/medicamentos/delete','App\Controllers\MedicinaController:delete');
$app->get('/medicamentos/edit','App\Controllers\MedicinaController:show');
$app->post('/medicamentos/update','App\Controllers\MedicinaController:update');

$app->get('/medicamentos/entrega','App\Controllers\MedicinaController:entregar');
$app->post('/medicamentos/entrega/edit','App\Controllers\MedicinaController:updateFallaEntrega');
$app->post('/medicamentos/entregar-paciente','App\Controllers\MedicinaController:asignarMedicamento');
/**
 * acciones lotes de medicamentos
 */
$app->get('/medicamentos/lotes','App\Controllers\LoteController:view');
$app->get('/medicamentos/lotes/create','App\Controllers\LoteController:create');
$app->get('/medicamentos/lotes/descargarxlxs','App\Controllers\LoteController:downloadLotesXlxs');
$app->post('/medicamentos/lotes/save','App\Controllers\LoteController:saveLote');
$app->get('/medicamentos/lotes/delete','App\Controllers\LoteController:delete');
$app->get('/medicamentos/lotes/edit','App\Controllers\LoteController:show');
$app->post('/medicamentos/lotes/update','App\Controllers\LoteController:update');

/**
 * acciones de pacientes
 */
$app->get('/pacientes','App\Controllers\PacienteController:view');
$app->get('/pacientes/create','App\Controllers\PacienteController:showCreateOne');
$app->post('/pacientes/savePersona','App\Controllers\PacienteController:SavePersonaAndStepNext');
$app->get('/pacientes/select-vivienda','App\Controllers\PacienteController:selectViviendaForPaciente');
$app->get('/pacientes/delete','App\Controllers\PacienteController:deletePaciente');
$app->get('/pacientes/edit-data','App\Controllers\PacienteController:showEditPaciente');
$app->post('/pacientes/editPersona','App\Controllers\PacienteController:editDataPersonaPaciente');
$app->post('/pacientes/update-vivienda','App\Controllers\PacienteController:saveViviendaEditPaciente');
$app->post('/pacientes/register-vivienda','App\Controllers\PacienteController:saveViviendaToPaciente');

$app->get('/pacientes/find-persona','App\Controllers\PacienteController:validarPaciente');
$app->get('/pacientes/find-json-paciente','App\Controllers\PacienteController:findPacienteData');
$app->get('/pacientes/select-persona-create','App\Controllers\PacienteController:selectPersonaNowNextStep');
$app->get('/pacientes/detalle-historia/{historia}','App\Controllers\PacienteController:showDetailsHistoria');

$app->get('/pacientes/detalles/downloadXlxs','App\Controllers\PacienteController:downloadPDFhistoryPaciente');
$app->get('/diseno-pdf','App\Controllers\PacienteController:disenoHtmlforPaciente');

//entrega medicamento
$app->get('/pacientes/entrega-medicamento','App\Controllers\PacienteController:viewEntregaMedicamento');
$app->get('/pacientes/entrega/seleccionar-paciente','App\Controllers\PacienteController:selectPacienteEntrega');
$app->get('/pacientes/entrega/historial','App\Controllers\PacienteController:viewHistoryPaciente');

$app->get('/pacientes/descargarxlxs','App\Controllers\PacienteController:downloadExcelPacientes');
$app->post('/pacientes/search','App\Controllers\PacienteController:searchPaciente');
$app->get('/pacientes/{paciente}','App\Controllers\PacienteController:viewProfilePaciente');

/**
 * acciones de historias paciente
 */
$app->get('/historia-medica/paciente/{paciente}','App\Controllers\HistoriaController:view');
$app->get('/historia-medica/editar/{historia}','App\Controllers\HistoriaController:showEditHistoriaActual');
$app->post('/historia-medica/actual/save','App\Controllers\HistoriaController:saveHistorial');
$app->post('/historia-medica/actual/update','App\Controllers\HistoriaController:updateHistoriaActual');

$app->get('/historias/save-evolucion','App\Controllers\HistoriaController:saveEvaluacionPaciente');
$app->get('/historias/return-evolution','App\Controllers\HistoriaController:returnEvolucionesPacientes');
$app->get('/historias/delete-evolution','App\Controllers\HistoriaController:deleteEvolucion');
$app->get('/historias/find-evolution','App\Controllers\HistoriaController:findEvolucion');
$app->get('/historias/update-evolution','App\Controllers\HistoriaController:editEvaluacion');
$app->post('/historias/finalizar-historia','App\Controllers\HistoriaController:finalizarHistoriaActual');

//crud de examen funcionales
$app->get('/historias/save-examen-funcional','App\Controllers\HistoriaController:createExamenFuncional');
$app->get('/historias/return-examenes','App\Controllers\HistoriaController:returnListExamen');
$app->get('/historias/delete-examenes','App\Controllers\HistoriaController:deleteExamenFuncional');
$app->get('/historias/edit-examenes','App\Controllers\HistoriaController:editExamenFuncional');
$app->get('/historias/find-examenes','App\Controllers\HistoriaController:findExamenFuncional');

//$app->get('/init-system','App\Controllers\UserController:initSystem'); //para crear el sistema borrar una vez se haya iniciado
