<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Models\Medicina;
use App\Models\Almacen;
use App\Models\Lote;

class LoteController extends BaseController
{
    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $lote = new Lote();
        $lote->setTable('lotes');
        $ListadoLote = $lote->select();
        echo $this->view->render('pages/Medicina/lotes/list',['loteMedicamneto'=>$ListadoLote]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        //obteniendo los medicamentos
        $medicinas = new Medicina();
        $medicinas->setTable('medicamentos');
        $ListaMedicinas = $medicinas->select();
        //obteniendo los almacenes
        $almacenes = new Almacen();
        $almacenes->setTable('almacenes');
        $listaAlmacenes = $almacenes->select();
        //vista
        echo $this->view->render('pages/Medicina/lotes/create',['medicinas'=>$ListaMedicinas,'almacenes'=>$listaAlmacenes]);
        return $response;
    }

    public function saveLote(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        
        $lotes = new Lote();
        $lotes->setTable('lotes');
        $lotes->SetEmision($paramts['date_emision']);
        $lotes->SetVencimiento( $paramts['date_vencimiento'] );
        $lotes->SetCantidad( $paramts['cantidad'] );
        $lotes->SetPresentacion( $paramts['presentacion'] );
        $lotes->SetMedicamento( $paramts['medicamento_id'] );
        $lotes->SetAlmacen( $paramts['almacen_id'] );
        $lotes->SetCodigo(substr( $paramts['medicamento_id'],0,4).date('s').$paramts['almacen_id'].time()*date('s'));
        $lotes->insert();
        return $response->withHeader('Location', '/medicamentos/lotes?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $lote = new Lote();
        $lote->setTable('lotes');
        $lote->delete( $_GET['lote'] );
        $lote->save();
        return $response->withHeader('Location', '/medicamentos/lotes?action=success');
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        //obteniendo medicamentos
        $medicinas = new Medicina();
        $medicinas->setTable('medicamentos');
        $ListaMedicinas = $medicinas->select();
        //obteniendo los almacenes
        $almacenes = new Almacen();
        $almacenes->setTable('almacenes');
        $listaAlmacenes = $almacenes->select();
        //lote exacto
        $lote = new Lote();
        $lote->setTable('lotes');
        $listadoLote = $lote->find($_GET['lote']);
        //editar lote
        echo $this->view->render('pages/medicina/lotes/edit',['medicinas'=>$ListaMedicinas,'almacenes'=>$listaAlmacenes,'lote'=>$listadoLote[0]]);
        return $response;
    }

    public function update(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $lote = new Lote();
        $lote->setTable('lotes');
        $lote->SetEmision( $paramts['date_emision']);
        $lote->SetVencimiento( $paramts['date_vencimiento'] );
        $lote->SetCantidad( $paramts['cantidad'] );
        $lote->SetMedicamento( $paramts['medicamento_id'] );
        $lote->SetPresentacion( $paramts['presentacion'] );
        $lote->SetAlmacen( $paramts['almacen_id'] );
        $lote->update($paramts['id_lote']);

        return $response->withHeader('Location', '/medicamentos/lotes?action=success');
    }

    public function downloadLotesXlxs(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $lote = new Lote();
        $lote->setTable('lotes');
        $ListadoLote = $lote->select();

        $documento = new Spreadsheet();

        $hoja = $documento->getActiveSheet();
        $hoja->setTitle("Lista Medicos");
        $titleSheet = ['N°','Medicamento','Almacen','Cantidad','Presentación','Fecha de emisión','Fecha de caducación'];
        $hoja->fromArray($titleSheet,null,'A1');

        $indexListPacientes = 0;
        for($row=1; $row <= count($ListadoLote); $row++){
            for($column = 1; $column <= count($titleSheet); $column++){
                switch($column){
                    case 1: $hoja->setCellValueByColumnAndRow($column, $row+1, $row);
                    break;
                    case 2: $hoja->setCellValueByColumnAndRow($column, $row+1, $ListadoLote[$indexListPacientes]['nombre_medicamento']);
                    break;
                    case 3: $hoja->setCellValueByColumnAndRow($column, $row+1, $ListadoLote[$indexListPacientes]['ubicacion']);
                    break;
                    case 4: $hoja->setCellValueByColumnAndRow($column, $row+1, $ListadoLote[$indexListPacientes]['cantidad_existencia']);
                    break;
                    case 5: $hoja->setCellValueByColumnAndRow($column, $row+1, $ListadoLote[$indexListPacientes]['presentacion']);
                    break;
                    case 6: $hoja->setCellValueByColumnAndRow($column, $row+1, $ListadoLote[$indexListPacientes]['fecha_emision']);
                    break;
                    case 7: $hoja->setCellValueByColumnAndRow($column, $row+1, $ListadoLote[$indexListPacientes]['fecha_vencimiento']);
                    break;
                }
            }
            $indexListPacientes++;
        }

        $nombreDelDocumento = "listado-medicos-".date("Y-m-d-h:m:s").".xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
        header('Cache-Control: max-age=0');
        
        $writer = IOFactory::createWriter($documento, 'Xlsx');
        $writer->setOffice2003Compatibility(true);
        $writer->save('php://output');

        return $response;
    }
}