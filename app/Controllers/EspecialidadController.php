<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Especialidad;

class EspecialidadController extends BaseController
{
    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $especialidad = new Especialidad();
        $especialidad->setTable('especialidades');
        $listEspecialidades = $especialidad->select();
        echo $this->view->render('pages/Medico/especialidad/list',['especialidades'=>$listEspecialidades]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $especialidad = new Especialidad();
        $especialidad->setTable('especialidades');
        $especialidad->SetCodigo(substr( $paramts['especialidadName'],0,4).date('s').$paramts['especialidadName'].time()*date('s'));
        $especialidad->SetNombre($paramts['especialidadName']);
        $especialidad->insert();
        return $response->withHeader('Location', '/medicos/especialidades?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        $especialidad = new Especialidad();
        $especialidad->setTable('especialidades');
        $especialidad->delete($_GET['especialidad']);
        $especialidad->save();
        return $response->withHeader('Location', '/medicos/especialidades?action=success');
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $especialidad = new Especialidad();
        $especialidad->setTable('especialidades');
        $selectEspecialidad = $especialidad->find($_GET['especialidad']);
        echo $this->view->render('pages/Medico/especialidad/edit',['especialidad'=>$selectEspecialidad[0]]);
        return $response;
    }

    public function update(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $especialidad = new Especialidad();
        $especialidad->setTable('especialidades');
        $especialidad->SetNombre($paramts['nameEspecialidad']);
        $especialidad->update($paramts['idEspecialidad']);
        return $response->withHeader('Location', '/medicos/especialidades?action=success');
    }
}