<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Models\Medicina;
use App\Models\Paciente;
use App\Models\Lote;

class MedicinaController extends BaseController
{   
    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $medicinas = new Medicina();
        $medicinas->setTable('medicamentos');
        $ListaMedicinas = $medicinas->select();
        echo $this->view->render('pages/Medicina/list',['medicinas'=>$ListaMedicinas]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/Medicina/create');
        return $response;
    }

    public function saveMedicamento(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        
        $resultWriteImg = $this->validateImgMedicamento($_FILES['imgMedicamento']);
        if( $resultWriteImg['status'] ){
            //escribir los valores para la imagen de la medicina
            $medicamento = new Medicina();
            $medicamento->setTable('medicamentos');
            $medicamento->SetTitulo($paramts['tituloMedicamento']);
            $medicamento->SetImg( $resultWriteImg['value'] );
            $medicamento->SetCodigo(substr( $paramts['tituloMedicamento'],0,4).date('s').$paramts['tituloMedicamento'].time()*date('s'));
            $medicamento->insert();
            return $response->withHeader('Location', '/medicamentos?action=success');
        }else{
            return $response->withHeader('Location', '/medicamentos/create?action='.$resultWriteImg['value']);
        }
    }

    /**
     * @var img será el $_FILE['nombre_name'] que evaluará dentro del método 
     * para devolver la dirección de la carpeta y colocar el archivo allí dentro
     */
    public function validateImgMedicamento($img)
    {
        if($img['type'] == 'image/jpeg' || $img['type'] == 'image/png' || $img['type'] == 'image/jpg'){
            if($img['size'] <= 5242880){
                $file_tmp = $img['tmp_name'];
                $file_name = $img['name'];
                $nameSaveDatabase = 'upload/'.date('y')."/".date('m')."/";
                $directoryFile = $_SERVER['DOCUMENT_ROOT'].'/'.$nameSaveDatabase;
                $nameFull = $directoryFile.$file_name;
                if(!file_exists($directoryFile)){
                    mkdir($directoryFile,0777,true);
                    move_uploaded_file($file_tmp,$nameFull);
                }else{
                    move_uploaded_file($file_tmp,$nameFull);
                }
                $newName = time()*date('s').date('s').str_replace(' ','',$file_name);
                rename($nameFull,$directoryFile.$newName);
                return ['status'=>true,'value'=>$nameSaveDatabase.$newName];
            }else{
                return ['status'=>false,'value'=>'imageflag'];
            }
            
        }else{
            return ['status'=>false,'value'=>'noisimage'];
        }
    }

    public function delete(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $medicamento = new Medicina();
        $medicamento->setTable('medicamentos');
        $medicamento->delete( $_GET['medicamento'] );
        $medicamento->save();
        return $response->withHeader('Location', '/medicamentos?action=success');
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $medicamento = new Medicina();
        $medicamento->setTable('medicamentos');
        $selectMedina = $medicamento->find($_GET['medicamento']);
        echo $this->view->render('pages/medicina/edit',['medicina'=>$selectMedina[0]]);
        return $response;
    }

    public function update(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $almacen = new Medicina();
        $almacen->setTable('medicamentos');
        if( $_FILES['imgMedicamento']['name'] == '' ){
            $almacen->SetImg($paramts['imgMedicamentoAntigua']);
        }else{
            $resultWriteImg = $this->validateImgMedicamento($_FILES['imgMedicamento']);
            $almacen->SetImg($resultWriteImg['value']);
        }
        $almacen->SetTitulo($paramts['tituloMedicamento']);
        $almacen->update($paramts['id_medicina']);

        return $response->withHeader('Location', '/medicamentos?action=success');
    }

    public function entregar(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('paciente_entregamedicamento');

        $lote = new Lote();
        $lote->setTable('lotes');
        $ListadoLote = $lote->inExistencia();

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $pacientePersona = $paciente->findCedula($_GET['cedula']);
        if( isset($pacientePersona[0]['codigo_persona']) && $pacientePersona[0]['codigo_persona'] !== null ){
            $datosPaciente = $paciente->find($pacientePersona[0]['codigo_persona']);
            echo $this->view->render('pages/medicina/entregas/selectMedicina',['paciente'=>$datosPaciente[0],'lotes'=>$ListadoLote]);
        }else{
            echo $this->view->render('pages/error',['menssage'=>'Cedula no esta registrada en el sistema','ayudaUsuario'=>'Registre al paciente para poder entregar medicamentos']);
        }

        return $response;
    }

    public function asignarMedicamento(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('paciente_entregamedicamento');

        $medicamentos = json_decode($_POST['medicinas']);

        $paciente = new Paciente();
        $paciente->SetCodigo($_POST['paciente']);
        $paciente->SetObservacion($_POST['observacion']);
        $paciente->SetCodeEntrega( substr($_POST['paciente'],0,4).date('s').date('ydmhs').$_POST['paciente'].time()*date('s').rand(0, 200)  );
        $paciente->SetStatusFalla('no');
        $paciente->entregarMedicamento();

        foreach($medicamentos as $medicina){
            $paciente->asignarMedicamentoEntrega(
                substr( $_POST['paciente'],0,4).date('s').date('ydmhs').$medicina->codigo.$medicina->cantidad.rand(0, 200).time()*date('s'),
                $paciente->GetCodeEntrega(),
                $medicina->codigo,
                $medicina->cantidad
            );
            $this->restarExistencia($medicina->codigo,$medicina->cantidad);
        }
        return $response;
    }

    public function restarExistencia($codigoLote,$cantidadEntregada)
    {
        $lote = new Lote();
        $lote->setTable('lotes');
        $resultado = $lote->find($codigoLote);
        $cantidadNueva = ($resultado[0]['cantidad_existencia']-$cantidadEntregada);

        $lote->updateExistenciaLote($codigoLote,$cantidadNueva);
    }

    public function updateFallaEntrega(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('paciente_entregamedicamento');

        $paciente = new Paciente();
        $paciente->SetFallaUpdate($_POST['descripcionFallaCorrecion']);
        $paciente->SetStatusFalla('si');
        $paciente->updateFallaEntrega($_POST['codigoEntregaMedicamento']); 


        return $response->withHeader('Location', '/pacientes/entrega/historial?codigo_paciente='.$_POST['pacienteCode']);
    }

    /**
     * @return document XLXS
     * descarga un archivo xlxs de las entregas de medicamentos
     */
    public function downloadEntregaXlxs(Request $request, Response $response, $args)
    {
        sessionValidate('auth');

        $paciente = new Paciente();
        $listaEntrega = $paciente->getEntregasMedicinas();

        $medicina = new Medicina();

        $documento = new Spreadsheet();

        $hoja = $documento->getActiveSheet();
        $hoja->setTitle("Entregas de medicamentos");
        $titleSheet = ['N°','Nombre','Apellido','Cédula','Medicamentos entregados','Fecha de entrega'];
        $hoja->fromArray($titleSheet,null,'A1');
        
        $indexList = 0;
        for($row=1; $row <= count($listaEntrega); $row++){
            for($column = 1; $column <= count($titleSheet); $column++){
                switch($column){
                    case 1: $hoja->setCellValueByColumnAndRow($column, $row+1, $row);
                    break;
                    case 2: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaEntrega[$indexList]['nombres']);
                    break;
                    case 3: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaEntrega[$indexList]['apellidos']);
                    break;
                    case 4: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaEntrega[$indexList]['cedula']);
                    break;
                    case 5: 
                        $medicinasEntrega = getMedicamentos($listaEntrega[$indexList]['codigo_entrega']);
                        $listMedicinasThisEntrega = '';
                        foreach($medicinasEntrega as $medicina){
                            $listMedicinasThisEntrega .= $medicina["nombre_medicamento"].' (Presentación: '.$medicina["presentacion"].') - cantidad: '.$medicina["cantidad_entregada"].' ,';
                        }
                        $hoja->setCellValueByColumnAndRow($column, $row+1, $listMedicinasThisEntrega);
                    break;
                    case 6: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaEntrega[$indexList]['date_creation']);
                    break;
                }
            }
            $indexList++;
        }
        
        $nombreDelDocumento = "listado-medicos-".date("Y-m-d-h:m:s").".xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
        header('Cache-Control: max-age=0');
        
        $writer = IOFactory::createWriter($documento, 'Xlsx');
        $writer->setOffice2003Compatibility(true);
        $writer->save('php://output');

        return $response;
    }
}