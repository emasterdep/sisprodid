<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Servicio;

class ServicioController extends BaseController
{
    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $servicio = new Servicio();
        $servicio->setTable('servicios');
        $listServiciosVivienda = $servicio->select();
        echo $this->view->render('pages/Vivienda/servicio/list',['servicios'=>$listServiciosVivienda]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $servicio = new Servicio();
        $servicio->setTable('servicios');
        $servicio->SetCodigo(substr( $paramts['servicename'],0,4).date('s').$paramts['servicename'].time()*date('s'));
        $servicio->SetNombre($paramts['servicename']);
        $servicio->insert();
        return $response->withHeader('Location', '/viviendas/service?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        $servicio = new Servicio();
        $servicio->setTable('servicios');
        $servicio->delete($_GET['servicio']);
        $servicio->save();
        return $response->withHeader('Location', '/viviendas/service?action=success');
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $servicio = new Servicio();
        $servicio->setTable('servicios');
        $selectServicio = $servicio->find($_GET['servicio']);
        echo $this->view->render('pages/Vivienda/servicio/edit',['servicio'=>$selectServicio[0]]);
        return $response;
    }

    public function update(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $servicio = new Servicio();
        $servicio->setTable('servicios');
        $servicio->SetNombre($paramts['nameService']);
        $servicio->update($paramts['idService']);
        return $response->withHeader('Location', '/viviendas/service?action=success');
    }
}