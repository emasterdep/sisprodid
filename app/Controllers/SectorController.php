<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Parroquea;
use App\Models\Comunidad;
use App\Models\Sector;

class SectorController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $estados = new Estado();
        $estados->setTable('estados');
        $estados = $estados->select();
        if( isset($_GET['filterEstado']) ){ 
            //listado de municipios del filtro
            $municipio = new Municipio();
            $municipio->setTable('municipios');
            $listaMunicipio = $municipio->whereEstado( $_GET['filterEstado'] );

            //listado de parroqueas del municipio
            $parroquea = new Parroquea();
            $parroquea->setTable('parroquias');
            $listaParroqueas = ( isset($_GET['filterMunicipio']) ) ? $parroquea->whereMunicipio( $_GET['filterMunicipio'] ) : null;

            //listado de comunidades de la parroquia
            $comunidades = new Comunidad();
            $comunidades->setTable('comunidades');
            $listComunidades = ( isset($_GET['filterParroquea']) ) ? $comunidades->whereParroquia($_GET['filterParroquea']) : null;

            //comunidad seleccionada
            $SelectComunidad = ( isset($_GET['comunidadSelect']) ) ? $comunidades->find($_GET['comunidadSelect']) : null;

            //listado de sectores
            $sectores = new Sector();
            $sectores->setTable('sectores');
            $listadoSectores = ( isset($_GET['comunidadSelect']) ) ? $sectores->whereComunidad( $_GET['comunidadSelect'] ) : null;

            echo $this->view->render('pages/localidad/sector/list',
                ['estados'          =>$estados,
                'municipios'        =>$listaMunicipio,
                'parroqueas'        =>$listaParroqueas,
                'comunidades'       =>$listComunidades,
                'sectores'          =>$listadoSectores,
                'comunidadSelect'   =>$SelectComunidad[0]
            ]);
        }else{
            echo $this->view->render('pages/localidad/sector/list',['estados'=>$estados]);
        }
        return $response;
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/localidad/sector/create');
        return $response;
    }

    public function saveSector(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $sector = new Sector(); 
        $sector->setTable('sectores');
        $sector->SetNombre( $paramts['nameSector'] );
        $sector->SetCodigo_comunidad( $paramts['comunidadId']);
        $sector->SetCodigo(substr( $paramts['nameSector'],0,4).date('s').$paramts['nameSector'].time()*date('s'));
        $sector->insert();

        return $response->withHeader('Location', '/sectores?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        $sector = new Sector();
        $sector->setTable('sectores');
        $sector->delete($_GET['sector']);
        $sector->save();
        return $response->withHeader('Location', '/sectores?action=success');
    }

    public function storeComunidad(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $sector = new Sector();
        $sector->setTable('sectores');
        $result = $sector->find($_GET['sector']);
        if( count($result) > 0 ){
            echo $this->view->render('pages/localidad/sector/edit',['sector'=>$result[0]]);
        } else{
           return $response->withHeader('Location', '/sectores?unabledata=fail');
        }
        return $response;
    }

    public function updateSector(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        $sector = new Sector();
        $sector->setTable('sectores');
        $sector->SetNombre( $paramts['nameSector']);
        $sector->update($paramts['idSector']);
        return $response->withHeader('Location', '/sectores?action=success');
    }

    public function filterByComunidad(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $sectores = new Sector();
        $sectores->setTable('sectores');
        $listadoSectores = $sectores->whereComunidad( $_GET['comunidad'] );

        echo json_encode($listadoSectores);
        return $response;
    }

}