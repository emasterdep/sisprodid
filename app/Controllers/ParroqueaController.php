<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Parroquea;

class ParroqueaController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $estados = new Estado();
        $estados->setTable('estados');
        $estados = $estados->select();
        if( isset($_GET['filterMunicipio']) ){ 
            
            $municipio = new Municipio();
            $municipio->setTable('municipios');
            $listMunicipios = $municipio->select();
            $selectMunicipio = ( isset($_GET['filterMunicipio']) ) ? $municipio->find($_GET['filterMunicipio']) : null ;
            //listado de parroqueas del municipio
            $parroquea = new Parroquea();
            $parroquea->setTable('parroquias');
            $listaParroqueas = ( isset($_GET['filterMunicipio']) ) ? $parroquea->whereMunicipio( $_GET['filterMunicipio'] ) : null;

            echo $this->view->render('pages/localidad/parroquea/list',
                ['estados'          =>$estados,
                'selectMunicipio'   =>$selectMunicipio[0],
                'municipios'        =>$listMunicipios,
                'parroqueas'        =>$listaParroqueas
            ]);
        }else{
            echo $this->view->render('pages/localidad/parroquea/list',['estados'=>$estados]);
        }
        return $response;
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/localidad/parroquea/create');
        return $response;
    }

    public function storageParroquea(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        sessionValidate('auth');
        $parroquea = new Parroquea();
        $parroquea->setTable('parroquias');
        $parroquea->SetNombre( $paramts['nameParroquea']);
        $parroquea->SetCodigo_municipio( $paramts['municipieCode']);
        $parroquea->SetCodigo(substr( $paramts['nameParroquea'],0,4).date('s').$paramts['nameParroquea'].time()*date('s'));
        $parroquea->insert();
        return $response->withHeader('Location', '/parroqueas?action=success');
    }

    public function deleteParroquia(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $parroquea = new Parroquea();
        $parroquea->setTable('parroquias');
        $parroquea->delete($_GET['parroquia']);
        $parroquea->save();
        return $response->withHeader('Location', '/parroqueas?action=success');
    }

    public function storeParroquia(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $Parroquea = new Parroquea();
        $Parroquea->setTable('parroquias');
        $result = $Parroquea->find($_GET['parroquia']);
        if( count($result) > 0 ){
            echo $this->view->render('pages/localidad/parroquea/edit',['parroquia'=>$result[0]]);
        } else{
            return $response->withHeader('Location', '/parroqueas?unabledata=fail');
        }
        return $response;
    }

    public function updateParroquia(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        $municipio = new Parroquea();
        $municipio->setTable('parroquias');
        $municipio->SetNombre( $paramts['nameParroquea']);
        $municipio->SetCodigo_municipio( $paramts['codeMunicipio']);
        $municipio->update($paramts['idParroquea']);
        return $response->withHeader('Location', '/parroqueas?action=success');
    }

    public function findByMunicipio(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $parroquea = new Parroquea();
        $parroquea->setTable('parroquias');
        $listaParroqueas = $parroquea->whereMunicipio( $_GET['municipio'] );

        echo json_encode($listaParroqueas);
        return $response;
    }

}