<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Core\Sesion;
use App\Models\Estado;

class LocalidadController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {
        $login = new Sesion();
        $login->sessionStart();
        if( $login->verifySession('auth') ){
            $estados = new Estado();
            $estados->setTable('estados');
            $estados = $estados->select();
            echo $this->view->render('pages/localidad/estados',['estados'=>$estados]);
        } else{
            return $response->withHeader('Location', '/?permise=error');
        }
        return $response;
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/localidad/createStado');
        return $response;
    }

    public function storageState(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $estado = new Estado();
        $estado->setTable('estados');
        $estado->SetNombre($paramts['nameState']);
        $estado->SetCodigo_estado( substr($paramts['nameState'],0,4).date('s').$paramts['nameState'].time()*date('s') );
        $estado->insert();
        return $response->withHeader('Location', '/estados?action=success');
    }

    public function deleteStado(Request $request, Response $response, $args)
    {
        $estado = new Estado();
        $estado->setTable('estados');
        $estado->delete($_GET['estado']);
        $estado->save();
        return $response->withHeader('Location', '/estados?action=success');
    }

    public function storeStado(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $estado = new Estado();
        $estado->setTable('estados');
        $result = $estado->find($_GET['estado']);
        if( count($result) > 0 ){
            echo $this->view->render('pages/localidad/update',['estado'=>$result]);
        } else{
            return $response->withHeader('Location', '/estados?unabledata=fail');
        }
        return $response;
    }

    public function updateStado(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $estado = new Estado();
        $estado->setTable('estados');
        $estado->SetNombre($paramts['nameState']);
        $estado->update($paramts['idState']);
        return $response->withHeader('Location', '/estados?action=success');
    }

}