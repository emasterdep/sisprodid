<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Core\Sesion;
use App\Models\Estado;
use App\Models\Municipio;

class MunicipioController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $estados = new Estado();
        $estados->setTable('estados');
        $estados = $estados->select();
        if( isset($_GET['filterEstado']) ){ 
            //obteneniendo el estado seleccionado
            $selectState = new Estado();
            $selectState->setTable('estados');
            $select = $selectState->find( $_GET['filterEstado'] ); 
            //obteneniendo el listado de municipios según el estado seleccionado
            $municipios = new Municipio();
            $municipios->setTable('municipios');
            $municipios = $municipios->whereEstado( $_GET['filterEstado'] ); 
            echo $this->view->render('pages/localidad/municipio/list',['estados'=>$estados,'select'=>$select[0],'municipios'=>$municipios]);
        }else{
            echo $this->view->render('pages/localidad/municipio/list',['estados'=>$estados]);
        }
        return $response;
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/localidad/municipio/create');
        return $response;
    }

    public function storageState(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $municipio = new Municipio();
        $municipio->setTable('municipios');
        $municipio->SetNombre( $paramts['nameMunicipie']);
        $municipio->SetCodigo_estado( $paramts['stateCode']);
        $municipio->SetCodigo(substr( $paramts['nameMunicipie'],0,4).date('s').$paramts['nameMunicipie'].time()*date('s'));
        $municipio->insert();
        return $response->withHeader('Location', '/municipios?action=success');
    }

    public function deleteMunicipio(Request $request, Response $response, $args)
    {
        $municipio = new Municipio();
        $municipio->setTable('municipios');
        $municipio->delete($_GET['municipio']);
        $municipio->save();
        return $response->withHeader('Location', '/municipios?action=success');
    }

    public function storeMunicipio(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $municipio = new Municipio();
        $municipio->setTable('municipios');
        $result = $municipio->find($_GET['municipio']);
        if( count($result) > 0 ){
            echo $this->view->render('pages/localidad/municipio/edit',['municipio'=>$result]);
        } else{
            return $response->withHeader('Location', '/municipios?unabledata=fail');
        }
        return $response;
    }

    public function updateMunicipio(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        $municipio = new Municipio();
        $municipio->setTable('municipios');
        $municipio->SetNombre( $paramts['nameState']);
        $municipio->SetCodigo_estado( $paramts['codeState']);
        $municipio->update($paramts['idMunicipio']);
        return $response->withHeader('Location', '/municipios?action=success');
    }

    public function filterByEstado(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $municipio = new Municipio();
        $municipio->setTable('municipios');
        $municipioEstado = $municipio->whereEstado($_GET['estado']);

        echo json_encode($municipioEstado);
        return $response;
    }

}