<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Paciente;
use App\Models\Historia;

class HistoriaController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {

        sessionValidate('auth');
        verifyPermission('ver_historia');

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $selectPaciente = $paciente->find($args['paciente']);

        echo $this->view->render('pages/Historias/create',['paciente'=>$selectPaciente[0]]);
        return $response;
    }

    public function saveHistorial(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('crear_historia');

        $paramts = $request->getParsedBody();

        $historia = new Historia();
        $historia->SetcontactoNombre( $paramts['nombre'] );
        $historia->SetTelefono( $paramts['telefono'] );
        $historia->SetMotivoAdmision( $paramts['motivoAdmision'] );
        $historia->SetEnfermedad( $paramts['motivoEnferma'] );
        $historia->SetDiagnostico( $paramts['diagnosticoAdmision'] );
        $historia->SetHora( $paramts['hour'] );
        $historia->SetDate( $paramts['date'] );

        //valida si no tiene historia
        $pacienteHistoria = $historia->findHistoria( $paramts['idPaciente']);
        if( count( $pacienteHistoria ) == 0 ){
            //crea la historia la primera vez
            $historia->SetPrimaryHistoria( $paramts['idPaciente'] );
            $historia->createHistoria();

            //devuelve el id de la historia a creada
            $historia->SetcodigoHistoria( $historia->getCodigoHistoria() );
        } else{
            $historia->SetcodigoHistoria( $pacienteHistoria[0]['codigo_historia'] );
        }
        $historia->SetCodigoHistoriaActual( substr($paramts['idPaciente'],0,4).$paramts['idPaciente'].time()*date('s') );
        $historia->insert();

        $url = "/pacientes"."/".$paramts['idPaciente']."?action=success";
        return $response->withHeader('Location', $url);
    }
    
    public function saveEvaluacionPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('crear_historia');
        
        $historia = new Historia();
        $historia->registerEvolution(
            substr($_GET['historia_actual'],0,4).str_replace(' ','',$_GET['tratamiento']).time()*date('s'),
            $_GET['historia_actual'],
            $_GET['hallazgos'],
            $_GET['data_objetivo'],
            $_GET['sistomas'],
            $_GET['complicaciones'],
            $_GET['impresion'],
            $_GET['tratamiento'],
            $_GET['resultados']
        );

        $listEvoluciones = $historia->getEvoluciones($_GET['historia_actual']);

        echo json_encode($listEvoluciones);
        return $response;
    }

    public function returnEvolucionesPacientes(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_historia');

        $historia = new Historia();
        $listEvoluciones = $historia->getEvoluciones($_GET['historia']);

        echo json_encode($listEvoluciones);
        return $response;
    }

    public function deleteEvolucion(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('borrar_historia');

        $historia = new Historia();
        $historia->deleteEvolucion($_GET['evolucion']);
        return $response;
    }

    public function findEvolucion(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $historia = new Historia();
        $listEvoluciones = $historia->findEvolucion($_GET['evolucion']);

        echo json_encode($listEvoluciones[0]);
        return $response;
    }

    public function editEvaluacion(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('crear_historia');
        
        $historia = new Historia();
        $historia->updateEvolucion(
            $_GET['codigoEvaluacion'],
            $_GET['historia_actual'],
            $_GET['hallazgos'],
            $_GET['data_objetivo'],
            $_GET['sistomas'],
            $_GET['complicaciones'],
            $_GET['impresion'],
            $_GET['tratamiento'],
            $_GET['resultados']
        );

        return $response;
    }

    public function finalizarHistoriaActual(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('crear_historia');

        $paramts = $request->getParsedBody();

        sessionValidate('auth');
        $historia = new Historia();
        $historia->SetCondisionSalida($paramts['tipoSalida']);
        $historia->SetDiagnosticoFinal($paramts['diagnostico']);
        $historia->SetEstatus('off');
        $historia->finalizarHistoria($paramts['codigo_historia_actual']);

        $url = "/pacientes"."/".$paramts['codigoPaciente']."?action=success";
        return $response->withHeader('Location', $url);
    }

    public function createExamenFuncional(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_historia');

        $historia = new Historia();
        $historia->registerExamen(
            substr($_GET['historiaActual'],0,4).$_GET['tipoExamen'].time()*date('s'),
            $_GET['historiaActual'],
            $_GET['tipoExamen'],
            $_GET['descripcion'],
            $_GET['datoObj']
        );

        return $response;
    }

    public function returnListExamen(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_historia');

        $historia = new Historia();
        $listExamenes = $historia->findExamenes($_GET['historiaActual']);

        echo json_encode($listExamenes);
        return $response;
    }

    public function deleteExamenFuncional(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_historia');
        $historia = new Historia();
        $historia->deleteExamen($_GET['examen']);
        return $response;
    }

    public function findExamenFuncional(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_historia');
        $historia = new Historia();
        $examenEspecifico = $historia->findExamen($_GET['examen']);

        echo json_encode($examenEspecifico[0]);
        return $response;
    }

    public function editExamenFuncional(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_historia');

        $historia = new Historia();
        $historia->updateExamen(
            $_GET['examen'],
            $_GET['tipoExamen'],
            $_GET['descripcion'],
            $_GET['datoObj']
        );

        return $response;
    }

    public function showEditHistoriaActual(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('editar_historia');

        $historia = new Historia();
        $historiaActual = $historia->findHActual($args['historia']);

        echo $this->view->render('pages/Historias/edit',['historia'=>$historiaActual[0]]);
        return $response;
    }

    public function updateHistoriaActual(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('editar_historia');

        $paramts = $request->getParsedBody();
        
        $historia = new Historia();
        $historia->SetcontactoNombre( $paramts['nombre'] );
        $historia->SetTelefono( $paramts['telefono'] );
        $historia->SetMotivoAdmision( $paramts['motivoAdmision'] );
        $historia->SetEnfermedad( $paramts['motivoEnferma'] );
        $historia->SetDiagnostico( $paramts['diagnosticoAdmision'] );
        $historia->SetHora( $paramts['hour'] );
        $historia->SetDate( $paramts['date'] );
        $historia->updateHActual($paramts['idHistoriaEdit']);

        $url = "/pacientes"."/".$paramts['idPaciente']."?action=success";
        return $response->withHeader('Location', $url);
    }

}
