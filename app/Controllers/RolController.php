<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Rol;
use App\Models\Permiso;

class RolController extends BaseController
{
    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('acciones_roles');
        $roles = new Rol();
        $roles->setTable('roles');
        $listRoles = $roles->select();
        echo $this->view->render('pages/Usuario/roles/list',['roles'=>$listRoles]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $roles = new Rol();
        $roles->setTable('roles');
        $roles->SetCodigo(substr( $paramts['rolname'],0,4).date('s').$paramts['rolname'].time()*date('s'));
        $roles->SetNombre($paramts['rolname']);
        $roles->insert();
        return $response->withHeader('Location', '/user/roles?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        $roles = new Rol();
        $roles->setTable('roles');
        $roles->delete($_GET['rol']);
        $roles->save();
        return $response->withHeader('Location', '/user/roles?action=success');
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $roles = new Rol();
        $roles->setTable('roles');
        $selectRol = $roles->find($_GET['rol']);
        echo $this->view->render('pages/Usuario/roles/edit',['rol'=>$selectRol[0]]);
        return $response;
    }

    public function update(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $roles = new Rol();
        $roles->setTable('roles');
        $roles->SetNombre($paramts['nameRol']);
        $roles->update($paramts['idRol']);
        return $response->withHeader('Location', '/user/roles?action=success');
    }

    public function showAsign(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $permise = new Permiso();
        $permise->setTable('permisos');
        $ListPermise = $permise->select();
        $roles = new Rol();
        $roles->setTable('roles');
        $selectRol = $roles->find($_GET['rol']);
        echo $this->view->render('pages/Usuario/roles/asignar',['rol'=>$selectRol[0],'permises'=>$ListPermise]);
        return $response;
    }

    public function AsignData(Request $request, Response $response, $args)
    { //mejorar esta parte
        $permisesGet = $_GET;
        $permise = new Permiso();
        $permise->setTable('permisos_roles');
        $permise->deletePermises( $_GET['idRol'] );
        $permise->save();
        
        foreach($permisesGet as $perm){
            if($perm == $_GET['idRol']){ continue; }
            $permise->SetCodigo(substr( $perm,0,4).date('s').$_GET['idRol'].$perm.time()*date('s'));
            $permise->SetCodigoRol($_GET['idRol']);
            $permise->SetCodigoPermiso($perm);
            $permise->SetEstatus(1);
            $permise->insert();
        }
        return $response->withHeader('Location', '/user/roles?action=success');
    }
}