<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Routing\RouteContext;
use Psr\Container\ContainerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Dompdf\Dompdf;
use App\Models\Persona;
use App\Models\Usuario;
use App\Models\Paciente;
use App\Models\Estado;
use App\Models\Vivienda;
use App\Models\Historia;

class PacienteController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_paciente');
        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $listadoPacientes = $paciente->select();

        echo $this->view->render('pages/Paciente/list',['pacientes'=>$listadoPacientes]);
        return $response;
    }

    public function showCreateOne(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('crear_paciente');
        $persona = new Persona();
        $persona->setTable('personas');
        $listadoPersonas = $persona->noPacientes();
        echo $this->view->render('pages/Paciente/createPaciente',['personas'=>$listadoPersonas]);
        return $response;
    }

    /**
     * @return response
     * guarda a la persona y envia los datos del paciente por sesiones para 
     * guardarlo posteriormente
     */
    public function SavePersonaAndStepNext(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();

        sessionValidate('auth');
        $_SESSION['diabetes'] = $paramts['diabetes'];
        $_SESSION['glucometro'] = $paramts['glucometro'];
        $_SESSION['fumador'] = $paramts['fumador'];
        $_SESSION['estatusLaboral'] = $paramts['ocupacion'];

        $persona = new Persona();
        $persona->setTable('personas');
        $persona->SetCedula( $paramts['cedulaPaciente'] );
        $persona->SetNombres( $paramts['nameMedico'] );
        $persona->SetApellidos( $paramts['lastnameMedico'] );
        $persona->SetSexo( $paramts['genero'] );
        $persona->SetFecha_nacimiento( $paramts['datebirth'] );
        $persona->SetTelefono( $paramts['phone'] );
        $persona->SetCodigo_persona( substr($paramts['nameMedico'],0,4).$paramts['cedulaPaciente'].'-'.time()*date('s') );
        $persona->insert();

        $paciente = $persona->find($persona->getCodigo_persona());
        
        return $response->withHeader('Location', '/pacientes/select-vivienda?paciente='.$paciente[0]['cedula']);
    }

    public function selectPersonaNowNextStep(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('crear_paciente');
        $persona = new Persona();
        $persona->setTable('personas');
        $paciente = $persona->find($_GET['user']);
        $_SESSION['diabetes'] = $_GET['diabetes'];
        
        return $response->withHeader('Location', '/pacientes/select-vivienda?paciente='.$paciente[0]['cedula']);
    }

    public function selectViviendaForPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $persona = new Persona();
        $persona->setTable('personas');
        $paciente = $persona->findCedula($_GET['paciente']);
        //listar estados del sistema
        $estados = new Estado();
        $estados->setTable('estados');
        $estados = $estados->select();
        echo $this->view->render('pages/Paciente/selectVivienda',['paciente'=>$paciente[0],'estados'=>$estados]);
        return $response;
    }

    /**
     * @return response 
     * guarda la vivienda del paciente y el paciente con los datos 
     * previamente enviados en la sección para crear personas
     */
    public function saveViviendaToPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        
        $vivienda = new Vivienda();
        $vivienda->setTable('viviendas');
        $vivienda->SetCodigo( substr($paramts['sectorVivienda'],0,4).$paramts['sectorVivienda'].'-'.time()*date('s') );
        $vivienda->SetSector( $paramts['sectorVivienda'] );
        $vivienda->SetNumeroHabitaciones( $paramts['numberHabitaciones'] );
        $vivienda->SetTipo( $paramts['tipoVivienda'] );
        $vivienda->SetEstructura( $paramts['estructuraVivienda'] );
        $vivienda->SetPiso( $paramts['materialPiso'] );
        $vivienda->SetTecho( $paramts['MaterialTecho'] );
        $vivienda->insert();

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $paciente->SetCodigo( $paramts['personaId'] );
        $paciente->SetCodigoVivienda($vivienda->GetCodigo());
        $paciente->SetDiabete( $_SESSION['diabetes'] );
        $paciente->SetOcupacion( $_SESSION['estatusLaboral'] );
        $paciente->SetGlucometro( $_SESSION['glucometro'] );
        $paciente->SetFumador( $_SESSION['fumador'] );
        $paciente->insert();

        unset( $_SESSION['diabetes'] );
        unset( $_SESSION['glucometro'] );
        unset( $_SESSION['fumador'] );
        unset( $_SESSION['estatusLaboral'] );
        return $response->withHeader('Location', '/pacientes?action=success');
    }

    /**
     * @return url
     * muestra el perfil del paciente seleccionado
     */
    public function viewProfilePaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_paciente');
        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $selectPaciente = $paciente->find($args['paciente']);

        $historia = new Historia();
        $historiaPaciente = $historia->find($args['paciente']);

        $resultadoUltimaEntrega = $paciente->getEntregasPaciente($args['paciente']);

        if( isset($resultadoUltimaEntrega[count($resultadoUltimaEntrega)-1]) ){
            $paraEntregar = $resultadoUltimaEntrega[count($resultadoUltimaEntrega)-1];
        } else{
            $paraEntregar = null;
        }

        echo $this->view->render('pages/Paciente/detailsPaciente',['paciente'=>$selectPaciente[0],'historias'=>$historiaPaciente,'entrega'=>$paraEntregar]);
        return $response;
    }

    public function validarPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $pacientePersona = $paciente->findCedula($_GET['persona']);

        if( count($pacientePersona) > 0){
            echo 'true';
        } else{
            echo 'false';
        }
        return $response;
    }

    public function showEditPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('editar_paciente');
        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $selectPaciente = $paciente->find($_GET['paciente']);
        echo $this->view->render('pages/Paciente/editPersonalData',['paciente'=>$selectPaciente[0]]);
        return $response;
    }

    public function editDataPersonaPaciente(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();

        sessionValidate('auth');
        $_SESSION['diabetes'] = $paramts['diabetes'];
        $_SESSION['glucometro'] = $paramts['glucometro'];
        $_SESSION['fumador'] = $paramts['fumador'];
        $_SESSION['estatusLaboral'] = $paramts['ocupacion'];
        
        $persona = new Persona();
        $persona->setTable('personas');
        $persona->SetNombres( $paramts['nameMedico'] );
        $persona->SetApellidos( $paramts['lastnameMedico'] );
        $persona->SetSexo( $paramts['genero'] );
        $persona->SetFecha_nacimiento( $paramts['datebirth'] );
        $persona->SetTelefono( $paramts['phone'] );
        $persona->update( $paramts['idPaciente']  );

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $selectPaciente = $paciente->find($paramts['idPaciente']);

        $estados = new Estado();
        $estados->setTable('estados');
        $estados = $estados->select();

        echo $this->view->render('pages/Paciente/editVivienda',['paciente'=>$selectPaciente[0],'estados'=>$estados]);

        return $response;
    }

    public function saveViviendaEditPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        
        $vivienda = new Vivienda();
        $vivienda->setTable('viviendas');
        $vivienda->SetSector( $paramts['sectorVivienda'] );
        $vivienda->SetNumeroHabitaciones( $paramts['numberHabitaciones'] );
        $vivienda->SetTipo( $paramts['tipoVivienda'] );
        $vivienda->SetEstructura( $paramts['estructuraVivienda'] );
        $vivienda->SetPiso( $paramts['materialPiso'] );
        $vivienda->SetTecho( $paramts['MaterialTecho'] );
        $vivienda->update( $paramts['idVivienda'] );

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $paciente->SetDiabete( $_SESSION['diabetes'] );
        $paciente->SetOcupacion( $_SESSION['estatusLaboral'] );
        $paciente->SetGlucometro( $_SESSION['glucometro'] );
        $paciente->SetFumador( $_SESSION['fumador'] );
        $paciente->update( $paramts['personaId'] );

        unset( $_SESSION['glucometro'] );
        unset( $_SESSION['fumador'] );
        unset( $_SESSION['estatusLaboral'] );
        unset( $_SESSION["diabetes"] );  
        return $response->withHeader('Location', '/pacientes?action=success');
    }

    public function deletePaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('borrar_paciente');

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $paciente->delete($_GET['paciente']);
        $paciente->save();

        $persona = new Persona();
        $persona->setTable('personas');
        $paciente->delete($_GET['paciente']);
        $paciente->save();

        return $response->withHeader('Location', '/pacientes?action=success');
    }

    public function showDetailsHistoria(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_historia');

        $historia = new Historia();
        $historiaPaciente = $historia->findHActual($args['historia']);

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $selectPaciente = $paciente->find($historiaPaciente[0]['codigo_historia']);

        echo $this->view->render('pages/Historias/detailsHistoria',['paciente'=>$selectPaciente[0],'historias'=>$historiaPaciente,'select_historia'=>$args['historia']]);

        return $response;
    }

    public function searchPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_paciente');
        $paramts = $request->getParsedBody();

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $pacientePersona = $paciente->findCedula($paramts['cedula']); 

        if( count($pacientePersona) > 0){ 
            return $response->withHeader('Location', '/pacientes/'.$pacientePersona[0]['codigo_persona']);
        } else{
            return $response->withHeader('Location', '/pacientes?search=fail');
        }
    }

    /**
     * @return document XlSX
     * retorna un documento con el listado de pacientes en excel
     */
    public function downloadExcelPacientes(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_paciente');

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $listadoPacientes = $paciente->select();

        $documento = new Spreadsheet();

        $hoja = $documento->getActiveSheet();
        $hoja->setTitle("Lista de pacientes");
        $titleSheet = ['N°','Nombre','Apellido','Cédula','Tipo diabetes','Glucometro','Teléfono','Dirección','Sexo','Fecha de nacimiento','Estado Laboral'];
        $hoja->fromArray($titleSheet,null,'A1');
        $indexListPacientes = 0;
        //listado de pacientes del sistema
        for($row = 1; $row <= count($listadoPacientes); $row++){
            for($column = 1; $column <= count($titleSheet); $column++ ){
                switch($column){
                    case 1: $hoja->setCellValueByColumnAndRow($column, $row+1, $row);
                    break;
                    case 2: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['nombres']);
                    break;
                    case 3: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['apellidos']);
                    break;
                    case 4: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['cedula']);
                    break;
                    case 5: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['tipo_diabetes']);
                    break;
                    case 6: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['glucometro']);
                    break;
                    case 7: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['telefono']);
                    break;
                    case 8: $hoja->setCellValueByColumnAndRow($column, $row+1, 'ESTADO: '.$listadoPacientes[$indexListPacientes]['nombre_estado'].', MUNICIPIO:'.$listadoPacientes[$indexListPacientes]['nombre_municipio'].', PARROQUIA:'.$listadoPacientes[$indexListPacientes]['nombre_parroquia'].', COMUNIDAD:'.$listadoPacientes[$indexListPacientes]['nombre_comunidad'].', SECTOR:'.$listadoPacientes[$indexListPacientes]['nombre_sector'].' ');
                    break;
                    case 9: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['sexo']);
                    break;
                    case 10: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['fecha_nacimiento']);
                    break;
                    case 11: $hoja->setCellValueByColumnAndRow($column, $row+1, $listadoPacientes[$indexListPacientes]['ocupacion']);
                    break;
                }
            }
            $indexListPacientes++;
        }
 
        $nombreDelDocumento = "listado-pacientes-".date("Y-m-d-h:m:s").".xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
        header('Cache-Control: max-age=0');
        
        $writer = IOFactory::createWriter($documento, 'Xlsx');
        $writer->setOffice2003Compatibility(true);
        $writer->save('php://output');

        return $response;
    }

    /**
     * @return response
     * Muestra la vista de todas las entregas de medicamentos
     */
    public function viewEntregaMedicamento(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('paciente_entregamedicamento');

        $paciente = new Paciente();
        $listaEntrega = $paciente->getEntregasMedicinas();
        echo $this->view->render('pages/Medicina/listadoEntregas',['entregas'=>$listaEntrega]);
        return $response;
    }

    /**
     * @return response
     * Selecciona paciente para entregar medicamento
     */
    public function selectPacienteEntrega(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('paciente_entregamedicamento');

        echo $this->view->render('pages/Medicina/entregas/selectPaciente');
        return $response;
    }
    /**
     * @return response
     * retorna los datos del paciente, sino existe menciona si debe registrarlo nuevo o si debe hacerlo por persona
     */
    public function findPacienteData(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_paciente');

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $pacientePersona = $paciente->findCedula($_GET['cedula']);
        if( isset($pacientePersona[0]['codigo_persona']) && $pacientePersona[0]['codigo_persona'] !== null ){
            $datosPaciente = $paciente->find($pacientePersona[0]['codigo_persona']);

            if( isset($datosPaciente[0]) ){
                echo json_encode($datosPaciente[0]);
            } else{
                echo 'La cédula esta registrada en el sistema, pero no es un paciente. Registre la cédula como paciente para poder entregar los medicamentos';
            }
            
        }else{
            echo 'la cédula no esta asociada a ningun registro, registre al paciente para poder entregar medicamentos';
        }

        return $response;
    }
    
    /**
     * @return
     * ver detalle de los medicamentos entregados a un paciente
     */
    public function viewHistoryPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('ver_paciente');

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $pacientePersona = $paciente->find($_GET['codigo_paciente']);
        $resultado = $paciente->getEntregasPaciente($_GET['codigo_paciente']);

        echo $this->view->render('pages/Paciente/historialEntregas',['paciente'=>$pacientePersona[0],'entregas'=>$resultado]);
        return $response;
    }

    public function downloadPDFhistoryPaciente(Request $request, Response $response, $args)
    {
        sessionValidate('auth');

        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $selectPaciente = $paciente->find($_GET['paciente']);

        $historia = new Historia();
        $historiaPaciente = $historia->find($_GET['paciente']);

        $dompdf = new Dompdf();

        ob_start();
        echo $this->view->render('planillas/pdf-paciente',['paciente'=>$selectPaciente[0],'historias'=>$historiaPaciente]); 
        $html = ob_get_clean();
        // instantiate and use the dompdf class
        $dompdf->loadHtml($html);
        $dompdf->render();
        $nameDocument = 'Historial del paciente-'.$selectPaciente[0]['nombres'].' '.$selectPaciente[0]['apellidos'];
        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=$nameDocument.pdf");
        echo $dompdf->output();

        return $response;
    }

    public function disenoHtmlforPaciente(Request $request, Response $response, $args)
    {
        $paciente = new Paciente();
        $paciente->setTable('pacientes');
        $selectPaciente = $paciente->find('aris12345678-34301213541');

        $historia = new Historia();
        $historiaPaciente = $historia->find('aris12345678-34301213541');

        echo $this->view->render('planillas/pdf-paciente',['paciente'=>$selectPaciente[0],'historias'=>$historiaPaciente]);   
        return $response;
    }

}