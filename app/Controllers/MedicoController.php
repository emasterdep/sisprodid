<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Models\Medico;
use App\Models\Persona;
use App\Models\Especialidad;

class MedicoController extends BaseController
{
    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $medicos = new Medico();
        $medicos->setTable('medicos');
        $listaMedicos = $medicos->select();
        echo $this->view->render('pages/Medico/list',['medicos'=>$listaMedicos]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        verifyPermission('crear_medicos');
        echo $this->view->render('pages/Medico/create');
        return $response;
    }

    public function saveMedico(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        
        $persona = new Persona();
        $persona->setTable('personas');
        $existPerson = $persona->findCedula( $paramts['cedulaMedico'] ); 
        if( isset($existPerson[0]['cedula']) && $existPerson[0]['cedula']  == $paramts['cedulaMedico'] ){
            return $response->withHeader('Location', '/medicos/create?action=repeatcedula');
        } 
        $persona->SetCedula( $paramts['cedulaMedico'] );
        $persona->SetNombres( $paramts['nameMedico'] );
        $persona->SetApellidos( $paramts['lastnameMedico'] );
        $persona->SetSexo( $paramts['genero'] );
        $persona->SetFecha_nacimiento( $paramts['datebirth'] );
        $persona->SetTelefono( $paramts['phone'] );
        $persona->SetCodigo_persona( substr($paramts['nameMedico'],0,4).$paramts['cedulaMedico'].time()*date('s') );
        $persona->insert();

        $medico = new Medico();
        $medico->setTable('medicos');
        $medico->SetCodigo( $persona->getCodigo_persona() );
        $medico->SetImpremedico('default');
        $medico->insert();
        return $response->withHeader('Location', '/medicos?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $medico = new Medico();
        $medico->setTable('medicos');
        $medico->delete($_GET['id_medico']);
        $medico->save();
        return $response->withHeader('Location', '/medicos?action=success');
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $medico = new Medico();
        $medico->setTable('medicos');
        $medicoResult = $medico->find($_GET['id_medico']);
        echo $this->view->render('pages/Medico/edit',['medico'=>$medicoResult[0]]);
        return $response;
    }

    public function update(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        
        $persona = new Persona();
        $persona->setTable('personas');
        $existPerson = $persona->findCedula( $paramts['cedulaMedico'] );
        if( isset($existPerson[0]['cedula']) 
            && $existPerson[0]['cedula'] != $paramts['lastCedula'] 
            && $existPerson[0]['cedula'] == $paramts['cedulaMedico'] ){
                //si la cedula esta repetida, retorno a editar al medico
                $medicoResult = $persona->findCedula($paramts['lastCedula']);
                echo $this->view->render('pages/Medico/edit',['medico'=>$medicoResult[0],'action'=>'repeatcedula']);
                return $response;
        } 
        $persona->SetCedula( $paramts['cedulaMedico'] );
        $persona->SetNombres( $paramts['nameMedico'] );
        $persona->SetApellidos( $paramts['lastnameMedico'] );
        $persona->SetSexo( $paramts['genero'] );
        $persona->SetFecha_nacimiento( $paramts['datebirth'] );
        $persona->SetTelefono( $paramts['phone'] );
        $persona->update( $paramts['id_medico'] );

        return $response->withHeader('Location', '/medicos?action=success');
    }

    public function detailsMedico(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        //me el medico en especifico
        $medico = new Medico();
        $medico->setTable('medicos');
        $medicoResult = $medico->find($args['medico']);
        //listado de especialidades para agregar al medico en caso de no poseer
        $especialidades = new Especialidad();
        $especialidades->setTable('especialidades');
        $listadoEspecialidades = $especialidades->getEspecialidadesLibre($args['medico']);
        //traer el listado de especialidades del medico
        $especialidadesMedico = $especialidades->medico($args['medico']);
        echo $this->view->render('pages/Medico/listaEspecialidad',
            ['medico'=>$medicoResult[0],'especialidades'=>$listadoEspecialidades,'especialidadesMedico'=>$especialidadesMedico]);
        return $response;
    }

    public function createEspecilidadMedico(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();

        $especialidadMedico = new Especialidad();
        $especialidadMedico->SetCodigoMedico($paramts['medico']);
        $especialidadMedico->SetCodigo($paramts['especialidad']);
        $especialidadMedico->SetCodigoMedicoEspecialidad( substr($paramts['medico'],0,4).$paramts['medico'].time()*date('s') ); //esta es la clave primaria
        $especialidadMedico->createMedicoEspecialidad();

        return $response->withHeader('Location', '/medicos/'.$paramts['medico'].'?action=success');
    }

    public function deleteEspecialidadMedico(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $especialidadMedico = new Especialidad();
        $especialidadMedico->deleteEspecialidadMedico($_GET['code_especialidad']);

        return $response->withHeader('Location', '/medicos/'.$_GET['medico'].'?action=deleteEspecialidad');
    }

    /**
     * 
     */
    public function downloadListMedicos(Request $request, Response $response, $args)
    {
        sessionValidate('auth');

        $medicos = new Medico();
        $medicos->setTable('medicos');
        $listaMedicos = $medicos->select();

        $documento = new Spreadsheet();

        $hoja = $documento->getActiveSheet();
        $hoja->setTitle("Lista Medicos");
        $titleSheet = ['N°','Nombre','Apellido','Cédula','Teléfono','Fecha de nacimiento'];
        $hoja->fromArray($titleSheet,null,'A1');

        $indexListPacientes = 0;
        for($row=1; $row <= count($listaMedicos); $row++){
            for($column = 1; $column <= count($titleSheet); $column++){
                switch($column){
                    case 1: $hoja->setCellValueByColumnAndRow($column, $row+1, $row);
                    break;
                    case 2: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaMedicos[$indexListPacientes]['nombres']);
                    break;
                    case 3: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaMedicos[$indexListPacientes]['apellidos']);
                    break;
                    case 4: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaMedicos[$indexListPacientes]['cedula']);
                    break;
                    case 5: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaMedicos[$indexListPacientes]['telefono']);
                    break;
                    case 6: $hoja->setCellValueByColumnAndRow($column, $row+1, $listaMedicos[$indexListPacientes]['fecha_nacimiento']);
                    break;
                }
            }
            $indexListPacientes++;
        }

        $nombreDelDocumento = "listado-medicos-".date("Y-m-d-h:m:s").".xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
        header('Cache-Control: max-age=0');
        
        $writer = IOFactory::createWriter($documento, 'Xlsx');
        $writer->setOffice2003Compatibility(true);
        $writer->save('php://output');

        return $response;
    }

}