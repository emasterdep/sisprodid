<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Parroquea;
use App\Models\Comunidad;

class ComunidadController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $estados = new Estado();
        $estados->setTable('estados');
        $estados = $estados->select();
        if( isset($_GET['filterEstado']) ){ 
            //listado de municipios del filtro
            $municipio = new Municipio();
            $municipio->setTable('municipios');
            $listaMunicipio = $municipio->whereEstado( $_GET['filterEstado'] );
            //listado de parroqueas del municipio
            $parroquea = new Parroquea();
            $parroquea->setTable('parroquias');
            $listaParroqueas = ( isset($_GET['filterMunicipio']) ) ? $parroquea->whereMunicipio( $_GET['filterMunicipio'] ) : null;
            //parroquia selecciona
            $selectParroquia = ( isset($_GET['filterParroquea']) ) ? $parroquea->find($_GET['filterParroquea']) : null ;
            //listado de comunidades de la parroquia
            $comunidades = new Comunidad();
            $comunidades->setTable('comunidades');
            $listComunidades = ( isset($_GET['filterParroquea']) ) ? $comunidades->whereParroquia($_GET['filterParroquea']) : null;

            echo $this->view->render('pages/localidad/comunidad/list',
                ['estados'          =>$estados,
                'municipios'        =>$listaMunicipio,
                'parroqueas'        =>$listaParroqueas,
                'comunidades'       =>$listComunidades,
                'selectParroquia'   =>$selectParroquia[0]
            ]);
        }else{
            echo $this->view->render('pages/localidad/comunidad/list',['estados'=>$estados]);
        }
        return $response;
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/localidad/comunidad/create');
        return $response;
    }

    public function storageComunidad(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $comunidad = new Comunidad();
        $comunidad->setTable('comunidades');
        $comunidad->SetNombre( $paramts['nameComunidad'] );
        $comunidad->SetCodigo_parroquia( $paramts['parroquiaCode']);
        $comunidad->SetCodigo(substr( $paramts['nameComunidad'],0,4).date('s').$paramts['nameComunidad'].time()*date('s'));
        $comunidad->insert();
        return $response->withHeader('Location', '/comunidades?action=success');
    }

    public function deleteParroquia(Request $request, Response $response, $args)
    {
        $comunidad = new Comunidad();
        $comunidad->setTable('comunidades');
        $comunidad->delete($_GET['comunidad']);
        $comunidad->save();
        return $response->withHeader('Location', '/comunidades?action=success');
    }

    public function storeComunidad(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $comunidad = new Comunidad();
        $comunidad->setTable('comunidades');
        $result = $comunidad->find($_GET['comunidad']);
        if( count($result) > 0 ){
            echo $this->view->render('pages/localidad/comunidad/edit',['comunidad'=>$result[0]]);
        } else{
           return $response->withHeader('Location', '/comunidades?unabledata=fail');
        }
        return $response;
    }

    public function updateComunidad(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        $comunidad = new Comunidad();
        $comunidad->setTable('comunidades');
        $comunidad->SetNombre( $paramts['nameComunidad']);
        $comunidad->SetCodigo_parroquia( $paramts['codeParroquia']);
        $comunidad->update($paramts['idComunidad']);
        return $response->withHeader('Location', '/comunidades?action=success');
    }

    public function filterByParroquea(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $comunidades = new Comunidad();
        $comunidades->setTable('comunidades');
        $listComunidades = $comunidades->whereParroquia($_GET['parroquea']);

        echo json_encode($listComunidades);
        return $response;
    }

}