<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Routing\RouteContext;
use Psr\Container\ContainerInterface;
use App\Core\Sesion;
use App\Core\Permission;
use App\Models\Persona;
use App\Models\Usuario;
use App\Models\Rol;
use App\Models\Paciente;


class UserController extends BaseController
{

    public function view(Request $request, Response $response, $args)
    {
        $sesionUser = new Sesion();
        $sesionUser->sessionStart();
        if(!$sesionUser->verifySession('auth')){
            echo $this->view->render('pages/login');
        }else{
            return $response->withHeader('Location', '/dashboard');
        }
        return $response;
    }

    public function initSystem(Request $request, Response $response, $args)
    {
        $persona = new Persona();
        $persona->setTable('personas');
        $persona->SetCedula( '12xxxx' );
        $persona->SetNombres( 'Administrador' );
        $persona->SetApellidos( 'Sistema' );
        $persona->SetSexo( 'o' );
        $persona->SetFecha_nacimiento( '2021/01/01' );
        $persona->SetTelefono( '000-00000000' );
        $persona->SetCodigo_persona( 'admin0102todo2021' );
        $persona->insert();

        $user = new Usuario();
        $user->setTable('usuarios');
        $user->SetCodigo_usuario($persona->getCodigo_persona());
        $user->SetCorreo('admin@admin.com');
        $user->SetPassword(password_hash('87654321abc',PASSWORD_BCRYPT));
        $user->SetUsername('administrador');
        $user->insert();
        
        return $response;
    }

    public function login(Request $request, Response $response, $args)
    {
        $routeParser = RouteContext::fromRequest($request)->getRouteParser();

        $paramts = $request->getParsedBody();
        $user = new Usuario();
        $user->setTable('usuarios');
        $respuesta = $user->login($paramts['password'],$paramts['email']); 
        if($respuesta['status']) {
            $sesionUser = new Sesion();
            $sesionUser->createSession('auth',['user'=>$respuesta['user'],'status'=>'active']);
            $_SESSION['globalUser'] = $respuesta['user'];

            $permisosUser = $user->getPermission($paramts['email']);
            $userPermisse = new Permission();
            $userPermisse->SetPermisse($permisosUser);

            $url = $routeParser->urlFor('dashboard');
            return $response->withHeader('Location', $url);
        }else{
            return $response->withHeader('Location', '/?status=error');   
        }
    }

    public function logout(Request $request, Response $response, $args)
    {
        $sesionUser = new Sesion();
        $sesionUser->sessionStart();
        if ($sesionUser->verifySession('auth')){ 
            $sesionUser->destroySession();
            return $response->withHeader('Location', '/');
        }
        return $response;
    }

    /**
     * @return view 
     * verifica si tiene permiso el usuario para poder mostrar los datos de inicio
     */
    public function dashboard(Request $request, Response $response, $args)
    {
        $sesionUser = new Sesion();
        $sesionUser->sessionStart();
        if ($sesionUser->verifySession('auth')){ 
            //verifica los permisos para ver el dashboard
            if( ifPermise('ver_dashboard') ){ 
                $paciente = new Paciente();
                $listaPaciente = $paciente->select();
                $number = $paciente->convertNumberTipoPaciente($listaPaciente);

                echo $this->view->render('pages/dashboard',['pacientes'=>$listaPaciente,'tipos'=>$number]);
            }else{
                return $response->withHeader('Location', '/user/edit?user='.$_SESSION['globalUser'][0]['codigo_usuario']);   
            }
        } else{ 
            return $response->withHeader('Location', '/?permise=error');
        }
        return $response;
    }

    public function ajustes(Request $request, Response $response, $args)
    {
        $sesionUser = new Sesion();
        $sesionUser->sessionStart();
        if ($sesionUser->verifySession('auth')){ 
            echo $this->view->render('pages/ajustes');
        } else{ 
            return $response->withHeader('Location', '/?permise=error');
        }
        return $response;
    }

    public function listUser(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $user = new Usuario();
        $user->setTable('usuarios');        
        $ListUsuarios = $user->select();
        echo $this->view->render('pages/Usuario/listUsuarios',['usuarios'=>$ListUsuarios]);
        return $response;
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $roles = new Rol();
        $roles->setTable('roles');
        $listadoRoles = $roles->select();
        echo $this->view->render('pages/Usuario/createUser',['roles'=>$listadoRoles]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();

        $persona = new Persona();
        $persona->setTable('personas');
        $persona->SetCedula( $paramts['cedulaPaciente'] );
        $persona->SetNombres( $paramts['nameUser'] );
        $persona->SetApellidos( $paramts['lastnameUser'] );
        $persona->SetSexo( $paramts['genero'] );
        $persona->SetFecha_nacimiento( $paramts['datebirth'] );
        $persona->SetTelefono( $paramts['phone'] );
        $persona->SetCodigo_persona( substr($paramts['nameUser'],0,4).$paramts['cedulaPaciente'].'-'.time()*date('s') );
        $persona->insert();

        $user = new Usuario();
        $user->setTable('usuarios');
        $user->SetCodigo_usuario($persona->getCodigo_persona());
        $user->SetCorreo( $paramts['correoUser'] );
        $user->SetPassword(password_hash( $paramts['passUser'] ,PASSWORD_BCRYPT));
        $user->SetUsername( $paramts['userNameID'] );
        $user->insert();
        //asignar rol a usuario
        $user->SetCodeRolUser( $persona->getCodigo_persona().time()*date('s').time()*date('m') );
        $user->SetCodeRol( $paramts['rolUser'] );
        $user->SetEstado( 'on' );
        $user->asignRol();

        return $response->withHeader('Location', '/users?action=success');
    }

    public function editView(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $roles = new Rol();
        $roles->setTable('roles');
        $listadoRoles = $roles->select();
        $user = new Usuario();
        $user->setTable('usuarios');
        $usuario = $user->find($_GET['user']);
        echo $this->view->render('pages/Usuario/EditUser',['roles'=>$listadoRoles,'usuario'=>$usuario[0]]);
        return $response;
    }

    public function updateUser(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();

        $persona = new Persona();
        $persona->setTable('personas');
        $persona->SetCedula( $paramts['cedulaPaciente'] );
        $persona->SetNombres( $paramts['nameUser'] );
        $persona->SetApellidos( $paramts['lastnameUser'] );
        $persona->SetSexo( $paramts['genero'] );
        $persona->SetFecha_nacimiento( $paramts['datebirth'] );
        $persona->SetTelefono( $paramts['phone'] );
        $persona->update( $paramts['userId'] );

        $user = new Usuario();
        $user->setTable('usuarios');
        $user->SetCorreo( $paramts['correoUser'] );
        if( $paramts['passUser'] == $paramts['passOldUser'] ){
            $user->SetPassword( $paramts['passOldUser'] );
        }else{
            $user->SetPassword(password_hash( $paramts['passUser'] ,PASSWORD_BCRYPT));
        }
        $user->SetUsername( $paramts['userNameID'] );
        $user->update( $paramts['userId'] );
        //asignar rol a usuario
        $user->SetCodeRol( $paramts['rolUser'] );
        $user->SetEstado( 'on' );
        $user->updateRol( $paramts['userId'] );

        return $response->withHeader('Location', '/users?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        echo $this->view->render('pages/login');

        $user = new Usuario();
        $user->setTable('usuarios');
        $user->delete($_GET['user']);
        $user->save();

        return $response->withHeader('Location', '/users?action=success');
    }

    public function noPermise(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/noPermiso');
        return $response;
    }

}   