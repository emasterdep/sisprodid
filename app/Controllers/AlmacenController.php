<?php 
namespace App\Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Almacen;

class AlmacenController extends BaseController
{
    public function view(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $almacenes = new Almacen();
        $almacenes->setTable('almacenes');
        $listaAlmacenes = $almacenes->select();
        echo $this->view->render('pages/Medicina/almacen/list',['almacenes'=>$listaAlmacenes]);
        return $response;
    }

    public function create(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        echo $this->view->render('pages/Medicina/almacen/create');
        return $response;
    }

    public function saveAlmacen(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $paramts = $request->getParsedBody();
        $almacen = new Almacen();
        $almacen->setTable('almacenes');
        $almacen->SetTitulo($paramts['tituloAlmacen']);
        $almacen->SetUbicacion($paramts['ubicacionAlmacen']);
        $almacen->SetTotalDisponible($paramts['seccionesTotales']);
        $almacen->SetDisponible($paramts['seccionesDisponibles']);
        $almacen->SetTipo($paramts['tipoAlmacen']);
        $almacen->SetAncho($paramts['anchoAlmacen']);
        $almacen->SetLargo($paramts['largoAlmacen']);
        $almacen->SetCodigo(substr( $paramts['tituloAlmacen'],0,4).date('s').$paramts['tituloAlmacen'].time()*date('s'));
        $almacen->insert();
        
        return $response->withHeader('Location', '/medicamentos/almacenes?action=success');
    }

    public function delete(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $almacen = new Almacen();
        $almacen->setTable('almacenes');
        $almacen->delete( $_GET['almacen'] );
        $almacen->save();
        return $response->withHeader('Location', '/medicamentos/almacenes?action=success');
    }

    public function show(Request $request, Response $response, $args)
    {
        sessionValidate('auth');
        $almacen = new Almacen();
        $almacen->setTable('almacenes');
        $selectAlmacen = $almacen->find($_GET['almacen']);
        echo $this->view->render('pages/medicina/almacen/edit',['almacen'=>$selectAlmacen[0]]);
        return $response;
    }

    public function update(Request $request, Response $response, $args)
    {
        $paramts = $request->getParsedBody();
        $almacen = new Almacen();
        $almacen->setTable('almacenes');
        $almacen->SetTitulo($paramts['tituloAlmacen']);
        $almacen->SetUbicacion($paramts['ubicacionAlmacen']);
        $almacen->SetTotalDisponible($paramts['seccionesTotales']);
        $almacen->SetDisponible($paramts['seccionesDisponibles']);
        $almacen->SetTipo($paramts['tipoAlmacen']);
        $almacen->SetAncho($paramts['anchoAlmacen']);
        $almacen->SetLargo($paramts['largoAlmacen']);
        $almacen->update($paramts['id_almacen']);

        return $response->withHeader('Location', '/medicamentos/almacenes?action=success');
    }
}